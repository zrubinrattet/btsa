<?php

//notas, agregue algunas cosas que no probe, para un helper de header_left


error_reporting(E_ALL);
ini_set('display_errors',true); 
define('DEBUG',true);

//define('SHORTINIT', true);
//define('RHCAJAX',true);

require '../../../wp-load.php';

class rhc_generate_block_js {
		
	function __construct(){
		$this->render();
	}
	
	//taken from class.rhc_visual_composer.php
	function set_pop_conditional_options( &$t, $i ){
		$t[$i]->options[]=(object)array(
				'id'			=> 'vc_tab_condition',
				'type' 			=> 'vc_tab', 
				'label'			=> __('Conditions','rhc'),
				'vc_tab'		=> true //flat the start of a tab in vc.
			);		
	
		$t[$i]->options[]=(object)array(
				'id'	=> 'cal_capability',
				'type'	=> 'text',
				'label' => __('Permission (capability)','rhc'),
				'description' => __( 'If used, the shortcode will only display if the user is logged in and have the specific capability.', 'rhc')
			);	
		$conditional_tags = apply_filters( 'postinfo_allowed_conditional_tags', array( 
				'is_home',
				'is_front_page',
				'is_singular',
				'is_page',
				'is_single',
				'is_sticky',
				'is_category',
				'is_tax',
				'is_author',
				'is_archive',
				'is_search',
				'is_attachment',
				'is_tag',
				'is_date',
				'is_paged',
				'is_main_query',
				'is_feed',
				'is_trackback',
				'in_the_loop',
				'is_user_logged_in'
				));
		$j = 0;	
		foreach($conditional_tags as $is_condition){				
			$tmp=(object)array(
				'id'			=> 'cal_conditional_tag_'.$is_condition,
				'name'			=> 'cal_conditional_tag[]',
				'type'			=> 'checkbox',
				'option_value'	=>$is_condition,
				'default'		=> '',
				'label'			=> $is_condition,
				'vc_label' 		=> __('Conditional Render','rhc')
			);
			if($j==0){
				$tmp->description = __("Check the conditions to test for displaying the shortcode.  Leave empty to display everywhere, included feeds and trackbacks.",'rhc');
			}
			$t[$i]->options[]=$tmp;
			$j++;	
		}					
	}
	
	function get_options(){
		$t = array();
		//options in RightHere options syntax.	
		define('RHC_GUTENBERG', 1);
		require 'includes/options.calendarize_shortcode.php';
		
		$t = apply_filters( 'pop_calendarizeit_options_for_vc_params', $t );	
		
		$this->set_pop_conditional_options( $t, $i );	
		
		return $t;
	}
		
	function render(){
		$t = $this->get_options()[0]->options;
		
		$obj = new ArrayObject( $t );
		$it = $obj->getIterator();		
	
		$this->render_item( $it );			

		return;

		


/*
				el( 
					PanelBody, {
						title: __( 'General' ),
						className: 'rhc-inspector-tab-general',
						initialOpen: true,
					},
					el(
						ToggleControl,
						{
							label:	'Test',
							checked: props.attributes.test,
							onChange:function(e) {
								return props.setAttributes({
									test: !props.attributes.test
								})
							}
						}
					)				
				)
*/	
	}

	function render_item( &$it, &$depth=0, $skip_if_type=false ){
		$depth++;
		
		$f = $it->current();
		$method = property_exists( $f, 'type' ) ? '_'.$f->type : '_unhandled' ;
		
		if( $method == $skip_if_type ){
			//this if for elements with children
			return;
		}
		
		if( !method_exists( $this, $method ) ){
			$method = '_unhandled';
		}		
		
		if( property_exists( $f, 'gutenberg_skip' ) && intval( $f->gutenberg_skip ) ){
			
		}else{
			$this->$method( $it, $f, $depth );		
		}	
	
		//-----
		$it->next();
		
		if( $it->valid() ){	
			$this->render_item( $it, $f, $skip_if_type );
		}
		
		$depth--;
	}
	
	function _vc_tab( &$it, $f, &$depth ){
?>
				el( 
					PanelBody, {
						title: __( '<?php echo $f->label ?>' ),
						className: 'rhc-inspector-<?php echo $f->id?>',
						initialOpen: <?php echo $it->key() == 0 ? 'true' : 'false' ?>,
					},
<?php 
		$it->next();
		if( $it->valid() ){

			$this->render_item( $it, $depth, '_vc_tab' );				
			//will eventually hit a vc_tab, we need to rewind or will skip that
			if( $it->key() > 0 ){
				$it->seek( $it->key() - 1 );			
			}	
		}
?>		
				),
<?php	
	}
	
	function is_tab( $f ){	
		return property_exists( $f, 'type' ) && $f->type == 'vc_tab' ? true : false ;
	}
	
	function _clear( &$it, $f, &$depth ){
		//clear does nothing in gutenberg.
	}
	
	function _submit( &$it, $f, &$depth ){
	
	}
	
	function _subtitle( &$it, $f, &$depth ){
?>
					el(
						'h2',
						{
							<?php echo $this->get_help_text( $it, $f, $depth ) ?> 
							className:'rhc-inspector-subtitle'
						},
						__('<?php echo esc_attr( $f->label )?>')
					),
<?php		
	}

	function _textarea( &$it, $f, &$depth ){
		return $this->_text( $it, $f, $depth );
	}

	function _buffered_text( &$it, $f, &$depth ){
		$id = $this->get_parameter_name_from_option_name( $f );
?>
					el(
						TextControl,
						{
							type: 'text',
							<?php $this->get_label( $it, $f, $depth ) ?>
							placeholder: props.attributes.<?php echo $id?>,
							<?php echo $this->get_help_text( $it, $f, $depth ) ?> 
							onClick: function(e){
								$( e.target ).val( props.attributes.<?php echo $id?> );
							},
							onChange: function(value){
								props.buffer.<?php echo $id?> = value;

								if(props.buffer.textcontrol_settimeout_id){
									clearTimeout( props.buffer.textcontrol_settimeout_id );
								}
			
								props.buffer.textcontrol_settimeout_id = setTimeout(function(){
									props.setAttributes({
										<?php echo $id?>: props.buffer.<?php echo $id?> 
									});
									props.buffer.textcontrol_settimeout_id = null;									
								}, 3000 );			
							}
						}
					),
<?php	
	}

	function _text( &$it, $f, &$depth ){
		$id = $this->get_parameter_name_from_option_name( $f );
?>
					el(
						TextControl,
						{
							type: 'text',
							<?php $this->get_label( $it, $f, $depth ) ?>
							placeholder: props.attributes.<?php echo $id?>,
							<?php echo $this->get_help_text( $it, $f, $depth ) ?> 
							value: props.attributes.<?php echo $id?>,
							onChange: function(value){
								props.setAttributes({
									<?php echo $id?>: value
								});		
							},
							'data-cal_id': '<?php echo $id?>',
						}
					),
<?php	
	}

	function _div_start( &$it, $f, &$depth ){
	
	}

	function _div_end( &$it, $f, &$depth ){
	
	}

	function _xrange( &$it, $f, &$depth ){
		return $this->_text( $it, $f, $depth );
	}

	function _range( &$it, $f, &$depth ){
		$id = $this->get_parameter_name_from_option_name( $f );
?>
					el(
						RangeControl,
						{
							<?php $this->get_label( $it, $f, $depth ) ?>
							<?php echo $this->get_help_text( $it, $f, $depth ) ?> 
							onChange: function(value){
								props.setAttributes({
									<?php echo $id?>: value.toString()
								});	
							},
							min: <?php echo $f->min ?>,
							max: <?php echo $f->max ?>,
							step: <?php echo $f->step ?>,
							value: Number( props.attributes.<?php echo $id?> ),
						}
					),
<?php	
	}

	function _checkbox( &$it, $f, &$depth ){
		$id = $this->get_parameter_name_from_option_name( $f );

		$name_suffix = '';
		if( '[]' == substr( $id, -2 ) ){
			$id = substr( $id, 0, -2 );
			$name_suffix = '[]';
		}			
?>
					el(
						CheckboxControl,
						{
							<?php $this->get_label( $it, $f, $depth ) ?>
							name: '<?php echo $id.$name_suffix?>',
							checked: !!+(props.attributes.<?php echo $id?>.indexOf('<?php echo $f->option_value ?>')>-1),
							onChange:function(e) {
								d = props.attributes.<?php echo $id?>.slice();
					
								var index = d.indexOf('<?php echo $f->option_value ?>');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('<?php echo $f->option_value ?>');
								}										

								props.setAttributes({
									<?php echo $id?>: d
								});	
							}							
						}
					),<?php
	}

	function _yesno( &$it, $f, &$depth ){
		$id = $this->get_parameter_name_from_option_name( $f );
?>
					el(
						ToggleControl,
						{
							<?php $this->get_label( $it, $f, $depth ) ?>
							checked: !!+props.attributes.<?php echo $id?>,
							<?php echo $this->get_help_text( $it, $f, $depth ) ?>
							onChange:function(e) {
								return props.setAttributes({
									<?php echo $id?>: e ? '1' : '0'	
								})
							}
						}
					),<?php
	}
	
	function _onoff( &$it, $f, &$depth ){
		return $this->_yesno( $it, $f, $depth );
	}
	
	function _select( &$it, $f, &$depth ){
		
		$id = $this->get_parameter_name_from_option_name( $f );
	
		//$type = property_exists( $f, 'type' ) ? $f->type : 'type not defined.' ;
		//echo "					el('h5',{},'Unhandled select: $type'),\n";
?>		
					el(
						SelectControl,
						{
							type: 'text',
							<?php $this->get_label( $it, $f, $depth ) ?>
							value:  props.attributes.<?php echo $id?> ,
							options: <?php $this->select_options( $it, $f, $depth )?>,
							<?php echo $this->get_help_text( $it, $f, $depth ) ?> 
							onChange: function(value){
								props.setAttributes({
									<?php echo $id?>: value
								});	
							}
						}
					),			
<?php			
	}
	
	function select_options( &$it, $f, &$depth ){
		if( $f->id == 'cal_defaultview' ){
			//this option has an apply_filter that may add more views
			echo 'RHC_GUTENBERG.defaultview_options';
			return;
		}
?>
	[
<?php foreach( $f->options as $value => $label ):?>								
									{
										label: __('<?php echo esc_attr($label) ?>'),
										value: '<?php echo $value ?>'
									},
<?php endforeach; ?>
								]<?php			
	}
	
	function get_help_text( &$it, $f, &$depth ){
		return '';
		
		if( property_exists( $f, 'description' ) && !empty( $f->description ) ){
			return sprintf("help: el('div',{},__('%s')),\n", $f->description);		
		}else{
			return '';
		}
	}
	
	function get_label( &$it, $f, &$depth ){
		if( property_exists( $f, 'gutenberg_helper' ) ){
			return $this->get_label_with_helper( $it, $f, $depth );
		}

		if( property_exists( $f, 'description' ) && !empty( $f->description ) ){
			return $this->get_label_with_tooltip( $it, $f, $depth );
		}
	
							?>label:	__('<?php echo strip_tags( $f->label )?>'),
<?php	
	}
	
	function get_label_with_tooltip( &$it, $f, &$depth ){ 
		$description = str_replace("'","\'",$f->description);
							?>label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('<?php echo $description ;?>')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('<?php echo strip_tags( $f->label ) ?>') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),<?php	
	}
	
	function get_label_with_helper( &$it, $f, &$depth ){ 
		
							?>
							label: el( 'span', {},
									el( 'span', {}, __('<?php echo strip_tags( $f->label ) ?>') ),
									el( 'span', {
										onClick: function(e){
											return <?php echo $f->gutenberg_helper?>.open( '#<?php echo $f->id?>' );	
										}									
									},  
										el( Dashicon, {
											icon:'admin-settings',
										} ),									
									),
							),<?php	
	}
	
	function get_parameter_name_from_option_name( $t ){
		if( property_exists( $t, 'name' ) ){
			$str = $t->name;	
		}else{
			$str = $t->id;
		}
	
		$prefix = 'cal_';
		if (substr($str, 0, strlen($prefix)) == $prefix) {
			$str = substr($str, strlen($prefix));
		} 
		
		return $str;
	}	
	
	
	function _unhandled( &$it, $f, &$depth ){
		$type = property_exists( $f, 'type' ) ? $f->type : 'type not defined.' ;
		if( property_exists( $f, 'label' ) ){
			$type.=' '.$f->label;
		}
		if( property_exists( $f, 'id' ) ){
			$type.=' '.$f->id;
		}		
		echo "					el('h5',{},'Unhandled type: $type'),\n";
	}
}

try {
	new rhc_generate_block_js(  );
}catch( Exception $e ){
	echo "ERR:".$e->getMessage();
}
