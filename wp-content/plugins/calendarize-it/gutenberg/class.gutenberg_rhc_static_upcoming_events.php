<?php

class gutenberg_rhc_static_upcoming_events {

	function __construct(){

		add_action( 'init', array( &$this, 'init' ) );

	}

	function init() {

		//date helper:
		wp_register_style(
			'rc-dateformat-helper-css',
			RHC_URL . 'gutenberg/css/rc_dateformat_helper.css',
			array(),
			'0.0.5'
		);
		wp_register_script(
			'rc-dateformat-helper-js',
			RHC_URL . 'gutenberg/js/rc_dateformat_helper.js',
			array(),
			'0.0.3'
		);

		//-- main scripts
		wp_register_style(
			'rhc_static_upcoming_events-gutenberg-css',
			RHC_URL . 'gutenberg/css/rhc_static_upcoming_events.css',
			array( 'calendarizeit', 'rhc-last-minue', 'rc-dateformat-helper-css' ),
			'1.0.8'
		);

		wp_register_script(
			'rhc_static_upcoming_events-gutenberg',
			RHC_URL . 'gutenberg/js/rhc_static_upcoming_events.js',
			array( 'wp-blocks', 'wp-element', 'wp-editor', 'wp-i18n', 'calendarize', 'bootstrap-select', 'rc-dateformat-helper-js' ),
			'1.0.11'
		);

		wp_localize_script( 'rhc_static_upcoming_events-gutenberg', 'RHC_GUTENBERG_SUPE', array(
			'attributes'		=> $this->get_attributes_for_register_block_type(),
			'ajax_url'			=> site_url('/wp-json/gutenberg/v1/block-renderer/calendarize-it/rhc-static-upcoming-events'),
			'template_options'	=> $this->get_supe_template_options(),
			'post_type_options' => $this->get_post_type_options(),
			'btn_tax_options'	=> $this->get_btn_tax_options(),
			'RHC_EVENTS'		=> RHC_EVENTS,
		) );

		global $rhc_plugin;

		if( !function_exists('register_block_type') ){
			return;
		}

		$result = register_block_type( 'calendarize-it/rhc-static-upcoming-events', array(
			'render_callback' 	=> array( &$rhc_plugin->shortcode_rhc_static_upcoming_events, 'sc_rhc_static_upcoming_events' ),
			'editor_script' 	=> 'rhc_static_upcoming_events-gutenberg',
			'editor_style'		=> 'rhc_static_upcoming_events-gutenberg-css',
			'attributes' 		=> $this->get_attributes_for_register_block_type(),

		) );

		//

	}

	function get_btn_tax_options(){
		//first find all post types used as events, as per options
		global $rhc_plugin;
		$post_types = $rhc_plugin->get_option('post_types',array());
		$post_types = is_array($post_types)?$post_types:array();
		$post_types = apply_filters('rhc_calendar_metabox_post_types',$post_types);

		if( ! in_array( RHC_EVENTS, $post_types ) ){
			$post_types[] = RHC_EVENTS;
		}

		if ( ! $post_types ) {
			$post_types[] = RHC_EVENTS;
		}

		$ret = (object)array(
			'__labels' => (object)array()//(lets cross fingers no one wants a post_type named __labels :)
		);

		//now find all associated taxonomies to each post type
		foreach( $post_types as $post_type ){
			if( $post_type_object = get_post_type_object( $post_type ) ){
				$ret->__labels->$post_type = $post_type_object->labels->name;
				$ret->$post_type = (object)array();
				if( $taxonomies = get_object_taxonomies( $post_type ) ){
					foreach( $taxonomies as $taxonomy ){
						if( $tax = get_taxonomy( $taxonomy ) ){
							if( ! $tax->public ) continue;

							$value = 'btn_tax_' . $taxonomy ;
							$ret->$post_type->$value = $tax->labels->name;
						}
					}
				}

				if( count( array_keys( (array)$ret->$post_type ) ) == 0 ){
					unset( $ret->$post_type ); //empty post type. no taxonomies
				}
			}
		}

		return $ret;
	}

	function get_post_type_options(){
		global $rhc_plugin;
		$post_types = $rhc_plugin->get_option('post_types',array());
		$post_types = is_array($post_types)?$post_types:array();
		$post_types = apply_filters('rhc_calendar_metabox_post_types',$post_types);

		if ( !in_array( RHC_EVENTS, $post_types ) ) {
			$post_types[] = RHC_EVENTS;
		}

		$options = array();
		foreach( $post_types as $post_type ){
			if( $o = get_post_type_object( $post_type ) ){
				$options[] = (object)array(
					'label' => $o->labels->name,
					'value' => $post_type
				);
			}
		}

		return $options;
	}

	function get_supe_template_options(){
		$disabled_controls = '.rhc-inspector-supe-tab_dropdown_filter,#rhc-gutenberg-inspector-control-nav';

		$templates = array(
			(object)array(
				'label' => 'Upcoming Events (Style 1)',
				'value' => 'widget_upcoming_events.php',
				'disabled_controls' => $disabled_controls,
			),
			(object)array(
				'label' => 'Upcoming Events (Style 2)',
				'value' => 'widget_upcoming_events_a.php',
				'disabled_controls' => $disabled_controls,
			),
			(object)array(
				'label' => 'Upcoming Events (Style 3)',
				'value' => 'widget_upcoming_events_a_end.php',
				'disabled_controls' => $disabled_controls,
			),
			(object)array(
				'label' => 'Upcoming Events (Style 4)',
				'value' => 'widget_upcoming_events_a_end_range.php',
				'disabled_controls' => $disabled_controls,
			),
			(object)array(
				'label' => 'Upcoming Events (Style 5)',
				'value' => 'widget_upcoming_events_a1.php',
				'disabled_controls' => $disabled_controls,
			),
			(object)array(
				'label' => 'Upcoming Events (Style 6)',
				'value' => 'widget_upcoming_events_agenda_b.php',
				'disabled_controls' => $disabled_controls,
			),
			(object)array(
				'label' => 'Upcoming Events (Style 7)',
				'value' => 'widget_upcoming_events_agenda_b2.php',
				'disabled_controls' => $disabled_controls,
			),
			/*
			(object)array(
				'label' => 'json',
				'value' => 'json',
				'disabled_controls' => $disabled_controls,
			),
			(object)array(
				'label' => 'php',
				'value' => 'php',
				'disabled_controls' => $disabled_controls,
			),
			*/
		);

		$ret = apply_filters( 'rhc_gutenberg_supe_template_options', $templates );

		array_unshift( $ret, (object)array(
			'label' => __('default'),
			'value' => ''
		) );

		return $ret;
	}

	function get_attributes_for_register_block_type(){

		return array(
				'test'	=> array(
					'type'	=> 'string'
				),
				'page'	=> array(
					'type'	=> 'string'
				),
				'number'	=> array(
					'type'		=> 'integer',
					'default'	=> 5,
				),
				'taxonomy'	=> array(
					'type'	=> 'string'
				),
				'terms'	=> array(
					'type'	=> 'string'
				),
				'terms_children'	=> array(
					'type'	=> 'string'
				),
				'template'	=> array(
					'type'	=> 'string'
				),
				'class'	=> array(
					'type'	=> 'string'
				),
				'prefix'	=> array(
					'type'	=> 'string'
				),
				'parse_postmeta'	=> array(
					'type'	=> 'string'
				),
				'parse_taxonomy'	=> array(
					'type'	=> 'string'
				),
				'parse_taxonomymeta'	=> array(
					'type'	=> 'string'
				),
				'order'	=> array(
					'type'	=> 'string'
				),
				'date'	=> array(
					'type'	=> 'string',
					'default' => 'NOW'
				),
				'date_end'	=> array(
					'type'	=> 'string'
				),
				'date_compare'	=> array(
					'type'	=> 'string'
				),
				'horizon'	=> array(
					'type'		=> 'string',
					'default'	=> 'hour'
				),
				'allday'	=> array(
					'type'	=> 'string',
					'default' => ''
				),
				'no_events_message'	=> array(
					'type'	=> 'string'
				),
				'post_status'	=> array(
					'type'	=> 'string'
				),
				'post_type'	=> array(
					'type'	=> 'string',
					'default' => ''
				),
				'author'	=> array(
					'type'	=> 'string'
				),
				'author_current'	=> array(
					'type'	=> 'string'
				),
				'do_shortcode'	=> array(
					'type'	=> 'string'
				),
				'the_content'	=> array(
					'type'	=> 'string'
				),
				'separator'	=> array(
					'type'	=> 'string'
				),
				'holder'	=> array(
					'type'	=> 'string'
				),
				'dayspast'	=> array(
					'type'	=> 'string'
				),
				'premiere'	=> array(
					'type'	=> 'string',
					'default' => '0'
				),
				'auto'	=> array(
					'type'	=> 'string',
					'auto'	=> '0'
				),
				'hideempty'	=> array(
					'type'	=> 'string',
					'default' => '0'
				),
				'feed'	=> array(
					'type'	=> 'string',
					'default' => '0'
				),
				'words'	=> array(
					'type'	=> 'string'
				),
				'render_images'	=> array(
					'type'	=> 'string'
				),
				'calendar_url'	=> array(
					'type'	=> 'string'
				),
				'loading_overlay'	=> array(
					'type'	=> 'string'
				),
				'for_sidebar'	=> array(
					'type'	=> 'string'
				),
				'post_id'	=> array(
					'type'	=> 'string'
				),
				'current_post'	=> array(
					'type'	=> 'string'
				),
				'rdate'	=> array(
					'type'	=> 'string'
				),
				'js_init_script'	=> array(
					'type'	=> 'string'
				),
				'vc_js_init_script'	=> array(
					'type'	=> 'string'
				),
				'nav'	=> array(
					'type'	=> 'integer',
					'default' => ''
				),
				'tax_and_filter'	=> array(
					'type'	=> 'string'
				),
				'header'	=> array(
					'type'	=> 'string',
					'default' => ''
				),
				'hierarchical_filter'	=> array(
					'type'	=> 'string'
				),
				'terms_hide_empty'	=> array(
					'type'	=> 'string'
				),
				'tax_filter_multiple'	=> array(
					'type'	=> 'string'
				),
				'geo_radius'	=> array(
					'type'	=> 'string'
				),
				'geo_center'	=> array(
					'type'	=> 'string'
				),
				'local_tz'	=> array(
					'type'	=> 'string',
					'default' => ''
				),
				'tax_and_filtering'	=> array(
					'type'	=> 'string'
				),
				'term_post_count'	=> array(
					'type'	=> 'string'
				),
				'btn_tax_parent_only'	=> array(
					'type'	=> 'string'
				),
				'showimage'	=> array(
					'type'	=> 'string'
				),
				'capability' => array(
					'type'	=> 'string'
				),
				'conditional_tag' => array(
					'type'	=> 'array',
					'default' => array(),
					'items'	=> null
				),
			);

	}
}