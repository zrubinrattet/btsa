;;
(function($){

	$.fn.extend({
		insertAtCaret: function(myValue){
		  	var obj;
		  	if( typeof this[0].name !='undefined' ) obj = this[0];
		  	else obj = this;
		
		  	if ($.browser.msie) {
		    	obj.focus();
		    	sel = document.selection.createRange();
		    	sel.text = myValue;
		    	obj.focus();
		    }
		 	else if ($.browser.mozilla || $.browser.webkit) {
		    	var startPos = obj.selectionStart;
		    	var endPos = obj.selectionEnd;
		    	var scrollTop = obj.scrollTop;
		    	obj.value = obj.value.substring(0, startPos)+myValue+obj.value.substring(endPos,obj.value.length);
		    	obj.focus();
		    	obj.selectionStart = startPos + myValue.length;
		    	obj.selectionEnd = startPos + myValue.length;
		    	obj.scrollTop = scrollTop;
		  	} else {
		    	obj.value += myValue;
		    	obj.focus();
		   	}
			return this.each(function(){});
		}
	});
	
window.rc_dateformat_helper = {
	open: function( sel ){
		var _id = $(sel).attr('id');
		var helper_id = _id+'_helper';
		if( $('#'+helper_id).length == 0 ){
			$(rc_dateformat_helper.template)
				.attr('id', helper_id )
				.attr('rel',_id)
				.hide()
				.appendTo( $(sel).parent() )
			;			
		}
	
		const helper = $('#'+helper_id);
	
		if( helper.is(':visible') ){
			helper.hide();
			return;
		}
	
		$(sel).attr('placeholder', $(sel).attr('rel') );
	
		$(sel).parent().find('.rhc_button').click(function(e){	
			$('#'+_id).insertAtCaret( $(this).val() ).trigger('change');
		});
	
		$(sel).parent().find('.rhc_button_default').click(function(e){
			$('#'+_id).val( $('#'+_id).attr('rel') ).trigger('change');	
		});
	
		$(sel).parent().find('.rhc_button_space').click(function(e){
			$('#'+_id).insertAtCaret( ' ' ).trigger('change');
		});
	
		$(sel).parent().find('.rhc_button_clear').click(function(e){
			$('#'+_id).val('').trigger('change');	
		});
	
		$(sel).parent().find('.rhc_button_close').click(function(e){
			$('.rc_dateformat_helper_holder').hide();
		});
	
		$(sel).change(function(e){
			var _now = new Date();
			var _formatted = $.fullCalendar.formatDate(_now,$(sel).val());
			$(sel).parent().find('.rc_dateformat_preview').html(_formatted);	
		});
	
		$(sel).focus(function(e){
			$('.rc_dateformat_helper_holder').hide();
			helper.show();
			$(sel).trigger('change');
		});
		
		$('.rc_dateformat_helper_holder').hide(function(){
			helper.show();		
		});
	},
	
	template: `
<div class="rc_dateformat_helper_holder">
	<div id="dateformat_helper_base" class="rc_dateformat_helper">
		<div class="helper-arrow-holder">
			<div class="helper-arrow"></div>
			<div class="helper-arrow-border"></div>
		</div>
		
		<div class="rc_dateformat_helper_content postbox">
			<div class="rc_dateformat_preview_cont">
				<label class="rc_preview_label">${__('Preview:')}</label>
				<div class="rc_dateformat_preview postbox"></div>
			</div>
			<div class="rc_dateformat_buttons">
				<input type="button" class="rhc_button"  title="date number" value="d" rel="d" />
				<input type="button" class="rhc_button"  title="date number, 2 digits" value="dd" rel="dd" />
				<input type="button" class="rhc_button"  title="date name, short" value="ddd" rel="ddd" />
				<input type="button" class="rhc_button"  title="date name, full" value="dddd" rel="dddd" />
				<input type="button" class="rhc_button"  title="month number" value="M" rel="M" />
				<input type="button" class="rhc_button"  title="month number, 2 digits" value="MM" rel="MM" />
				<input type="button" class="rhc_button"  title="month name, short" value="MMM" rel="MMM" />
				<input type="button" class="rhc_button"  title="month name, full" value="MMMM" rel="MMMM" />
				<input type="button" class="rhc_button"  title="year, 2 digits" value="yy" rel="yy" />
				<input type="button" class="rhc_button"  title="year, 4 digits" value="yyyy" rel="yyyy" />
				<input type="button" class="rhc_button"  title="hours, 12 hour format" value="h" rel="h" />
				<input type="button" class="rhc_button"  title="hours, 12 hour format, 2 digits" value="hh" rel="hh" />
				<input type="button" class="rhc_button"  title="hours, 24 hour format" value="H" rel="H" />
				<input type="button" class="rhc_button"  title="hours, 24 hour format, 2 digits" value="HH" rel="HH" />
				<input type="button" class="rhc_button"  title="colon" value=":" rel=":" />
				<input type="button" class="rhc_button"  title="minutes" value="m" rel="m" />
				<input type="button" class="rhc_button"  title="minutes, 2 digits" value="mm" rel="mm" />
				<input type="button" class="rhc_button"  title="a or p" value="t" rel="t" />
				<input type="button" class="rhc_button"  title="am or pm" value="tt" rel="tt" />
				<input type="button" class="rhc_button"  title="A or P" value="T" rel="T" />
				<input type="button" class="rhc_button"  title="AM or PM" value="TT" rel="TT" />
				<input type="button" class="rhc_button"  title="ISO8601 format" value="u" rel="u" />
				<input type="button" class="rhc_button"  title="Single quote" value="''" rel="''" />
				<input type="button" class="rhc_button"  title="comma" value="," rel="," />
				<input type="button" class="rhc_button"  title="forward slash" value="/" rel="/" />
				<input type="button" class="rhc_button"  title="dot" value="." rel="." />
				<input type="button" class="rhc_button_default" title="default format" value="default"  />
				<input type="button" class="rhc_button_clear" title="clear value" value="clear"  />
				<input type="button" class="rhc_button_space" title="space" value="&nbsp;&nbsp;&nbsp;space&nbsp;&nbsp;&nbsp;" />
			</div>
			<div class="rc_dateformat_footer">
				<input type="button" class="button-secondary rhc_button_close" value="done"  />
			</div>
		</div>
	</div>
</div>
`,
}

})(jQuery);
