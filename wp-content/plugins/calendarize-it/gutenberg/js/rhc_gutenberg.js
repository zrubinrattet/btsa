;;
( function($) {

var el = wp.element.createElement,
    registerBlockType = wp.blocks.registerBlockType,
    blockStyle = { backgroundColor: '#900', color: '#fff', padding: '20px' }
    BlockControls = wp.editor.BlockControls,
    AlignmentToolbar = wp.editor.AlignmentToolbar,
    InspectorControls = wp.editor.InspectorControls,
    ToggleControl = wp.components.ToggleControl,
    TextControl = wp.components.TextControl,
    RangeControl = wp.components.RangeControl,
    SelectControl = wp.components.SelectControl,
    CheckboxControl = wp.components.CheckboxControl,
    PanelBody	= wp.components.PanelBody,
    __ = wp.i18n.__,
    ServerSideRender = wp.components.ServerSideRender,
    parameters = {},
    Dashicon = wp.components.Dashicon,
    Tooltip = wp.components.Tooltip
    ;

const iconEl = el('svg', { width: 24, height: 24 },
  el('path', { d: "M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V8h16v13z" } ),
  el('path', { d: "M0 0h24v24H0z", fill: "none" } )
);

var timeout_rhc_calendarizeit=null;

registerBlockType( 'calendarize-it/calendarizeit', {
    title: 'Calendarize it!',

    icon: iconEl,

    category: 'calendarize-it',

	description: __('Flexible and feature-rich calendar for WordPress'),

	attributes: RHC_GUTENBERG.attributes,

    edit: function( props ) {
		if( ! props.buffer ) props.buffer = {} ;
// console.log('edit props',props);

		if( timeout_rhc_calendarizeit )
			clearTimeout( timeout_rhc_calendarizeit );

		var textcontrol_settimeout_id=null;
		var textcontrol_settimeout_value=null;

		var atts = props.attributes;
//console.log( 'atts', atts );
//console.log( 'xx', typeof props.attributes );
//console.log( 'xx', typeof props.attributes.skipmonths, props.attributes.skipmonths );
//console.log( 'RHC_GUTENBERG.attributes', RHC_GUTENBERG.attributes );

		if( !props.attributes.conditional_tag ){
			props.attributes.conditional_tag = [];
		}
		if( !props.attributes.skipmonths ){
			props.attributes.skipmonths = [];
		}
		if( !props.attributes.hiddendays ){
			props.attributes.hiddendays = [];
		}
		if( !props.attributes.post_type ){
			props.attributes.post_type = [];
		}

		url = RHC_GUTENBERG.ajax_url + '?';

		o = {
			'context':'edit',
			'attributes':{}
		};
		$.each(props.attributes, function(i,attr){
			o.attributes[i] = attr;
		});

		url = url + '&post_id=' + wp.data.select ( 'core/editor' ).getCurrentPostId();

		url = url + $.param(o) ;

		var el_id = '_' + Math.random().toString(36).substr(2, 9);

		args = {
			'action': 'rhc_gutenberg_calendarizeit',
			'post_id': wp.data.select ( 'core/editor' ).getCurrentPostId(),
			'data' : o
		}

		timeout_rhc_calendarizeit = setTimeout(function(){
			$.post( ajaxurl, args, function(data){
				sel = '#' + el_id;
				$(sel).html(data);
			},'html');
		},1000);

		function _post_types(){
			
			output = [];
			
			if( RHC_GUTENBERG.post_types.length > 0 ){
				$.each( RHC_GUTENBERG.post_types, function(i,p){
					tmp = el(
						CheckboxControl,
						{
							label:	p.label,
							name: 'post_type[]',
							checked: !!+(props.attributes.post_type.indexOf( p.post_type )>-1),
							onChange:function(e) {
								d = props.attributes.post_type.slice();
					
								var index = d.indexOf( p.post_type );
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push( p.post_type );
								}										

								props.setAttributes({
									post_type: d
								});	
							}							
						}
					);
					
					output.push( tmp );				
				});
			}
		
			return output;
		}

        return [


            el( 'div', {
            	'id': el_id,
            	key: "editable",
            	class: 'rhc-gutenberg-preview',
            }, 'loading' ),

			el(
				InspectorControls,
				{ key: 'inspector' },
				

//------- START

                
				el( 
					PanelBody, {
						title: __( 'General' ),
						className: 'rhc-inspector-vc_tab_general',
						initialOpen: true,
					},
		
					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Default view'),
							value:  props.attributes.defaultview ,
							options: RHC_GUTENBERG.defaultview_options,
							 
							onChange: function(value){
								props.setAttributes({
									defaultview: value
								});	
							}
						}
					),			
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Aspect ratio'),
							placeholder: props.attributes.aspectratio,
							 
							value: props.attributes.aspectratio,
							onChange: function(value){
								props.setAttributes({
									aspectratio: value
								});		
							},
							'data-cal_id': 'aspectratio',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Left header'),
							placeholder: props.attributes.header_left,
							 
							value: props.attributes.header_left,
							onChange: function(value){
								props.setAttributes({
									header_left: value
								});		
							},
							'data-cal_id': 'header_left',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Center header'),
							placeholder: props.attributes.header_center,
							 
							value: props.attributes.header_center,
							onChange: function(value){
								props.setAttributes({
									header_center: value
								});		
							},
							'data-cal_id': 'header_center',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Defaults to: <b>month,agendaWeek,agendaDay,rhc_event</b>. Also available: basicWeek and basicDay')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Right header') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							placeholder: props.attributes.header_right,
							 
							value: props.attributes.header_right,
							onChange: function(value){
								props.setAttributes({
									header_right: value
								});		
							},
							'data-cal_id': 'header_right',
						}
					),
					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Choose yes if only want to display upcoming events.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Upcoming only (all views)') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.upcoming,
														onChange:function(e) {
								return props.setAttributes({
									upcoming: e ? '1' : '0'	
								})
							}
						}
					),		
					el(
						SelectControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('By default allday events are rendered ordered by title. Choose color to group by color, or by order to use the menu order from the edit event screen.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Allday group') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							value:  props.attributes.allday_group ,
							options: 	[
								
									{
										label: __('By title(default)'),
										value: ''
									},
								
									{
										label: __('By event color'),
										value: 'color'
									},
								
									{
										label: __('By menu order'),
										value: 'order'
									},
								],
							 
							onChange: function(value){
								props.setAttributes({
									allday_group: value
								});	
							}
						}
					),			
					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Choose yes if you want to remove past weeks from the Month view. Only applicable when Upcoming only is also enabled.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Month View, trim past weeks') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.upcoming_trim_past,
														onChange:function(e) {
								return props.setAttributes({
									upcoming_trim_past: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Choose yes if you want the time to be converted to the browsers timezone.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Local timezone') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.local_tz,
														onChange:function(e) {
								return props.setAttributes({
									local_tz: e ? '1' : '0'	
								})
							}
						}
					),					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Disable months')
					),
					el(
						CheckboxControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Check months that you do NOT want to show. Pressing prev or next button in calendar will skip to the next available month.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('January') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('0')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('0');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('0');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('February'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('1')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('1');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('1');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('March'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('2')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('2');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('2');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('April'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('3')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('3');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('3');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('May'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('4')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('4');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('4');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('June'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('5')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('5');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('5');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('July'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('6')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('6');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('6');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('August'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('7')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('7');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('7');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('September'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('8')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('8');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('8');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('October'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('9')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('9');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('9');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('November'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('10')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('10');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('10');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('December'),
							name: 'skipmonths[]',
							checked: !!+(props.attributes.skipmonths.indexOf('11')>-1),
							onChange:function(e) {
								d = props.attributes.skipmonths.slice();
					
								var index = d.indexOf('11');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('11');
								}										

								props.setAttributes({
									skipmonths: d
								});	
							}							
						}
					),					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Hide days from view')
					),
					el(
						CheckboxControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('<p>Check days that you do NOT want to show.</p><p><strong>Shortcode argument:</strong>&nbsp;hiddendays</p><p><strong>Shortcode values:</strong>&nbsp;Comma separated numeric values.  0 for Sunday, 1 for Monday, 2 for Tuesday, 3 for Wednsday, 4 for Thursday, 5 for Friday, 6 for Saturday</p><p><strong>Example(hiding sunday and monday):</strong>&nbsp;&#91;calendarizeit hiddendays=\'0,1\'&#93;</p>')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Sunday') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							name: 'hiddendays[]',
							checked: !!+(props.attributes.hiddendays.indexOf('0')>-1),
							onChange:function(e) {
								d = props.attributes.hiddendays.slice();
					
								var index = d.indexOf('0');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('0');
								}										

								props.setAttributes({
									hiddendays: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('Monday'),
							name: 'hiddendays[]',
							checked: !!+(props.attributes.hiddendays.indexOf('1')>-1),
							onChange:function(e) {
								d = props.attributes.hiddendays.slice();
					
								var index = d.indexOf('1');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('1');
								}										

								props.setAttributes({
									hiddendays: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('Tuesday'),
							name: 'hiddendays[]',
							checked: !!+(props.attributes.hiddendays.indexOf('2')>-1),
							onChange:function(e) {
								d = props.attributes.hiddendays.slice();
					
								var index = d.indexOf('2');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('2');
								}										

								props.setAttributes({
									hiddendays: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('Wednesday'),
							name: 'hiddendays[]',
							checked: !!+(props.attributes.hiddendays.indexOf('3')>-1),
							onChange:function(e) {
								d = props.attributes.hiddendays.slice();
					
								var index = d.indexOf('3');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('3');
								}										

								props.setAttributes({
									hiddendays: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('Thursday'),
							name: 'hiddendays[]',
							checked: !!+(props.attributes.hiddendays.indexOf('4')>-1),
							onChange:function(e) {
								d = props.attributes.hiddendays.slice();
					
								var index = d.indexOf('4');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('4');
								}										

								props.setAttributes({
									hiddendays: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('Friday'),
							name: 'hiddendays[]',
							checked: !!+(props.attributes.hiddendays.indexOf('5')>-1),
							onChange:function(e) {
								d = props.attributes.hiddendays.slice();
					
								var index = d.indexOf('5');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('5');
								}										

								props.setAttributes({
									hiddendays: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('Saturday'),
							name: 'hiddendays[]',
							checked: !!+(props.attributes.hiddendays.indexOf('6')>-1),
							onChange:function(e) {
								d = props.attributes.hiddendays.slice();
					
								var index = d.indexOf('6');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('6');
								}										

								props.setAttributes({
									hiddendays: d
								});	
							}							
						}
					),					el(
						ToggleControl,
						{
							label:	__('Show weekends'),
							checked: !!+props.attributes.weekends,
														onChange:function(e) {
								return props.setAttributes({
									weekends: e ? '1' : '0'	
								})
							}
						}
					),		
					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Calendar first day'),
							value:  props.attributes.firstday ,
							options: 	[
								
									{
										label: __('Sunday'),
										value: '0'
									},
								
									{
										label: __('Monday'),
										value: '1'
									},
								
									{
										label: __('Tuesday'),
										value: '2'
									},
								
									{
										label: __('Wednesday'),
										value: '3'
									},
								
									{
										label: __('Thursday'),
										value: '4'
									},
								
									{
										label: __('Friday'),
										value: '5'
									},
								
									{
										label: __('Saturday'),
										value: '6'
									},
								],
							 
							onChange: function(value){
								props.setAttributes({
									firstday: value
								});	
							}
						}
					),			
					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Show a pre-loader on the calendar viewport when fetching events')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Show pre-loader') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.loading_overlay,
														onChange:function(e) {
								return props.setAttributes({
									loading_overlay: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('<p>Enables displaying week numbers on the calendar views.</p><p><b>Week number label</b>: By default it is "W", this is the label shown on the week column in month view. In agenda views it is shown in the top left corner.</p>')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Enable week numbers') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.week_numbers,
														onChange:function(e) {
								return props.setAttributes({
									week_numbers: e ? '1' : '0'	
								})
							}
						}
					),					el(
						TextControl,
						{
							type: 'text',
							label:	__('Week number label'),
							placeholder: props.attributes.week_numbers_title,
							 
							value: props.attributes.week_numbers_title,
							onChange: function(value){
								props.setAttributes({
									week_numbers_title: value
								});		
							},
							'data-cal_id': 'week_numbers_title',
						}
					),
		
				),
				el( 
					PanelBody, {
						title: __( 'Filtering' ),
						className: 'rhc-inspector-vc_tab_taxonomy_filter',
						initialOpen: false,
					},
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Filter by taxonomy terms')
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Use in combination with terms to display events from a specific taxonomy term. Default Taxonomies are calendar, organizer, and venue. This also supports any custom taxonomies you have created.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Taxonomy') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							placeholder: props.attributes.taxonomy,
							 
							value: props.attributes.taxonomy,
							onChange: function(value){
								props.setAttributes({
									taxonomy: value
								});		
							},
							'data-cal_id': 'taxonomy',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Specify comma separated term slugs.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Terms') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							placeholder: props.attributes.terms,
							 
							value: props.attributes.terms,
							onChange: function(value){
								props.setAttributes({
									terms: value
								});		
							},
							'data-cal_id': 'terms',
						}
					),
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Filter by author')
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Calendarize it! makes it possible for you to display individual calendars for each user in WordPress. Use `current_user` to display events created by the current logged in user. Filter by specific Author by adding the user_id. Find the user_id by hovering the cursor over the username in the users table and look in the bottom of your browser where it shows the URL. Write multiple users separated by comma. ')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Author') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							placeholder: props.attributes.author,
							 
							value: props.attributes.author,
							onChange: function(value){
								props.setAttributes({
									author: value
								});		
							},
							'data-cal_id': 'author',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Message to display when using `current_user` as author, but the visitor is not logged in')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Personal calendar message') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							placeholder: props.attributes.personal_calendar_message,
							 
							value: props.attributes.personal_calendar_message,
							onChange: function(value){
								props.setAttributes({
									personal_calendar_message: value
								});		
							},
							'data-cal_id': 'personal_calendar_message',
						}
					),
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Filter by post types')
					),
					
					_post_types(),
					/*
					el(
						CheckboxControl,
						{
							label:	__('Advent Item'),
							name: 'post_type[]',
							checked: !!+(props.attributes.post_type.indexOf('advent')>-1),
							onChange:function(e) {
								d = props.attributes.post_type.slice();
					
								var index = d.indexOf('advent');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('advent');
								}										

								props.setAttributes({
									post_type: d
								});	
							}							
						}
					),					
					
					el(
						CheckboxControl,
						{
							label:	__('Timetable Item'),
							name: 'post_type[]',
							checked: !!+(props.attributes.post_type.indexOf('rhc-timetable')>-1),
							onChange:function(e) {
								d = props.attributes.post_type.slice();
					
								var index = d.indexOf('rhc-timetable');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('rhc-timetable');
								}										

								props.setAttributes({
									post_type: d
								});	
							}							
						}
					),					
					
					el(
						CheckboxControl,
						{
							label:	__('Events'),
							name: 'post_type[]',
							checked: !!+(props.attributes.post_type.indexOf('events')>-1),
							onChange:function(e) {
								d = props.attributes.post_type.slice();
					
								var index = d.indexOf('events');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('events');
								}										

								props.setAttributes({
									post_type: d
								});	
							}							
						}
					),	
					*/
						
					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Feed'),
							value:  props.attributes.feed ,
							options: 	[
								
									{
										label: __('default'),
										value: ''
									},
								
									{
										label: __('Do not show external sources'),
										value: '0'
									},
								
									{
										label: __('Only show external sources'),
										value: '1'
									},
								
									{
										label: __('Show both local and external sources'),
										value: '2'
									},
								],
							 
							onChange: function(value){
								props.setAttributes({
									feed: value
								});	
							}
						}
					),			
		
				),
				el( 
					PanelBody, {
						title: __( 'Labels' ),
						className: 'rhc-inspector-vc_tab_labels',
						initialOpen: false,
					},
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Calendar labels')
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Month names'),
							placeholder: props.attributes.monthnames,
							 
							value: props.attributes.monthnames,
							onChange: function(value){
								props.setAttributes({
									monthnames: value
								});		
							},
							'data-cal_id': 'monthnames',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Short month names'),
							placeholder: props.attributes.monthnamesshort,
							 
							value: props.attributes.monthnamesshort,
							onChange: function(value){
								props.setAttributes({
									monthnamesshort: value
								});		
							},
							'data-cal_id': 'monthnamesshort',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Day names'),
							placeholder: props.attributes.daynames,
							 
							value: props.attributes.daynames,
							onChange: function(value){
								props.setAttributes({
									daynames: value
								});		
							},
							'data-cal_id': 'daynames',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Short day names'),
							placeholder: props.attributes.daynamesshort,
							 
							value: props.attributes.daynamesshort,
							onChange: function(value){
								props.setAttributes({
									daynamesshort: value
								});		
							},
							'data-cal_id': 'daynamesshort',
						}
					),
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Button labels')
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Button Today'),
							placeholder: props.attributes.button_text_today,
							 
							value: props.attributes.button_text_today,
							onChange: function(value){
								props.setAttributes({
									button_text_today: value
								});		
							},
							'data-cal_id': 'button_text_today',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Button Month'),
							placeholder: props.attributes.button_text_month,
							 
							value: props.attributes.button_text_month,
							onChange: function(value){
								props.setAttributes({
									button_text_month: value
								});		
							},
							'data-cal_id': 'button_text_month',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Button Day'),
							placeholder: props.attributes.button_text_day,
							 
							value: props.attributes.button_text_day,
							onChange: function(value){
								props.setAttributes({
									button_text_day: value
								});		
							},
							'data-cal_id': 'button_text_day',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Button Week'),
							placeholder: props.attributes.button_text_week,
							 
							value: props.attributes.button_text_week,
							onChange: function(value){
								props.setAttributes({
									button_text_week: value
								});		
							},
							'data-cal_id': 'button_text_week',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Button Calendar'),
							placeholder: props.attributes.button_text_calendar,
							 
							value: props.attributes.button_text_calendar,
							onChange: function(value){
								props.setAttributes({
									button_text_calendar: value
								});		
							},
							'data-cal_id': 'button_text_calendar',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Button event'),
							placeholder: props.attributes.button_text_event,
							 
							value: props.attributes.button_text_event,
							onChange: function(value){
								props.setAttributes({
									button_text_event: value
								});		
							},
							'data-cal_id': 'button_text_event',
						}
					),
		
				),
				el( 
					PanelBody, {
						title: __( 'Month' ),
						className: 'rhc-inspector-vc_tab_month',
						initialOpen: false,
					},
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Month view')
					),
		
					el(
						SelectControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Determines the number of weeks displayed in the calendar.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Week mode') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							value:  props.attributes.week_mode ,
							options: 	[
								
									{
										label: __('Fixed'),
										value: 'fixed'
									},
								
									{
										label: __('Liquid'),
										value: 'liquid'
									},
								
									{
										label: __('Variable'),
										value: 'variable'
									},
								],
							 
							onChange: function(value){
								props.setAttributes({
									week_mode: value
								});	
							}
						}
					),			
					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Turn this feature on if you want the background color of the day to match the color of the event.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Background match event color') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.matchbackground,
														onChange:function(e) {
								return props.setAttributes({
									matchbackground: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Turn this feature off if you dont want the event titles to render. This is to be used in combination with the match background color. Only applicable to month view.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Render events') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.render_events,
														onChange:function(e) {
								return props.setAttributes({
									render_events: e ? '1' : '0'	
								})
							}
						}
					),					el(
						TextControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Replace the event title with a fixed label.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Fixed title') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							placeholder: props.attributes.fixed_title,
							 
							value: props.attributes.fixed_title,
							onChange: function(value){
								props.setAttributes({
									fixed_title: value
								});		
							},
							'data-cal_id': 'fixed_title',
						}
					),
					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Turn this feature on to enable inserting the "Month view image" in events that has this image set. Then "Month view image" is set in a metabox when editing an event. The metabox has to be enabled separately on the next option.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Month view image') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.month_event_image,
														onChange:function(e) {
								return props.setAttributes({
									month_event_image: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Turn this feature on to enable the "Month view image" metabox.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Month view image metabox') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.month_event_image_metabox,
														onChange:function(e) {
								return props.setAttributes({
									month_event_image_metabox: e ? '1' : '0'	
								})
							}
						}
					),		
					el(
						SelectControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('<p>Determines how much time an event can get past midnight without displaying it on that day.</p><p>Shortcode argument: next_day_threshold</p><p>Possible values: 0,1,2... 12</p>')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Next day threshold') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							value:  props.attributes.next_day_threshold ,
							options: 	[
								
									{
										label: __('Not used'),
										value: ''
									},
								
									{
										label: __('00:00'),
										value: '00'
									},
								
									{
										label: __('01:00'),
										value: '1'
									},
								
									{
										label: __('02:00'),
										value: '2'
									},
								
									{
										label: __('03:00'),
										value: '3'
									},
								
									{
										label: __('04:00'),
										value: '4'
									},
								
									{
										label: __('05:00'),
										value: '5'
									},
								
									{
										label: __('06:00'),
										value: '6'
									},
								
									{
										label: __('07:00'),
										value: '7'
									},
								
									{
										label: __('08:00'),
										value: '8'
									},
								
									{
										label: __('09:00'),
										value: '9'
									},
								
									{
										label: __('10:00'),
										value: '10'
									},
								
									{
										label: __('11:00'),
										value: '11'
									},
								
									{
										label: __('12:00'),
										value: '12'
									},
								],
							 
							onChange: function(value){
								props.setAttributes({
									next_day_threshold: value
								});	
							}
						}
					),			
					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('In month view, some dates from other months are rendered to complete the week. Choose yes to display events on it, or no to leave them blank.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Show other month events') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.showothermonth,
														onChange:function(e) {
								return props.setAttributes({
									showothermonth: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Turn this feature on if you do not want the time to be rendered.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Hide time') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.month_hide_time,
														onChange:function(e) {
								return props.setAttributes({
									month_hide_time: e ? '1' : '0'	
								})
							}
						}
					),		
				),
				el( 
					PanelBody, {
						title: __( 'Agenda' ),
						className: 'rhc-inspector-vc_tab_agenda',
						initialOpen: false,
					},
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Agenda view (week and day view)')
					),
					el(
						ToggleControl,
						{
							label:	__('Show all-day slot'),
							checked: !!+props.attributes.alldayslot,
														onChange:function(e) {
								return props.setAttributes({
									alldayslot: e ? '1' : '0'	
								})
							}
						}
					),					el(
						TextControl,
						{
							type: 'text',
							label:	__('all-day label'),
							placeholder: props.attributes.alldaytext,
							 
							value: props.attributes.alldaytext,
							onChange: function(value){
								props.setAttributes({
									alldaytext: value
								});		
							},
							'data-cal_id': 'alldaytext',
						}
					),
					el(
						RangeControl,
						{
							label:	__('First hour'),
							 
							onChange: function(value){
								props.setAttributes({
									firsthour: value.toString()
								});	
							},
							min: 0,
							max: 24,
							step: 1,
							value: Number( props.attributes.firsthour ),
						}
					),
					el(
						RangeControl,
						{
							label:	__('Slot minutes'),
							 
							onChange: function(value){
								props.setAttributes({
									slotminutes: value.toString()
								});	
							},
							min: 5,
							max: 60,
							step: 1,
							value: Number( props.attributes.slotminutes ),
						}
					),
					el(
						RangeControl,
						{
							label:	__('Minimun displayed time'),
							 
							onChange: function(value){
								props.setAttributes({
									mintime: value.toString()
								});	
							},
							min: 0,
							max: 24,
							step: 1,
							value: Number( props.attributes.mintime ),
						}
					),
					el(
						RangeControl,
						{
							label:	__('Maximun displayed time'),
							 
							onChange: function(value){
								props.setAttributes({
									maxtime: value.toString()
								});	
							},
							min: 0,
							max: 24,
							step: 1,
							value: Number( props.attributes.maxtime ),
						}
					),
		
				),
				el( 
					PanelBody, {
						title: __( 'Event list' ),
						className: 'rhc-inspector-vc_tab_event_list',
						initialOpen: false,
					},
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Event list view')
					),
					el(
						ToggleControl,
						{
							label:	__('Show same date header'),
							checked: !!+props.attributes.eventlistshowheader,
														onChange:function(e) {
								return props.setAttributes({
									eventlistshowheader: e ? '1' : '0'	
								})
							}
						}
					),					el(
						TextControl,
						{
							type: 'text',
							label:	__('No events text'),
							placeholder: props.attributes.eventlistnoeventstext,
							 
							value: props.attributes.eventlistnoeventstext,
							onChange: function(value){
								props.setAttributes({
									eventlistnoeventstext: value
								});		
							},
							'data-cal_id': 'eventlistnoeventstext',
						}
					),
					el(
						ToggleControl,
						{
							label:	__('Upcoming only'),
							checked: !!+props.attributes.eventlistupcoming,
														onChange:function(e) {
								return props.setAttributes({
									eventlistupcoming: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Reverse order'),
							checked: !!+props.attributes.eventlistreverse,
														onChange:function(e) {
								return props.setAttributes({
									eventlistreverse: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Show multi month events'),
							checked: !!+props.attributes.eventlistoutofrange,
														onChange:function(e) {
								return props.setAttributes({
									eventlistoutofrange: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Remove ended'),
							checked: !!+props.attributes.eventlistremoveended,
														onChange:function(e) {
								return props.setAttributes({
									eventlistremoveended: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Stack behaviour'),
							checked: !!+props.attributes.eventliststack,
														onChange:function(e) {
								return props.setAttributes({
									eventliststack: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Stack autoload'),
							checked: !!+props.attributes.eventlistauto,
														onChange:function(e) {
								return props.setAttributes({
									eventlistauto: e ? '1' : '0'	
								})
							}
						}
					),					el(
						TextControl,
						{
							type: 'text',
							label:	__('Custom Days per page(optional)'),
							placeholder: props.attributes.eventlistdelta,
							 
							value: props.attributes.eventlistdelta,
							onChange: function(value){
								props.setAttributes({
									eventlistdelta: value
								});		
							},
							'data-cal_id': 'eventlistdelta',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Months ahead to show(optional)'),
							placeholder: props.attributes.eventlistmonthsahead,
							 
							value: props.attributes.eventlistmonthsahead,
							onChange: function(value){
								props.setAttributes({
									eventlistmonthsahead: value
								});		
							},
							'data-cal_id': 'eventlistmonthsahead',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Days ahead to show(optional)'),
							placeholder: props.attributes.eventlistdaysahead,
							 
							value: props.attributes.eventlistdaysahead,
							onChange: function(value){
								props.setAttributes({
									eventlistdaysahead: value
								});		
							},
							'data-cal_id': 'eventlistdaysahead',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Max displayed events(optional)'),
							placeholder: props.attributes.eventlist_display,
							 
							value: props.attributes.eventlist_display,
							onChange: function(value){
								props.setAttributes({
									eventlist_display: value
								});		
							},
							'data-cal_id': 'eventlist_display',
						}
					),
					el(
						ToggleControl,
						{
							label:	__('Load Dynamic Event Details Box'),
							checked: !!+props.attributes.eventlistextendeddetails,
														onChange:function(e) {
								return props.setAttributes({
									eventlistextendeddetails: e ? '1' : '0'	
								})
							}
						}
					),		
				),
				el( 
					PanelBody, {
						title: __( 'Tooltip' ),
						className: 'rhc-inspector-vc_tab_tooltip',
						initialOpen: false,
					},
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Tooltip behaviour')
					),
		
					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Tooltip links target'),
							value:  props.attributes.tooltip_target ,
							options: 	[
								
									{
										label: __('_self'),
										value: '_self'
									},
								
									{
										label: __('_blank'),
										value: '_blank'
									},
								
									{
										label: __('_top'),
										value: '_top'
									},
								
									{
										label: __('_parent'),
										value: '_parent'
									},
								],
							 
							onChange: function(value){
								props.setAttributes({
									tooltip_target: value
								});	
							}
						}
					),			
					el(
						ToggleControl,
						{
							label:	__('Disable title link'),
							checked: !!+props.attributes.tooltip_disable_title_link,
														onChange:function(e) {
								return props.setAttributes({
									tooltip_disable_title_link: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Turn this feature off if you dont want to use the custom layout that can be set on the event edit page.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Enable custom details layout') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.tooltip_enable_custom,
														onChange:function(e) {
								return props.setAttributes({
									tooltip_enable_custom: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Show on hover'),
							checked: !!+props.attributes.tooltip_on_hover,
														onChange:function(e) {
								return props.setAttributes({
									tooltip_on_hover: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Close on event title leave'),
							checked: !!+props.attributes.tooltip_close_on_title_leave,
														onChange:function(e) {
								return props.setAttributes({
									tooltip_close_on_title_leave: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Close on click outside tooltip'),
							checked: !!+props.attributes.tooltip_close_on_outside_click,
														onChange:function(e) {
								return props.setAttributes({
									tooltip_close_on_outside_click: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Render image in tooltip'),
							checked: !!+props.attributes.tooltip_image,
														onChange:function(e) {
								return props.setAttributes({
									tooltip_image: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	__('Show excerpt'),
							checked: !!+props.attributes.tooltip_excerpt,
														onChange:function(e) {
								return props.setAttributes({
									tooltip_excerpt: e ? '1' : '0'	
								})
							}
						}
					),		
					el(
						SelectControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Please notice enabling this feature can cause your events to load a little bit slower in Month View, but the custom tooltip content will load immediately when you click the event title.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Load custom tooltip content with events') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							value:  props.attributes.ajax_render_tooltip_details ,
							options: 	[
								
									{
										label: __('Never'),
										value: ''
									},
								
									{
										label: __('Always'),
										value: 'always'
									},
								
									{
										label: __('As external source only'),
										value: 'sources'
									},
								],
							 
							onChange: function(value){
								props.setAttributes({
									ajax_render_tooltip_details: value
								});	
							}
						}
					),			
		
				),
				el( 
					PanelBody, {
						title: __( 'Other' ),
						className: 'rhc-inspector-vc_tab_other',
						initialOpen: false,
					},
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('icalendar button')
					),
					el(
						ToggleControl,
						{
							label:	__('Enable icalendar button'),
							checked: !!+props.attributes.icalendar,
														onChange:function(e) {
								return props.setAttributes({
									icalendar: e ? '1' : '0'	
								})
							}
						}
					),					el(
						RangeControl,
						{
							label:	__('Dialog width'),
							 
							onChange: function(value){
								props.setAttributes({
									icalendar_width: value.toString()
								});	
							},
							min: 0,
							max: 1024,
							step: 1,
							value: Number( props.attributes.icalendar_width ),
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Button label'),
							placeholder: props.attributes.icalendar_button,
							 
							value: props.attributes.icalendar_button,
							onChange: function(value){
								props.setAttributes({
									icalendar_button: value
								});		
							},
							'data-cal_id': 'icalendar_button',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Dialog title'),
							placeholder: props.attributes.icalendar_title,
							 
							value: props.attributes.icalendar_title,
							onChange: function(value){
								props.setAttributes({
									icalendar_title: value
								});		
							},
							'data-cal_id': 'icalendar_title',
						}
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Dialog description'),
							placeholder: props.attributes.icalendar_description,
							 
							value: props.attributes.icalendar_description,
							onChange: function(value){
								props.setAttributes({
									icalendar_description: value
								});		
							},
							'data-cal_id': 'icalendar_description',
						}
					),
		
					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Alignment'),
							value:  props.attributes.icalendar_align ,
							options: 	[
								
									{
										label: __('Left'),
										value: 'left'
									},
								
									{
										label: __('Center'),
										value: 'center'
									},
								
									{
										label: __('Right'),
										value: 'right'
									},
								],
							 
							onChange: function(value){
								props.setAttributes({
									icalendar_align: value
								});	
							}
						}
					),			
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Calendar button (filter)')
					),
					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Turn this feature on to limit the terms listed under a taxonomy in the taxonomy filter. For example, lets say that the calendar is configured to only show events from the "Sports" category, the filter will only display terms that contains events and that are children of the "Sports" term.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Hierarchical filter') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.hierarchical_filter,
														onChange:function(e) {
								return props.setAttributes({
									hierarchical_filter: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Turn this feature on to only show the Parent terms in the drop-down added when using the Taxonomy Filter add-on.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Show Parent terms only (Taxonomy Filter)') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.btn_tax_parent_only,
														onChange:function(e) {
								return props.setAttributes({
									btn_tax_parent_only: e ? '1' : '0'	
								})
							}
						}
					),					el(
						ToggleControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Turn this feature on to show the post count for a term in the taxonomy dropdown filter.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Show term post count (Taxonomy Filter)') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							checked: !!+props.attributes.term_post_count,
														onChange:function(e) {
								return props.setAttributes({
									term_post_count: e ? '1' : '0'	
								})
							}
						}
					),					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Calendar Widget')
					),
					el(
						'h2',
						{
							 
							className:'rhc-inspector-subtitle'
						},
						__('Event click behavior')
					),
		
					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Event click behavior'),
							value:  props.attributes.event_click ,
							options: 	[
								
									{
										label: __('Default'),
										value: 'fc_click'
									},
								
									{
										label: __('No action'),
										value: 'fc_click_no_action'
									},
								],
							 
							onChange: function(value){
								props.setAttributes({
									event_click: value
								});	
							}
						}
					),			
		
				),
				el( 
					PanelBody, {
						title: __( 'Conditions' ),
						className: 'rhc-inspector-vc_tab_condition',
						initialOpen: false,
					},
					el(
						TextControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('If used, the shortcode will only display if the user is logged in and have the specific capability.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Permission (capability)') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							placeholder: props.attributes.capability,
							 
							value: props.attributes.capability,
							onChange: function(value){
								props.setAttributes({
									capability: value
								});		
							},
							'data-cal_id': 'capability',
						}
					),
					el(
						CheckboxControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Check the conditions to test for displaying the shortcode.  Leave empty to display everywhere, included feeds and trackbacks.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('is_home') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_home')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_home');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_home');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_front_page'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_front_page')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_front_page');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_front_page');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_singular'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_singular')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_singular');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_singular');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_page'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_page')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_page');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_page');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_single'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_single')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_single');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_single');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_sticky'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_sticky')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_sticky');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_sticky');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_category'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_category')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_category');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_category');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_tax'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_tax')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_tax');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_tax');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_author'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_author')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_author');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_author');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_archive'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_archive')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_archive');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_archive');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_search'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_search')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_search');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_search');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_attachment'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_attachment')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_attachment');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_attachment');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_tag'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_tag')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_tag');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_tag');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_date'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_date')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_date');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_date');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_paged'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_paged')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_paged');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_paged');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_main_query'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_main_query')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_main_query');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_main_query');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_feed'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_feed')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_feed');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_feed');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_trackback'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_trackback')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_trackback');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_trackback');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('in_the_loop'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('in_the_loop')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('in_the_loop');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('in_the_loop');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_user_logged_in'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_user_logged_in')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_user_logged_in');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_user_logged_in');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),		
				),


//------- END
			),


            /*
            el(ServerSideRender, {
            	key: "editable",
                block: "calendarizeit-gutenberg/calendarizeit",
                attributes:  props.attributes,
                onChange: function(e){
                	console.log( 'serverside onupdate', typeof e, e );
                }
            }),
            */




        ];
    },

    save: function( e ) {


		return null;
    },
} );

})(jQuery);
