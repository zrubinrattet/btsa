;;

( function() {

	var el = wp.element.createElement,
		registerBlockType = wp.blocks.registerBlockType,
		blockStyle = { backgroundColor: '#900', color: '#fff', padding: '20px' }
		BlockControls = wp.editor.BlockControls,
		AlignmentToolbar = wp.editor.AlignmentToolbar,
		InspectorControls = wp.editor.InspectorControls,
		ToggleControl = wp.components.ToggleControl,
		TextControl = wp.components.TextControl,
		RangeControl = wp.components.RangeControl,
		SelectControl = wp.components.SelectControl,
		CheckboxControl = wp.components.CheckboxControl,
		PanelBody	= wp.components.PanelBody,
		__ = wp.i18n.__,
		ServerSideRender = wp.components.ServerSideRender,
		parameters = {},
		Dashicon = wp.components.Dashicon,
		Tooltip = wp.components.Tooltip,
		DatePicker = wp.components.DatePicker
		;

	const iconEl = el('svg', { width: 24, height: 24 },
	  el('path', { d: "M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V8h16v13z" } ),
	  el('path', { d: "M0 0h24v24H0z", fill: "none" } )
	);

	var rhc_gutenberg_timeout=null;
	var rhc_gutenberg_timeout_initial = true;
	registerBlockType( 'calendarize-it/rhc-static-upcoming-events', {
		title: __('Accordion Events'),

		icon: iconEl,

		category: 'calendarize-it',

		description: __('Calendarize it! - Accordion Upcoming Events'),

		attributes: RHC_GUTENBERG_SUPE.attributes,

		edit: function( props ) {	

			if( true ) {
				//Gutenberg is refreshing preview on every keystroke,
				//this block prevents that by adding a small delay before updating the view.
				if( ! props.buffer ) props.buffer = {} ;

				if( rhc_gutenberg_timeout )
					clearTimeout( rhc_gutenberg_timeout );

				var atts = props.attributes;

				url = RHC_GUTENBERG_SUPE.ajax_url + '?';

				o = {
					'context':'edit',
					'attributes':{}
				};
				$.each(props.attributes, function(i,attr){
					o.attributes[i] = attr;
				});

				url = url + '&post_id=' + wp.data.select ( 'core/editor' ).getCurrentPostId();

				url = url + $.param(o) ;

				var el_id = '_' + Math.random().toString(36).substr(2, 9);

				args = {
					'action': 'rhc_gutenberg_supe',//defined in class.rhc_static_upcoming_events
					'post_id': wp.data.select ( 'core/editor' ).getCurrentPostId(),
					'data' : o
				}

				var _delay = 500;

				if( rhc_gutenberg_timeout_initial ){
					rhc_gutenberg_timeout_initial = false;
					_delay = 0;
				}
	
				rhc_gutenberg_timeout = setTimeout(function(){
					$.post( ajaxurl, args, function(data){
						sel = '#' + el_id;
						$(sel).html(data);
						
						if( 'function' == typeof init_uew_widgets ){
							init_uew_widgets();
						}
						
					},'html');
				}, _delay );			
			}

			if( !props.attributes.conditional_tag ){
				props.attributes.conditional_tag = [];
			}

			function __refresh_header(){
				let header = [];
				$('[data-btn_tax].is-primary:visible').each(function(i,li){		
					header.push( $(li).attr('data-btn_tax') );
				});
				props.setAttributes({
					header: header.join(',')
				});				
			}	

			var __init_btn_tax_sortable_timeout = null;
			function __init_btn_tax_sortable(){				
				//TODO, find a callback to initialize sortables, instead of recurring to a timed event.
				if( $( ".btn-tax-sortable" ).length == 0 ){
					if( __init_btn_tax_sortable_timeout ){
						clearTimeout(__init_btn_tax_sortable_timeout);
					}				
					__init_btn_tax_sortable_timeout = setTimeout( __init_btn_tax_sortable, 1000 );
				}else{
					$( ".btn-tax-sortable" ).sortable({
						stop: __refresh_header,
					});
					$( ".btn-tax-sortable" ).disableSelection();				
				}
			}

			__init_btn_tax_sortable();

			return [

				el( 'div', {
					'id': el_id,
					key: "editable",
					class: 'rhc-supe-gutenberg-preview',
				}, 'loading' ), //loading

				el(
					InspectorControls,
					{ key: 'inspector' },
	//------- START
				el(
					PanelBody, {
						title: __( 'Style' ),
						className: 'rhc-inspector-supe-tab_style',
						initialOpen: false,
					},

					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Style'),
							value:  props.attributes.template ,
							options: RHC_GUTENBERG_SUPE.template_options,
							onChange: function(value){
								props.setAttributes({
									template: value
								});
								
								let disable_controls = RHC_GUTENBERG_SUPE.template_options.reduce( (a,b) => b.value == value ? b.disabled_controls : a ,'');
								
								$('.rhc-gutenberg-inspector-disabled')
									.show()
									.removeClass('rhc-gutenberg-inspector-disabled')
								;
								
								$(disable_controls)
									.addClass('rhc-gutenberg-inspector-disabled')
									.hide()
								;								
							}
						}
					), //-- template

					/* this work only for accordeon addon. */
					el(
						'div',
						{
							id: 'rhc-gutenberg-inspector-control-nav',
						},
						el(
							SelectControl,
							{
								type: 'text',
								label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('<p>1. will show the navigation arrows above the accordion.</p><p>2. will show the navigation arrows below the accordion.</p><p>3. will show the navigation arrows both above and below.</p><p>4. will hide the navigation arrows, but still, show the Taxonomy Filter (drop down) if added.</p>')} }) },
									el( 'span', {},
										el( 'span', {}, __('Navigation') ),
										el( Dashicon, {icon:'editor-help'} )
									)
								),							
								value:  props.attributes.nav ,
								options: 	[
										{
											label: __('default'),
											value: ''
										},
										{
											label: __('1. Arrows above'),
											value: 1
										},
										{
											label: __('2. Arrows below'),
											value: 2
										},
										{
											label: __('3. Arrows above and below'),
											value: 3
										},
										{
											label: __('4. No arrows'),
											value: 4
										},
									],

								onChange: function(value){
									props.setAttributes({
										nav: value
									});
								}
							}
						), //-- Nav						
					),
										
					el(
						TextControl,
						{
							type: 'text',
							label:	
								el( Tooltip, {
									text: el('div', {class:'rhc-gutenberg-tooltip',},
										__('Specify a message to display when there are no events.')
									) 
								},	
								el( 'span', {},
									el( 'span', {}, __('No events message') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),	
							placeholder: props.attributes.no_events_message,
							value: props.attributes.no_events_message,
							onChange: function(value){
								props.setAttributes({
									no_events_message: value
								});
							}
						}
					),	//-- no events message

				), //-- close Style panel.

				el(
					PanelBody, {
						title: __( 'Display' ),
						className: 'rhc-inspector-supe-tab_display',
						initialOpen: false,
					},
					
					el(
						RangeControl,
						{
							label:	
								el( Tooltip, {
									text: el('div', {class:'rhc-gutenberg-tooltip',},
										__('Number of events to display.')
									) 
								},	
								el( 'span', {},
									el( 'span', {}, __('Number') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),	

							onChange: function(value){
								props.setAttributes({
									number: value.toString()
								});
							},
							min: 0,
							max: 99,
							step: 1,
							value: Number(props.attributes.number),
						}
					), //Number of events

					el(
						ToggleControl,
						{
							label:	__('Show image'),
							checked: !!+props.attributes.showimage,
							onChange:function(e) {
								return props.setAttributes({
									showimage: e ? '1' : '0'							
								})
							}
						}
					),//-- showimage
					
					el(
						ToggleControl,
						{
							label:	__('Auto'),
							help: __('Turn on and the widget will automatically display events related to the loaded page, if you are for example loading the calendar archive, it will then show events from that particular calendar.'),
							checked: !!+props.attributes.auto,
							onChange:function(e) {
								return props.setAttributes({
									auto: e ? '1' : '0'							
								})
							}
						}
					),//-- showimage	
					
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Words'),	
							placeholder: props.attributes.words,
							help: __('Max number of words to display in the description.'),
							value: props.attributes.words,
							onChange: function(value){
								props.setAttributes({
									words: value
								});
							}
						}
					),	//-- words									

					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Recurring events'),							
							value:  props.attributes.premiere ,
							options: 	[
									{
										label: __('1. Show all recurring events'),
										value: ''
									},
									{
										label: __('2. Only premiere (first event)'),
										value: '1'
									},
									{
										label: __('3. First event in date range'),
										value: '2'
									},									
								],

							onChange: function(value){
								props.setAttributes({
									premiere: value
								});
							}
						}
					),//-- Premiere

					el(
						SelectControl,
						{
							type: 'text',
							label:	
								el( Tooltip, {
									text: el('div', {class:'rhc-gutenberg-tooltip',},
										__('Use this option to display events that are all day ( no time ) or exclude all day events.')
									) 
								},	
								el( 'span', {},
									el( 'span', {}, __('Show all day events') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							
							value:  props.attributes.allday ,
							options: 	[
									{
										label: __('Both all day and non all day'),
										value: ''
									},
									{
										label: __('Only show all day events'),
										value: '1'
									},
									{
										label: __('Exclude all day events'),
										value: '0'
									},									
								],

							onChange: function(value){
								props.setAttributes({
									allday: value
								});
							}
						}
					),//-- Allday

					el(
						SelectControl,
						{
							multiple: true,
							label:	
								el( Tooltip, {
									text: el('div', {class:'rhc-gutenberg-tooltip',},
										__('If you are using other post types as events, choose which ones to display')
									) 
								},	
								el( 'span', {},
									el( 'span', {}, __('Post type') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							
							value:  props.attributes.post_type ? props.attributes.post_type.split(',') : [] ,
							options: 	RHC_GUTENBERG_SUPE.post_type_options,
							onChange: function(value){					
								props.setAttributes({
									post_type: value.join(',')
								});
								
								props.setAttributes({
									header: ''//since post type changed, remove btn_tax_* filters.
								});		
								
								$('.supe-btn-tax').removeClass('is-primary');							
							}
						}
					),//-- post_type
					
					el(
						SelectControl,
						{
							type: 'text',
							label:	
								el( Tooltip, {
									text: el('div', {class:'rhc-gutenberg-tooltip',},
										__('Relevant only with the external event sources add-on')
									) 
								},	
								el( 'span', {},
									el( 'span', {}, __('Display External Sources') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							
							value:  props.attributes.feed ,
							options: 	[
									{
										label: __('Do not show external sources'),
										value: '0'
									},
									{
										label: __('Only show external sources'),
										value: '1'
									},
									{
										label: __('Show both local and external sources'),
										value: '2'
									},									
								],

							onChange: function(value){
								props.setAttributes({
									feed: value
								});
							}
						}
					),//-- Feed	
					
					el(
						ToggleControl,
						{
							label:	__('Client side, local timezone'),
							checked: !!+props.attributes.local_tz,
							onChange:function(e) {
								return props.setAttributes({
									local_tz: e ? '1' : '0'							
								})
							}
						}
					),//--local_tz	
										
					el(
						ToggleControl,
						{
							label:	__('Hide widget if no events'),
							checked: !!+props.attributes.hideempty,
							onChange:function(e) {
								return props.setAttributes({
									hideempty: e ? '1' : '0'								
								})
							}
						}
					),//-- hideempty	
					
		
				), //-- Display panel

				el(
					PanelBody, {
						title: __( 'Date format' ),
						className: 'rhc-inspector-supe-tab_format',
						initialOpen: false,
					},
					el(
						TextControl,
						{
							id: 'rhc-supe-date-format',
							type: 'text',
							label: el( 'span', {},
									el( 'span', {}, __('Date format') ),
									el( 'span', {
										onClick: function(e){
											return rc_dateformat_helper.open( '#rhc-supe-date-format', 'date' );	
										}									
									},  
										el( Dashicon, {
											icon:'admin-settings',
										} ),									
									),
							),
							placeholder: props.attributes.date_format,
							value: props.attributes.date_format,
							onChange: function(value){
								props.setAttributes({
									date_format: value
								});
							}
						}
					),	//-- date_format
					el(
						TextControl,
						{
							id: 'rhc-supe-time-format',
							type: 'text',
							label: el( 'span', {},
									el( 'span', {}, __('Time format') ),
									el( 'span', {
										onClick: function(e){
											return rc_dateformat_helper.open( '#rhc-supe-time-format', 'time' );	
										}									
									},  
										el( Dashicon, {
											icon:'admin-settings',
										} ),									
									),
							),
							placeholder: props.attributes.time_format,
							value: props.attributes.time_format,
							onChange: function(value){
								props.setAttributes({
									time_format: value
								});
							}
						}
					),	//-- time_format						
				), //-- Date format panel

				el(
					PanelBody, {
						title: __( 'Taxonomy/Term Filtering' ),
						className: 'rhc-inspector-supe-tab_filter_tax',
						initialOpen: false,
					},
					
					el(
						TextControl,
						{
							type: 'text',
							label:	
								el( Tooltip, {
									text: el('div', {class:'rhc-gutenberg-tooltip',},
										__('Specify a taxonomy slug.  Example: calendar, venue, organizer; or any available custom taxonomy enabled for events.')
									) 
								},	
								el( 'span', {},
									el( 'span', {}, __('Taxonomy') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),	
							placeholder: props.attributes.taxonomy,
							value: props.attributes.taxonomy,
							onChange: function(value){
								props.setAttributes({
									taxonomy: value
								});
							}
						}
					),	

					el(
						TextControl,
						{
							type: 'text',
							label:	
								el( Tooltip, {
									text: el('div', {class:'rhc-gutenberg-tooltip',},
										__('Specify comma separated term slugs corresponding to the selected taxonomy.  Example: holiday,concert')
									) 
								},	
								el( 'span', {},
									el( 'span', {}, __('Terms') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),	
							placeholder: props.attributes.terms,
							value: props.attributes.terms,
							onChange: function(value){
								props.setAttributes({
									terms: value
								});
							}
						}
					),						
				), //-- Taxonomy Filter panel.

				el(
					PanelBody, {
						title: __( 'Dropdown Filter Controls' ),
						className: 'rhc-inspector-supe-tab_dropdown_filter',
						initialOpen: false,
					},
					
					el(
						'div',
						{},
						__('Click the dropdown filters that you would like displayed.  Drag and drop to sort them.  Observe that the dropdown filter only displays when a navigation is selected.')
					),

					el(
						TextControl,
						{
							type: 'text',
							label: __('header'),	
							value: props.attributes.header,
							onChange: function(value){
								props.setAttributes({
									header: value
								});
							}
						}
					),	//-- no events message

					el(
						'ul',
						{
							class: 'btn-tax-sortable',
						},
						function(){
								
							let post_types = props.attributes.post_type;
							if( post_types =='' ){
								post_types = RHC_GUTENBERG_SUPE.RHC_EVENTS
							}
							let post_types_arr = post_types.split(',')	;
						
							let selected = props.attributes.header.split(',');
						
							let ret = [];
							let o = RHC_GUTENBERG_SUPE.btn_tax_options;
							for( var post_type in o ){
								if( post_type == '__labels' ) continue;
								if( -1 == post_types_arr.indexOf( post_type ) ) continue;
								for( var btn_tax in o[post_type] ){
									let tax_label = o[post_type][btn_tax];
									
									if( -1 == selected.indexOf(btn_tax) ){
										var _class = '';
									}else{
										var _class = ' is-primary';
									}
									
									ret.push( el(
										'li',
										{
											'class': 'supe-btn-tax components-button is-button is-default is-large widefat' + _class,
											'data-post_type': post_type,
											'data-btn_tax': btn_tax,
											onClick: function(e){
												$(e.target).toggleClass('is-primary');
												__refresh_header();
											}
										},
										tax_label + ' (' + o.__labels[post_type] + ')'
									));
			
								}

							}
						
							return ret;
						}(),
					),
					
				), //-- Dropdown Filter panel

				el(
					PanelBody, {
						title: __( 'Advanced Date Filtering' ),
						className: 'rhc-inspector-supe-tab_filter_date',
						initialOpen: false,
					},

					el(
						SelectControl,
						{
							type: 'text',
							label:	
								el( Tooltip, {
									text: el('div', {class:'rhc-gutenberg-tooltip',},
										__('Order to sort events, choose reversed in combination with a date in the past to display past events rather than upcoming events.')
									) 
								},	
								el( 'span', {},
									el( 'span', {}, __('Order') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							
							value:  props.attributes.order ,
							options: 	[
									{
										label: __('Normal'),
										value: 'ASC'
									},
									{
										label: __('Reversed'),
										value: 'DESC'
									},
								],

							onChange: function(value){
								props.setAttributes({
									order: value
								});
							}
						}
					),//-- Order

					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Remove events by'),							
							value:  props.attributes.horizon,
							options: 	[
									{
										label: __('Starting hour'),
										value: 'hour'
									},
									{
										label: __('Day'),
										value: 'day'
									},
									{
										label: __('End date'),
										value: 'end'
									},
								],

							onChange: function(value){
								props.setAttributes({
									horizon: value
								});
							}
						}
					),//-- Horizon

					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Reference Date'),							
							value:  function(){
							
								return props.attributes.date == 'NOW' ? 'NOW' : '';							
							}() ,
							options: 	[
									{
										label: __('Current date'),
										value: 'NOW'
									},
									{
										label: __('Custom'),
										value: ''
									},
								],

							onChange: function(value){

								props.setAttributes({						
									date: value
								});
							}
						}
					),//-- Date NOW or CUSTOM
					
					el(
						'div',
						{
							style: function(){
								return props.attributes.date == 'NOW' ? {display:'none'} : {} ;
							}(),
						},
						el(
							TextControl,
							{
								type: 'text',
								label:	el( 'span', {},
									el( 'span', {}, __('Custom Reference Date') ),
									el( 'span', {
										onClick: function(e){
											if( $('#supe-date-datepicker').is(':visible') ){
												$('#supe-date-datepicker').slideUp();
											}else{
												$('#supe-date-datepicker').hide().css('height','100%').slideDown();
											}
										}									
									},  
										el( Dashicon, {
											icon:'calendar',
										} ),									
									),
									
									//--DatePicker helper
									el(
										'div',
										{
											id:'supe-date-datepicker',
											style: {
												height: '0',
												overflow: 'hidden'
											}
										},
										el(
											DatePicker,
											{
												currentDate : moment(),
												//locale : 'en',
												onChange: function(value){					
													props.setAttributes({
														date: moment( value ).format('YYYY-MM-DD')
													});
													$('#supe-date-datepicker').slideUp();
												},
											}
										),	
									),//-- datepicker for date
								),
								help: el('div',{},
									el('p',{},__('Choose a specific date from the calendar by clicking on the calendar icon, or use specialized filters.')	),
									el('p',{},__('Example:')	),									
									el('p',{},	
										el('a',{href:'#',onClick:function(e){props.setAttributes({date:'-7 days'});}},__('-7 days')),
										__(' display events from 7 days in the past'),
									),																
									el('p',{},	
										el('a',{href:'#',onClick:function(e){props.setAttributes({date:'last Friday'});}},__('last Friday')),
										__(' to display events that started last Friday'),
									),																	
									el('p',{},
										el('a',{href:'#',onClick:function(e){props.setAttributes({date:'next Wednesday'});}},__('next Wednesday')),
										__(' to display events that will start on next Wednesday.'),
									),										
								),
								value: props.attributes.date,
								onChange: function(value){
									props.setAttributes({
										date: value
									});
								},
							},
						
						),
						
						//-- date						
					),//-- Custom Date start
				
					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Events starting'),							
							value:  props.attributes.date_compare ,
							options: 	[
									{
										label: __('default'),
										value: ''
									},
									{
										label: __('> After reference date ( date exclusive )'),
										value: '>'
									},
									{
										label: __('>= After reference date ( date inclusive )'),
										value: '>='
									},
									{
										label: __('< Before reference date ( date exclusive )'),
										value: '<'
									},
									{
										label: __('<= Before reference date ( date inclusive )'),
										value: '<='
									},
								],

							onChange: function(value){
								props.setAttributes({
									date_compare: value
								});
							}
						}
					),//-- Date compare	
					
					el(
						'div',
						{class:'supe-date-end-desc'},
						'The following settings are used to filter events by their ending date compared to the reference date.'
					),	//description
					
					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Reference End Date'),							
							value:  function(){
								return props.attributes.date_end == 'NOW' ? 'NOW' : '';							
							}() ,
							options: 	[
									{
										label: __('Current date'),
										value: 'NOW'
									},
									{
										label: __('Custom or none'),
										value: ''
									},
								],

							onChange: function(value){
								props.setAttributes({
									date_end: value
								});
							}
						}
					),//-- Date End NOW or CUSTOM		
					
					el(
						'div',
						{
							style: function(){
								return props.attributes.date_end == 'NOW' ? {display:'none'} : {} ;
							}(),
						},
						el(
							TextControl,
							{
								type: 'text',
								label:	el( 'span', {},
									el( 'span', {}, __('Custom Reference Date End') ),
									el( 'span', {
										onClick: function(e){
											if( $('#supe-date-end-datepicker').is(':visible') ){
												$('#supe-date-end-datepicker').slideUp();
											}else{
												$('#supe-date-end-datepicker').hide().css('height','100%').slideDown();
											}
										}									
									},  
										el( Dashicon, {
											icon:'calendar',
										} ),									
									),
									
									//--DatePicker helper
									el(
										'div',
										{
											id:'supe-date-end-datepicker',
											style: {
												height: '0',
												overflow: 'hidden'
											}
										},
										el(
											DatePicker,
											{
												currentDate : moment(),
												//locale : 'en',
												onChange: function(value){					
													props.setAttributes({
														date_end: moment( value ).format('YYYY-MM-DD')
													});
													$('#supe-date-end-datepicker').slideUp();
												},
											}
										),	
									),//-- datepicker for date
								),
								help: el('div',{},
									el('p',{},__('Choose a specific date from the calendar by clicking on the calendar icon, or use specialized filters.')	),
									el('p',{},__('Example:')	),									
									el('p',{},	
										el('a',{href:'#',onClick:function(e){props.setAttributes({date_end:'-7 days'});}},__('-7 days')),
										__(' display events from 7 days in the past'),
									),																
									el('p',{},	
										el('a',{href:'#',onClick:function(e){props.setAttributes({date_end:'last Friday'});}},__('last Friday')),
										__(' to display events that ended last Friday'),
									),																	
									el('p',{},
										el('a',{href:'#',onClick:function(e){props.setAttributes({date_end:'next Wednesday'});}},__('next Wednesday')),
										__(' to display events that will end on next Wednesday.'),
									),										
								),
								value: props.attributes.date_end,
								onChange: function(value){
									props.setAttributes({
										date_end: value
									});
								},
							},
						
						),
						
						//-- date						
					),//-- Custom Date end
					
					el(
						SelectControl,
						{
							type: 'text',
							label:	__('Events ending'),							
							value:  props.attributes.date_end_compare ,
							options: 	[
									{
										label: __('default'),
										value: ''
									},
									{
										label: __('> After reference date ( date exclusive )'),
										value: '>'
									},
									{
										label: __('>= After reference date ( date inclusive )'),
										value: '>='
									},
									{
										label: __('< Before reference date ( date exclusive )'),
										value: '<'
									},
									{
										label: __('<= Before reference date ( date inclusive )'),
										value: '<='
									},
								],

							onChange: function(value){
								props.setAttributes({
									date_end_compare: value
								});
							}
						}
					),//-- Date end compare																			
														
				), //-- date Filtering		

				el(
					PanelBody, {
						title: __( 'Geo Filtering' ),
						className: 'rhc-inspector-supe-tab_filter_geo',
						initialOpen: false,
					},
					el(
						'p',{},__('Filter events by distance to a fixed geographical coordinate')
					),
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Geo Center'),	
							help: __('This is the Latitude and Longitude that can be used to set a fixed location. If you do not set this the Geo IP plugin will detect the visitor’s location.'),
							value: props.attributes.geo_center,
							onChange: function(value){
								props.setAttributes({
									geo_center: value
								});
							}
						}
					),					
					el(
						TextControl,
						{
							type: 'text',
							label:	__('Geo Radius'),	
							help: __(' Choose either Miles or Kilometer in Options > General Settings and then enter the radius you want to set for showing events.'),
							value: props.attributes.geo_radius,
							onChange: function(value){
								props.setAttributes({
									geo_radius: value
								});
							}
						}
					),					
				),//-- Geo Filter panel
				
				el(
					PanelBody, {
						title: __( 'Author Filtering' ),
						className: 'rhc-inspector-supe-tab_filter_author',
						initialOpen: false,
					},
					
					el(
						SelectControl,
						{
							type: 'text',
							label:	
								el( Tooltip, {
									text: el('div', {class:'rhc-gutenberg-tooltip',},
										__('Display events filtered by author.')
									) 
								},	
								el( 'span', {},
									el( 'span', {}, __('Events posted by') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							
							value:  props.attributes.author ,
							options: 	[
									{
										label: __('Any author / Specific author'),
										value: ''
									},									
									{
										label: __('Logged user'),
										value: 'current'
									},
									{
										label: __('Post/page author'),
										value: 'the_post_author'
									},
								],

							onChange: function(value){
								props.setAttributes({
									author: value
								});
							}
						}
					),//-- Author	
					
					el(
						'div',
						{style: function(){return props.attributes.author && props.attributes.author=='current' ? {} : {display:'none'} }()},
						__('Logged user selected.  Widget will display events authored by the user that is logged in to Wordpress')
					),	//description for author option
					
					el(
						'div',
						{style: function(){return props.attributes.author && props.attributes.author=='the_post_author' ? {} : {display:'none'} }()},
						__('Post/page author selected.  Widget will display events authored by the author of the post/page where this block is inserted.')
					),	//description for author option
					
					el(
						'div',
						{style: function(){return !props.attributes.author || props.attributes.author=='' ? {} : {display:'none'} }()},
						__('Any author / Specific author option selected.  Widget will display events from any author, or from a specific author if the next field is filled with an author ID.')
					),	//description for author option

					//wrap required because Textcontrol label was not hidden.
					el(
						'div',
						{
							style: function(){
								switch( props.attributes.author ){
									case 'current':
									case 'the_post_author':
										return {display:'none'};
								}
								return {};
							}()						
						},
						el(
							TextControl,
							{
								type: 'text',
								label:	__('Specific author ID'),	
								value: props.attributes.author,
								onChange: function(value){
									props.setAttributes({
										author: value
									});
								}
							}
						),
					),	// arg author								
					
				),//-- Author filtering
				
				el( 
					PanelBody, {
						title: __( 'Conditional display' ),
						className: 'rhc-supe-inspector-tab_condition',
						initialOpen: false,
					},
					el(
						TextControl,
						{
							type: 'text',
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('If used, the shortcode will only display if the user is logged in and have the specific capability.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('Permission (capability)') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							placeholder: props.attributes.capability,
							 
							value: props.attributes.capability,
							onChange: function(value){
								props.setAttributes({
									capability: value
								});		
							}
						}
					),
					el(
						CheckboxControl,
						{
							label:	el( Tooltip, {text: el('div', {class:'rhc-gutenberg-tooltip',dangerouslySetInnerHTML:{__html:__('Check the conditions to test for displaying the shortcode.  Leave empty to display everywhere, included feeds and trackbacks.')} }) },  
								el( 'span', {}, 
									el( 'span', {}, __('is_home') ),
									el( Dashicon, {icon:'editor-help'} )
								)
							),							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_home')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_home');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_home');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_front_page'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_front_page')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_front_page');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_front_page');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_singular'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_singular')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_singular');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_singular');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_page'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_page')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_page');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_page');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_single'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_single')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_single');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_single');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_sticky'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_sticky')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_sticky');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_sticky');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_category'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_category')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_category');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_category');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_tax'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_tax')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_tax');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_tax');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_author'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_author')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_author');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_author');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_archive'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_archive')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_archive');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_archive');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_search'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_search')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_search');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_search');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_attachment'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_attachment')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_attachment');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_attachment');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_tag'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_tag')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_tag');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_tag');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_date'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_date')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_date');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_date');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_paged'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_paged')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_paged');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_paged');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_main_query'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_main_query')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_main_query');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_main_query');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_feed'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_feed')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_feed');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_feed');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_trackback'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_trackback')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_trackback');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_trackback');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('in_the_loop'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('in_the_loop')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('in_the_loop');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('in_the_loop');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),					el(
						CheckboxControl,
						{
							label:	__('is_user_logged_in'),
							name: 'conditional_tag[]',
							checked: !!+(props.attributes.conditional_tag.indexOf('is_user_logged_in')>-1),
							onChange:function(e) {
								d = props.attributes.conditional_tag.slice();
					
								var index = d.indexOf('is_user_logged_in');
								
								if (index > -1) {
									d.splice(index, 1);									
								}						
								
								if( e ){								
									d.push('is_user_logged_in');
								}										

								props.setAttributes({
									conditional_tag: d
								});	
							}							
						}
					),		
				),								
	//------- END
				),//Close InspectorControls
				
				/* Replaced with the delayed preview code.
				el(ServerSideRender, {
					key: "editable",
					block: "calendarize-it/rhc-static-upcoming-events",
					attributes:  props.attributes,
					onChange: function(e){
						console.log( 'serverside onupdate', typeof e, e );
					}
				}),		
				*/		

			];
		},

		save: function( e ) {
			return null;
		},
	} );

})();
