<?php

class rhc_gutenberg {
	function __construct(){

		//return;

		add_filter( 'block_categories', array( &$this, 'rhc_sample_block_categories' ), 10, 2 );

		add_filter( 'rhc_load_frontend_only_scripts_in_admin', array( &$this, 'rhc_load_frontend_only_scripts_in_admin' ) );

		add_action( 'init', array( &$this, 'init_rhc_gutenberg' ) );

		//Other Blocks

	}



	function pop_to_gutenberg_options( $options ){
		$arr = array();
		foreach( $options as $value => $label ){
			$arr[] = (object)array(
				'label' => $label,
				'value'	=> $value
			);
		}
		return $arr;
	}

	function init_rhc_gutenberg() {
		global $rhc_plugin;
		
		wp_register_style(
			'calendarizeit-gutenberg-css',
			RHC_URL . 'gutenberg/css/rhc_gutenberg.css',
			array( 'calendarizeit', 'rhc-last-minue' ),
			'1.0.7'
		);

		wp_register_script(
			'calendarizeit-gutenberg',
			RHC_URL . 'gutenberg/js/rhc_gutenberg.js',
			array( 'wp-blocks', 'wp-element', 'wp-editor', 'wp-i18n', 'calendarize', 'bootstrap-select' ),
			'1.0.7.8'
		);

//$this->get_rhc_taxonomies();
		$selected_post_types = $rhc_plugin->get_option('post_types',array());
		$selected_post_types = is_array($selected_post_types)?$selected_post_types:array();
		if( !in_array( RHC_EVENTS, $selected_post_types ) ){
			$selected_post_types[] = RHC_EVENTS;//always have events.
		}

		$post_types=array();
		foreach(get_post_types(array(/*'public'=> true,'_builtin' => false*/),'objects','and') as $post_type => $pt){
			if(!in_array($post_type,$selected_post_types))continue;
			$post_types[]=(object)array(
				'label'	=> $pt->label,
				'post_type' => $post_type
			);
		} 

		wp_localize_script( 'calendarizeit-gutenberg', 'RHC_GUTENBERG', array(
			'defaultview_options' => $this->pop_to_gutenberg_options( apply_filters('rhc_views', array(
						''			=> __('--choose--','rhc'),
						'month'		=> __('Month','rhc'),
						'basicWeek'	=> __('Week','rhc'),
						'basicDay'	=> __('Day','rhc'),
						'agendaWeek'=> __('Agenda Week','rhc'),
						'agendaDay'	=> __('Agenda Day','rhc'),
						'rhc_event'	=> __('Events list','rhc')
					))),
			'attributes'	=> $this->get_attributes_for_register_block_type(),
			'ajax_url'		=> site_url('/wp-json/gutenberg/v1/block-renderer/calendarizeit-gutenberg/calendarizeit'),
			'post_types'	=> $post_types,
		) );

		global $rhc_plugin;

		if( !function_exists('register_block_type') ){
			return;
		}

		register_block_type( 'calendarize-it/calendarizeit', array(
			'render_callback' => array( &$rhc_plugin->shortcode_calendarize, 'calendarizeit' ),
			//'render_callback' => array( &$this, 'dummy_content'),
			'editor_script' => 'calendarizeit-gutenberg',
			'editor_style'	=> 'calendarizeit-gutenberg-css',
			/*
			'attributes'	=> array(
				'test'	=> array(
					'type'	=> 'string'
				),
				'test2'	=> array(
					'type'	=> 'string'
				),
				'defaultview' => array(
					'type' 	=> 'string'
				),
			)
			*/
			'attributes' => $this->get_attributes_for_register_block_type()
		) );
	}

	function get_rhc_taxonomies(){
		global $rhc_plugin;
		//$rhc_plugin->shortcode_calendarize	
		echo "<pre>";
		
		echo "</pre>";
	}

	function get_attributes_for_register_block_type(){
		global $rhc_plugin;

		$options = $this->get_calendarizeit_options();

		$arr = array();

		$arr['gutenberg'] = array(
			'type' 		=> 'string',
			'default' 	=> '1'
		);

		foreach( $options[0]->options as $t ){
			if( property_exists( $t, 'id' ) ){

				$id = $this->get_parameter_name_from_option_name( $t );

				if( '[]' == substr( $id, -2 ) ){
					$id = substr( $id, 0, -2 );
					$type = 'array';
				}else{
					$type = 'string';
				}

//echo "id $id\n";

				$option_name = $this->get_option_name( $t );

				$default = '';
				if( property_exists( $t, 'default' ) ){
					$default = $t->default;
				}
//echo 'opt_name '.$option_name."\n";
				$default = $rhc_plugin->get_option( $option_name, $default, true );

				if( $type == 'array' && !is_array( $default ) ){
					$default = explode(',', $default);
				}

				//bug, when no checkbox is set, default takesover and does not match what is checked
				if( $type == 'array' && $t->type=='checkbox' ){
					$default = array();
				}

				if( empty($default) ){
					$arr[$id] = array(
						'type' 		=> $type
					);
				}else{
					$arr[$id] = array(
						'type' 		=> $type,
						'default' 	=> $default
					);
				}


			}
		}

		return $arr;
	}

	function get_option_name( $t ){
		if( property_exists( $t, 'name' ) ){
			$str = $t->name;
			if( '[]' == substr( $str, -2 ) ){
				$str = substr( $str, 0, -2 );
			}
		}else{
			$str = $t->id;
		}

		return $str;
	}

	function get_parameter_name_from_option_name( $t ){
		if( property_exists( $t, 'name' ) ){
			$str = $t->name;
		}else{
			$str = $t->id;
		}

		$prefix = 'cal_';
		if (substr($str, 0, strlen($prefix)) == $prefix) {
			$str = substr($str, strlen($prefix));
		}

		return $str;
	}

	function get_calendarizeit_options(){
		$t = array();
		//options in RightHere options syntax.
		if(!defined('RHC_GUTENBERG'))define('RHC_GUTENBERG', 1);
		require RHC_PATH . 'includes/options.calendarize_shortcode.php';

		$t = apply_filters( 'pop_calendarizeit_options_for_vc_params', $t );

		$this->set_pop_conditional_options( $t, $i );

		return $t;
	}

	//taken from class.rhc_visual_composer.php
	function set_pop_conditional_options( &$t, $i ){
		$t[$i]->options[]=(object)array(
				'id'			=> 'vc_tab_condition',
				'type' 			=> 'vc_tab',
				'label'			=> __('Conditions','rhc'),
				'vc_tab'		=> true //flat the start of a tab in vc.
			);

		$t[$i]->options[]=(object)array(
				'id'	=> 'cal_capability',
				'type'	=> 'text',
				'label' => __('Permission (capability)','rhc'),
				'description' => __( 'If used, the shortcode will only display if the user is logged in and have the specific capability.', 'rhc')
			);
		$conditional_tags = apply_filters( 'postinfo_allowed_conditional_tags', array(
				'is_home',
				'is_front_page',
				'is_singular',
				'is_page',
				'is_single',
				'is_sticky',
				'is_category',
				'is_tax',
				'is_author',
				'is_archive',
				'is_search',
				'is_attachment',
				'is_tag',
				'is_date',
				'is_paged',
				'is_main_query',
				'is_feed',
				'is_trackback',
				'in_the_loop',
				'is_user_logged_in'
				));
		$j = 0;
		foreach($conditional_tags as $is_condition){
			$tmp=(object)array(
				'id'			=> 'cal_conditional_tag_'.$is_condition,
				'name'			=> 'cal_conditional_tag[]',
				'type'			=> 'checkbox',
				'option_value'	=>$is_condition,
				'default'		=> '',
				'label'			=> $is_condition,
				'vc_label' 		=> __('Conditional Render','rhc')
			);
			if($j==0){
				$tmp->description = __("Check the conditions to test for displaying the shortcode.  Leave empty to display everywhere, included feeds and trackbacks.",'rhc');
			}
			$t[$i]->options[]=$tmp;
			$j++;
		}
	}


	function rhc_sample_block_categories( $categories, $post ){
		return array_merge(
			$categories,
			array(
				array(
					'slug' => 'calendarize-it',
					'title' => __( 'Calendarize it!', 'rhc' ),
				),
			)
		);
	}

	function rhc_load_frontend_only_scripts_in_admin($n=null){
		return true;
	}

	function dummy_content( $atts ){
		return "<p>DUMMY CONTENT</p>";
	}
}

