<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class rhc_event_attendance_mode_metabox {
	var $uid=0;
	var $post_type;
	var $debug=false;
	function __construct($post_type=RHC_EVENTS,$debug=false){
		$this->debug = $debug;
		if(!class_exists('post_meta_boxes'))
			require_once('class.post_meta_boxes.php');		
		$this->post_type = $post_type;

		$this->post_meta_boxes = new post_meta_boxes(array(
			'post_type'=>$post_type,
			'options'=>$this->metaboxes(),
			'styles'=>array(),
			'scripts'=>array(),
			'metabox_meta_fields' =>  'amode_meta_fields',
			'pluginpath'=>RHC_PATH
		));
		
		$this->post_meta_boxes->save_fields = array('rhc_attendance_mode','rhc_event_status');
	
		//-- added dec 4, gutenberg is no longer loading the section where saved field was output.
		$this->post_meta_boxes->metabox_meta_fields_arr = $this->post_meta_boxes->save_fields;	
	}
	
	function metaboxes($t=array()){
		global $rhc_plugin;
		$i = count($t);
		//------------------------------		
		$i++;
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhc_attendance_mode_mbox'; 
		$t[$i]->label 		= __('Structured Data','rhc');
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;
		$t[$i]->context = 'side';
		$t[$i]->priority = 'low';
		$t[$i]->options = array(
			(object)array(
				'id'			=> 'rhc_attendance_mode',
				'type'			=> 'select',
				'name'			=> 'rhc_attendance_mode',
				'label'			=> __('Attendance mode','rhc'),
				'options'		=> array(
					''	=> __('choose','rhc'),
					'OfflineEventAttendanceMode' 	=> __('Offline','rhc'),
					'OnlineEventAttendanceMode' 	=> __('Online','rhc'),
					'MixedEventAttendanceMode' 		=> __('Mix','rhc')
				),
				'save_option'	=> true,
				'load_option'	=> true
			),	
			
			
				
			(object)array(
				'id'			=> 'rhc_event_status',
				'type'			=> 'select',
				'name'			=> 'rhc_event_status',
				'label'			=> __('Event Status','rhc'),
				'options'		=> array(
					''	=> __('choose','rhc'),
					'eventcancelled' 	=> __('EventCancelled','rhc'),
					'eventmovedonline' 	=> __('EventMovedOnline','rhc'),
					'eventpostponed' 	=> __('EventPostponed','rhc'),
					'eventrescheduled' 	=> __('EventRescheduled','rhc'),
					'eventscheduled' 	=> __('EventScheduled','rhc')
				),
				'save_option'	=> true,
				'load_option'	=> true
			),		
	
			(object)array(
				'type'=>'clear'
			)
		);	
		//------------------------------		
		
		//----- 
		return $t;
	}	
}
?>