<?php

class rhc_wpml_string_translation {

    public function remove_option( $option_name ) {
        $option_names = get_option( '_icl_admin_option_names' );

        if ( isset( $option_names[ $option_name ] ) ) {
            unset( $option_names[ $option_name ] );
            update_option( '_icl_admin_option_names', $option_names );
        }
    }

}