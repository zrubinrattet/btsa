��    %      D  5   l      @     A  b   H     �     �     �     �     �     �     �     �     �     �     �                    '  
   6  B   A  /   �  U   �     
          %     ,     9  
   H     S  
   b  
   m     x  8   �     �     �     �       �  !     �  x   	     |	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     
  V   
  /   p
  S   �
     �
  ,   �
     (     /     ?  	   U     _  	   m     w     �  4   �     �     �     �     �     
      #       $                              "             	                                                                                 !                %                     Accept After adding the initial event simply click on any date in order to add the arbitrary repeat date. Calendar Cancel Color Date Date format Dates Day End Date End DateTime End date End datetime End time Event Details Exclude Excluded dates Go to date In addition to recurring rules you can set arbitrary repeat dates. Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec January,February,March,April,May,June,July,August,September,October,November,December Month No repeat dates set. Repeat Repeat dates Reset settings Start Date Start DateTime Start date Start time Sun,Mon,Tue,Wed,Thu,Fri,Sat Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday Update calendar Week taxonomy general nameCalendar taxonomy singular nameCalendar Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Calendarize it! for WordPress
PO-Revision-Date: 2020-03-03 15:58-0500
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: calendarize-it.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: da
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
X-Poedit-SearchPathExcluded-1: vendor
 Accepter Når du har tilføjet den første event skal du blot klikke på en dato for at tilføje den vilkårlige gentagelsesdato. Kalender Annuller Farve Dato Datoformat Datoer Dag Slutdato Slut DatoTid Slutdato Slut datotid Sluttid Eventdetaljer Ekskludér Udeladte datoer Gå til dato Ud over tilbagevendende regler for events kan du angive vilkårlige gentagelsesdatoer. Jan,Feb,Mar,Apr,Maj,Jun,Jul,Aug,Sep,Okt,Nov,Dec Januar,Februar,Marts,April,Maj,Juni,Juli,August,September,Oktober,November,December Måned Der er ikke angivet nogen gentagelsesdatoer. Gentag Gentagne datoer Nulstil indstillinger Startdato Start DatoTid Startdato Starttid Søn,Man,Tir,Ons,Tor,Fre,Lør Søndag,Mandag,Tirsdag,Onsdag,Torsdag,Fredag,Lørdag Opdater kalender Uge Kalender Kalender 