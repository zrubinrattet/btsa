<?php

get_header();

PartialUtil::get('tiles.Main');

ComponentUtil::get('nav.Main');

PartialUtil::get('logo.Main', array( 'bg_color' => MiscUtil::get_color('green') ));

$qo = get_queried_object();
error_log(var_export($qo, true));
?>

<h1 class="pagetitle">
	<?php
		echo $qo->label;
		?>
		<script type="text/javascript">
			var post_type = '<?php echo $qo->name; ?>';
		</script>
		<?php
	?>
</h1>

<section class="archiveheading">
	<h2 class="archiveheading-title">
		<?php
		if( $qo->name === 'lessonplans' ):
			the_field('lesson_plans_heading_title', 'option');
		endif;
		?>
	</h2>
	<div class="archiveheading-subtitle">
		<?php
		if( $qo->name === 'lessonplans' ):
			the_field('lesson_plans_heading_subtitle', 'option');
		endif;
		?>
	</div>
</section>
<section class="archivefilters">
	<div class="archivefilters-wrapper">
		<?php
			if( in_array($qo->name, array('lessonplans', 'post')) ):
				
				foreach( get_object_taxonomies( $qo->name, 'object' ) as $taxonomy ):?>
					<select class="archivefilters-wrapper-select" name="<?php echo $taxonomy->name; ?>" data-placeholder="<?php echo $taxonomy->label ?>" multiple autocomplete="off" data-width="100%">
						<?php foreach( get_terms($taxonomy->name) as $term ): ?>
							<option value="<?php echo $term->term_id ?>"><?php echo $term->name; ?></option>
						<?php endforeach; ?>
					</select>
				<?php
				endforeach;

			endif;
		?>
	</div>
</section>
<section class="pagewrap">
	<?php
		if( in_array($qo->name, array('lessonplans', 'post')) ):
			PartialUtil::get('module.Feed', array(
				'feed_module_id' => 'archivefeed',
				'feed_module_post_type' => $qo->name,
				'feed_module_max_posts' => 10,
				'feed_module_cta' => array(
					'title' => 'View More',
					'url' => '#',
					'target' => '',
					'isarchivefeed' => true,
				),
			));
		endif;
	?>
</section>
<?php

ComponentUtil::get('nav.Footer');

get_footer();

?>