tinymce.PluginManager.add('font_weights', function( editor, url ) {
	editor.addButton( 'font_weights', {
		text: 'Font Weight',
		icon: false,
		type: 'menubutton',
		menu: [
			{
				text: '300',
				onclick: function() {
					editor.insertContent('<span style="font-weight: 300;">' + editor.selection.getContent() + '</span>');
				}
			},
			{
				text: '400',
				onclick: function() {
					editor.insertContent('<span style="font-weight: 400;">' + editor.selection.getContent() + '</span>');
				}
			},
			{
				text: '500',
				onclick: function() {
					editor.insertContent('<span style="font-weight: 500;">' + editor.selection.getContent() + '</span>');
				}
			},
			{
				text: '700',
				onclick: function() {
					console.log('<span style="font-weight: 700;">' + editor.selection.getContent() + '</span>');
					editor.insertContent('<span style="font-weight: 700;">' + editor.selection.getContent() + '</span>');
				}
			},
			{
				text: '900',
				onclick: function() {
					editor.insertContent('<span style="font-weight: 900;">' + editor.selection.getContent() + '</span>');
				}
			},
		]
	});
});