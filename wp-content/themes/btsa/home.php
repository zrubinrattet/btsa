<?php

get_header();

PartialUtil::get('tiles.Main');

ComponentUtil::get('nav.Main');

PartialUtil::get('logo.Main', array( 'bg_color' => MiscUtil::get_color('green') ));

?>

<h1 class="pagetitle">
		Blog
		<script type="text/javascript">
			var post_type = 'post';
		</script>
		<?php
	?>
</h1>

<section class="archivefilters">
	<div class="archivefilters-wrapper">
		<?php
			if( in_array('post', array('lessonplans', 'post')) ):
				
				foreach( get_object_taxonomies( 'post', 'object' ) as $taxonomy ): if( $taxonomy->name === 'post_format' ) continue; ?>
					<select class="archivefilters-wrapper-select" name="<?php echo $taxonomy->name; ?>" data-placeholder="<?php echo $taxonomy->label ?>" multiple autocomplete="off" data-width="100%">
						<?php foreach( get_terms($taxonomy->name) as $term ): ?>
							<option value="<?php echo $term->term_id ?>"><?php echo $term->name; ?></option>
						<?php endforeach; ?>
					</select>
				<?php
				endforeach;

			endif;
		?>
	</div>
</section>
<section class="pagewrap">
	<?php
		PartialUtil::get('module.Feed', array(
			'feed_module_id' => 'archivefeed',
			'feed_module_post_type' => 'post',
			'feed_module_max_posts' => 10,
			'feed_module_cta' => array(
				'title' => 'View More',
				'url' => '#',
				'target' => '',
				'isarchivefeed' => true,
			),
		));
	?>
</section>
<?php

ComponentUtil::get('nav.Footer');

get_footer();

?>