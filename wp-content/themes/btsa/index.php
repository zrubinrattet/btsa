<?php

get_header();

PartialUtil::get('tiles.Main');

ComponentUtil::get('nav.Main');

PartialUtil::get('logo.Main', array( 'bg_color' => MiscUtil::get_color('green') ));

?>

<h1 class="pagetitle"><?php echo $post->post_title; ?></h1>
<div class="pagewrap index">
	<?php
		if( has_post_thumbnail( $post->ID ) ):
		?>
			<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0]; ?>" class="pagewrap-featuredimage">
		<?php
		endif;
		echo apply_filters( 'the_content', do_shortcode( $post->post_content ) );
	?>
</div>
<?php if( comments_open() ): ?>
	<div class="commentswrap">
		<?php comments_template(); ?>
	</div>
<?php
endif;

ComponentUtil::get('nav.Footer');

get_footer();

?>