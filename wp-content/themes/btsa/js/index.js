import $ from 'jquery';
import MainNav from './modules/MainNav.js';
import HomeInpageNav from './modules/HomeInpageNav.js';
import HomeHeroModal from './modules/HomeHeroModal.js';
import ArchiveFeed from './modules/ArchiveFeed.js';
import LessonPlan from './modules/LessonPlan.js';
import Gallery from './modules/Gallery.js';
import FindLegislators from './modules/FindLegislators.js';
import FindDistrict from './modules/FindDistrict.js';

window.$ = $;

window.APP = {
	modules : {
		'MainNav' : MainNav,
		'HomeInpageNav' : HomeInpageNav,
		'ArchiveFeed' : ArchiveFeed,
		'LessonPlan' : LessonPlan,
		'Gallery' : Gallery,
		'FindLegislators' : FindLegislators,
		'FindDistrict' : FindDistrict,
		'HomeHeroModal' : HomeHeroModal,
	},
	_init : () => {
		for (const key in window.APP.modules) {
			window.APP.modules[key]._init();
		}
	}
};

window.APP._init();