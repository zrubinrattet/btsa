import select2 from 'select2';

let ArchiveFeed = {
	_init : () => {
		select2($);
		$(document).ready(ArchiveFeed._ready);
	},
	_ready : () => {
		// build the select2s
		$('.archivefilters-wrapper-select').each( (index, el) => {
			$(el).select2({
				minimumResultsForSearch: Infinity,
				placeholder : el.dataset.placeholder,
			});
		} );

		// set vars
		ArchiveFeed.filters = $('.archivefilters-wrapper-select');
		ArchiveFeed.viewmore = $('#archivefeed .feedmodule-wrapper-cta.archivefeed');

		// listen for shit
		ArchiveFeed.filters.on('change', ArchiveFeed._summonFeedChange);
		ArchiveFeed.viewmore.on('click', ArchiveFeed._summonFeedChange);
	},
	_summonFeedChange : (e) => {
		// cuz viewmore is an a tag
		e.preventDefault();

		let data = {
			action : 'archive_feed',
			post_type : post_type,
			offset : 0,
			terms : (function(ArchiveFeed){
				let terms = [];
				ArchiveFeed.filters.find(':selected').each( (index, el) => terms.push(el.value));
				return terms;
			})(ArchiveFeed),
		};

		// if there's a button (more posts to load), we need to append the content
		if( $(e.target).hasClass('feedmodule-wrapper-cta') ){
			data.append = true;
			data.offset = $('.pagewrap .feedmodule-wrapper-posts-post').length;
		}
		else{
			data.append = false;
		}

		$.post(ajax_url, data, ArchiveFeed._updateFeed.bind(null, data.append));
	},
	_updateFeed : (append, res, status, xhr) => {
		res = JSON.parse(res);
		if( !res.remaining ){
			$('.pagewrap .feedmodule-wrapper-cta').css('display', 'none');
		}
		else{
			$('.pagewrap .feedmodule-wrapper-cta').css('display', 'inline-block');
		}
		if( append ){
			res.posts.forEach( el => $('.pagewrap .feedmodule-wrapper-posts').append(el) );
		}
		else{
			$('.pagewrap .feedmodule-wrapper-posts').empty();
			res.posts.forEach( el => $('.pagewrap .feedmodule-wrapper-posts').append(el) );
		}
	}
};

export default ArchiveFeed;