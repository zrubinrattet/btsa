let FindDistrict = {
	_init : () => {
		$(document).ready(FindDistrict._ready);
	},
	_ready : () => {
		FindDistrict.$district = $('.eduboardmembers-form-selectfield-select');
		FindDistrict.$submit = $('.eduboardmembers-form-submit');
		FindDistrict.$form = $('.eduboardmembers-form');
		FindDistrict.$email = $('.eduboardmembers-email');
		FindDistrict.$emailField = $('.eduboardmembers-email-input[name="email"]');
		FindDistrict.$nameField = $('.eduboardmembers-email-input[name="name"]');
		FindDistrict.$messageField = $('.eduboardmembers-email-textarea');
		FindDistrict.$members = $('.eduboardmembers-members');
		FindDistrict.$messages = $('.eduboardmembers-messages');

		if( FindDistrict.$district.length > 0 ){
			FindDistrict.$form.on('submit', FindDistrict._districtSubmitHandler);
		}
	},
	_districtSubmitHandler : (e) => {
		e.preventDefault();

		if( typeof eduBoardMembers != 'undefined' ){
			// get board member by district
			let member = eduBoardMembers[eduBoardMembers.map( ( member ) => {
				return member.edu_board_member_district;
			} ).indexOf(FindDistrict.$district.val())];

			// hide the form
			FindDistrict.$form.addClass('hidden');

			// show the email
			FindDistrict.$email.addClass('visible');

			// add the member
			FindDistrict.$members.append(`
				<div class="eduboardmembers-members-member">
					<img class="eduboardmembers-members-member-image" src="${member.edu_board_member_image.url}">
					<div class="eduboardmembers-members-member-text">
						<strong>${member.edu_board_member_name}</strong><br/>(District #${member.edu_board_member_district})
					</div>
				</div>`);

			// set the email fields
			FindDistrict.$messageField.val(`Dear ${member.edu_board_member_name},

As a strong supporter of the Beverley Taylor Sorenson Arts Learning Program, I’d like to thank you for all you’ve done to help make sure this incredible program continues to get much needed funding. I believe that it is very important to do everything we can to keep the arts in our Utah schools. The BTS Program is a great example of how beneficial the arts can be when they are integrated into education. Not only do the arts help improve academic learning, but the arts also play a huge role in positively affecting students’ overall wellbeing and personal growth.

I know that thanks to your continued support, more students than ever before have the BTS Program in their schools. Not all students are able to experience the benefits of an arts-integrated education. I ask that you continue to show support to the BTS Program by doing what you can to make sure the requested funding is approved in the upcoming legislative session. That way, the BTS Program can continue to grow and improve the lives and academic experiences of all students across our great state.

Thank you again for your support of the BTS Program, and thank you for your service to the people—and students—of Utah.

Sincerely,
`);
			$('.eduboardmembers-email').on('submit', FindDistrict._emailSubmitHandler.bind(null, member));
		}
	},
	_emailSubmitHandler : (member, e) => {
		e.preventDefault();
		let res = $.post(ajax_url, {
			action: 'email_legislator',
			nonce: $('.eduboardmembers-email-nonce').val(),
			to: member.edu_board_member_email,
			message: FindDistrict.$messageField.val(),
			fromEmail: $('.eduboardmembers-email-input[name="email"]').val(),
			fromName: $('.eduboardmembers-email-input[name="name"]').val(),
		}, FindDistrict._emailSubmitCompleteHandler);
	},
	_emailSubmitCompleteHandler : (res, status, xhr) => {
		if( res ){
			FindDistrict.$email.removeClass('visible');
			FindDistrict.$members.addClass('hidden');
			FindDistrict.$messages.addClass('visible').empty();
			FindDistrict.$messages.append('Thanks for writing to your education board member! BTSALP is very grateful for your support!');
		}
		else{
			FindDistrict.$messages.addClass('visible').empty();
			FindDistrict.$messages.append('We could not email your board member. Sorry.');
		}
	},
};

export default FindDistrict;