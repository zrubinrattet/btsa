let FindLegislators = {
	_init : () => {
		$(document).ready(FindLegislators._ready);
	},
	_ready : () => {
		// set some vars
		FindLegislators.$form = $('.findlegislator-form');
		FindLegislators.$email = $('.findlegislator-email');
		FindLegislators.$legislators = $('.findlegislator-legislators');
		FindLegislators.$messages = $('.findlegislator-messages');

		// if the find legislator form exists
		if( FindLegislators.$form.length > 0 ){
			FindLegislators.$form.on('submit', FindLegislators._formSubmitHandler);
		}
	},
	_formSubmitHandler : (e) => {
		e.preventDefault();

		let address = $('.findlegislator-form-input[name="address"]').val();
		let zip = $('.findlegislator-form-input[name="zip"]').val();

		$.get('https://nominatim.openstreetmap.org/search?street=' + address + '&postalcode=' + zip + '&format=json', {}, (res) => {
			if( res.length > 0 ){
				FindLegislators.$messages.empty();
				$.get('https://v3.openstates.org/people.geo', {
					// btsa old key
					// apikey : '724f61c8-9e2e-4e1b-926a-fe22d71b907b',
					// methodcom new key
					apikey: '7b44d267-5fc8-4103-912d-b9205fe63b8e',
					lat: res[0].lat,
					lng: res[0].lon,
				}, FindLegislators._formSubmitCompleteHandler);
			}
			else{
				FindLegislators.$messages.empty();
				FindLegislators.$messages.append('Your input could not be geolocated. Please double-check your entries.');
			}
		} );

	},
	_formSubmitCompleteHandler : (res, status, xhr) => {
		// if we got a response
		if( typeof res.results != 'undefined' && res.results.length > 0 ){
			FindLegislators.$messages.empty();
			// init the names & emails
			let emails = [];
			let names = [];

			// add the legislator photos
			res.results.forEach( (legislator) => {
				FindLegislators.$legislators.append(`
					<div class="findlegislator-legislators-legislator">
						<img class="findlegislator-legislators-legislator-image" src="${legislator.image}">
						<div class="findlegislator-legislators-legislator-text">
							<strong>${legislator.name}</strong><br/>(${legislator.party})
						</div>
					</div>`);
				emails.push(legislator.email);
				names.push(legislator.name);
			} );

			// show the email form & hide the form
			FindLegislators.$form.addClass('hidden');
			FindLegislators.$email.addClass('visible');

			// build the email body
			let emailbody = `Dear ${names.join(', ').replace(/,([^,]*)$/,'\ and$1')},

As a strong supporter of the Beverley Taylor Sorenson Arts Learning Program, I’d like to thank you for all you’ve done to help make sure this incredible program continues to get much needed funding. I believe that it is very important to do everything we can to keep the arts in our Utah schools. The BTS Program is a great example of how beneficial the arts can be when they are integrated into education. Not only do the arts help improve academic learning, but the arts also play a huge role in positively affecting students’ overall wellbeing and personal growth.

I know that thanks to the continued support of legislators like you on Capitol Hill, more students than ever before have the BTS Program in their schools. Not all students are able to experience the benefits of an arts-integrated education. I ask that you continue to show support to the BTS Program by doing what you can to make sure the requested funding is approved in the upcoming legislative session. That way, the BTS Program can continue to grow and improve the lives and academic experiences of all students across our great state.

Thank you again for your support of the BTS Program, and thank you for your service to the people—and students—of Utah.

Sincerely,
`;

			// set the email body
			$('textarea', FindLegislators.$email).val(
				emailbody
			);

			// listen for submit on the email form
			 $('.findlegislator-email').on('submit', FindLegislators._emailSubmitHandler.bind(null, emails, emailbody));
		}
		else{
			FindLegislators.$messages.empty();
			FindLegislators.$messages.append('No legislators listed for that address.');
		}
	},
	_emailSubmitHandler : (emails, emailbody, e) => {
		e.preventDefault();
		$.post(ajax_url, {
			action: 'email_legislator',
			nonce: $('.findlegislator-email-nonce').val(),
			to: emails,
			message: emailbody,
			fromEmail: $('.findlegislator-email-input[name="email"]').val(),
			fromName: $('.findlegislator-email-input[name="name"]').val(),
		}, FindLegislators._emailSubmitCompleteHandler);
	},
	_emailSubmitCompleteHandler : (res, status, xhr) => {
		if( res ){
			FindLegislators.$email.removeClass('visible');
			FindLegislators.$legislators.addClass('hidden');
			FindLegislators.$messages.empty();
			FindLegislators.$messages.append('Thanks for writing to your legislator! BTSALP is very grateful for your support!');
		}
		else{
			FindLegislators.$messages.empty();
			FindLegislators.$messages.append('We could not email your legislators. Sorry.');
		}
	}
};

export default FindLegislators;