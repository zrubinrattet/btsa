import 'lightgallery.js';

let Gallery = {
	_init : () => {
		$(document).ready(Gallery._ready);
	},
	_ready : () => {
		Gallery.galleries = $('.gallerymodule-wrapper');

		if( Gallery.galleries.length > 0 ){
			Gallery.galleries.each( (index, gallery) => {
				lightGallery(gallery, {
					'selector' : '.gallerymodule-wrapper-item[data-src]',
				});
			} );
		}
	},
};

export default Gallery;