let HomeHeroModal = {
	_init : () => {
		$(document).ready(HomeHeroModal._ready);
	},
	_ready : () => {
		if( $('.homeheromodal-wrapper iframe').length > 0 ){
			$('.homehero-wrapper-image').on('click', () => {
				$('.homeheromodal').addClass('visible');
			});
			$('.homeheromodal:not(.homeheromodal-wrapper)').on('click', () => {
				$('.homeheromodal').removeClass('visible');
			})
		}
	}
};

export default HomeHeroModal;
