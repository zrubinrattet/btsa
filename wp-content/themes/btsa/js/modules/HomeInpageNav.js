/**
 * [HomeInpageNav manages the home inpage nav behavior]
 * @type {Object}
 */
let HomeInpageNav = {
	/**
	 * [_init entry point]
	 */
	_init : () => {
		// listen for document ready
		$(document).ready(HomeInpageNav._ready);
	},
	/**
	 * [_ready fires when document ready]
	 */
	_ready : () => {
		// set some vars
		HomeInpageNav.items = $('.homemobileinpagenav-item[data-target], .homedesktopinpagenav-wrapper-item[data-target]');
		HomeInpageNav.sections = $('.homecontent section');

		// set the first section to have the active class
		if( HomeInpageNav.sections.length > 0 ){
			$(HomeInpageNav.sections[0]).addClass('active');
		}

		// set the event listeners
		HomeInpageNav.items.off('click');
		HomeInpageNav.items.on('click', HomeInpageNav._itemsClickHandler);
	},
	/**
	 * [_itemsClickHandler fired when clicking on the home inpage nav items]
	 * @param  {object} e the event object
	 */
	_itemsClickHandler : (e) => {
		// remove the active class from the sections & the items
		HomeInpageNav.sections.removeClass('active');
		HomeInpageNav.items.removeClass('active');
		// reveal the target content
		$('.homecontent section:nth-child(' + e.target.dataset.target + ')').addClass('active');
		// set the active class to the nav item just clicked
		$(e.target).addClass('active');
		// the calendar component doesn't show unless resize event is triggered by window
		if( e.target.dataset.target == 3 ){
			window.dispatchEvent(new Event('resize'));
		}
	}
}
// export module
export default HomeInpageNav;