import TabNav from './TabNav.js';

let LessonPlan = {
	_init : () => {
		$(document).ready(LessonPlan._ready);
	},
	_ready : () => {
		new TabNav('.lpstandards-tabs-tab', '.lpstandards-items-item');
		new TabNav('.lpfootertabs-wrapper-items-item-tab', '.lpfootertabs-wrapper-items-item-content');
	},
};

export default LessonPlan;