/**
 * [MainNav controls the behavior of the main nav]
 * @type {Object}
 */
let MainNav = {
	/**
	 * [_init entry point]
	 */
	_init : () => {
		// listen for document ready
		$(document).ready(MainNav._ready);
	},
	/**
	 * [_ready fires when document is ready]
	 */
	_ready : () => {
		// set some vars
		MainNav.mobileToggle = $('.mainnav-wrapper-mobilebar-toggle');
		MainNav.menu = $('.mainnav-wrapper-menu');
		MainNav.menuItems = $('.mainnav-wrapper-menu-items-item-link, .mainnav-wrapper-menu-items-item-subitems-item-link');

		// set some event listeners
		MainNav.mobileToggle.off('click');
		MainNav.mobileToggle.on('click', MainNav._mobileToggleClickHandler);
		MainNav.menuItems.off('click');
		MainNav.menuItems.on('click', MainNav._menuItemClickHandler);
	},
	/**
	 * [_mobileToggleClickHandler fired when clicking on the mobile nav toggle. Expands/contracts mobile nav]
	 * @param  {object} e the event object
	 */
	_mobileToggleClickHandler : (e) => {
		// prevent default button behavior
		e.preventDefault();
		// toggle the active class
		MainNav.mobileToggle.toggleClass('is-active');
		// slide toggle the menu
		MainNav.menu.slideToggle();
	},
	/**
	 * [_menuItemClickHandler prevent empty hrefs from appending # to the url]
	 * @param  {object} e the event object
	 */
	_menuItemClickHandler : (e) => {
		// bail on adding hash to empty href
		if( $(e.target).attr('href') == '#' ){
			e.preventDefault();
		}
	},
};

export default MainNav;