export default class TabNav{
	constructor(tabSelector = '', itemSelector = ''){
		this.tabSelector = tabSelector;
		this.itemSelector = itemSelector;
		// get the tabs
		this.$tabs = $(tabSelector);
		// get the items
		this.$items = $(itemSelector);

		// are there tabs?
		if( this.$tabs.length > 0 ){
			// set the first to be active
			$(this.$tabs[0]).addClass('tnactive--tab');
			// then listen for the click
			this.$tabs.on('click', this._tabClickHandler.bind(null, this));
		}
		if( this.$items.length > 0 ){
			$(this.$items[0]).addClass('tnactive--item');
		}
	}

	// handle the click
	_tabClickHandler(context, e){
		// prevent default behavior just in case
		e.preventDefault();
		// remove all active classes
		context.$tabs.removeClass('tnactive--tab');
		context.$items.removeClass('tnactive--item');
		// get the target index
		let targetIndex = $(e.target).attr('data-target');
		// reveal the target item
		context.$items.each( (index, el) => {
			if( $(el).attr('data-target') === targetIndex ){
				$(el).addClass('tnactive--item');
			}
		} )
		// add the class to the target nav item
		$(e.target).addClass('tnactive--tab');
	};
}