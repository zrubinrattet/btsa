<?php

get_header();

$tile_images = get_field('tile_images', 'option');

error_log(var_export(get_permalink( get_option( 'page_for_posts' ) ), true));

?>
<section class="homedesktoptiles">
	<div class="homedesktoptiles-tiles">
		<div class="homedesktoptiles-tiles-tile green"></div>
		<div class="homedesktoptiles-tiles-tile"<?php
		if( !empty($tile_images[0]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[0]['url'] . "')"
			));
		endif;
		?>></div>
		<div class="homedesktoptiles-tiles-tile"<?php
		if( !empty($tile_images[1]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[1]['url'] . "')"
			));
		endif;
		?>></div>
		<div class="homedesktoptiles-tiles-tile blue"></div>
		<div class="homedesktoptiles-tiles-tile"></div>
		<div class="homedesktoptiles-tiles-tile"></div>
		<div class="homedesktoptiles-tiles-tile"></div>
		<div class="homedesktoptiles-tiles-tile"></div>
		<div class="homedesktoptiles-tiles-tile aqua"></div>
		<div class="homedesktoptiles-tiles-tile"></div>
		<div class="homedesktoptiles-tiles-tile pink"></div>
		<div class="homedesktoptiles-tiles-tile"<?php
		if( !empty($tile_images[2]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[2]['url'] . "')"
			));
		endif;
		?>></div>
		<div class="homedesktoptiles-tiles-tile"<?php
		if( !empty($tile_images[3]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[3]['url'] . "')"
			));
		endif;
		?>></div>
		<div class="homedesktoptiles-tiles-tile orange"></div>
		<div class="homedesktoptiles-tiles-tile"></div>
		<div class="homedesktoptiles-tiles-tile"></div>
		<div class="homedesktoptiles-tiles-tile"></div>
		<div class="homedesktoptiles-tiles-tile"<?php
		if( !empty($tile_images[4]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[4]['url'] . "')"
			));
		endif;
		?>></div>
	</div>
</section>
<?php

ComponentUtil::get('nav.Main');

PartialUtil::get('logo.Main');

?>
<section class="homehero">
	<div class="homehero-wrapper">
		<?php $hero_image = get_field('home_hero_image', $post->ID); ?>
		<img src="<?php echo $hero_image['url']; ?>" class="homehero-wrapper-image" alt="<?php echo $hero_image['alt']; ?>">
		<div class="homehero-wrapper-copy"><?php the_field('home_hero_copy', $post->ID); ?></div>
	</div>
</section>
<section class="homeheromodal">
	<div class="homeheromodal-wrapper">
		<?php the_field('home_modal_video', $post->ID); ?>
	</div>
</section>
<section class="homemobileinpagenav">
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item green"></div>
	<div class="homemobileinpagenav-item"<?php
		if( !empty($tile_images[0]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[0]['url'] . "')"
			));
		endif;
		?>></div>
	<div class="homemobileinpagenav-item"<?php
		if( !empty($tile_images[1]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[1]['url'] . "')"
			));
		endif;
		?>></div>
	<div class="homemobileinpagenav-item"<?php
		if( !empty($tile_images[2]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[2]['url'] . "')"
			));
		endif;
		?>></div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item blue"></div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item pink"></div>
	<div class="homemobileinpagenav-item"<?php
		if( !empty($tile_images[3]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[3]['url'] . "')"
			));
		endif;
		?>></div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item aqua active" data-target="1">
		<?php echo get_field('home_inpagenav_title1', $post->ID) ? get_field('home_inpagenav_title1', $post->ID) : 'Lesson Plans' ?>
		<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="homemobileinpagenav-item-arrow">
	</div>
	<div class="homemobileinpagenav-item"<?php
		if( !empty($tile_images[4]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[4]['url'] . "')"
			));
		endif;
		?>></div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item green" data-target="2">
		<?php echo get_field('home_inpagenav_title2', $post->ID) ? get_field('home_inpagenav_title2', $post->ID) : 'Training Videos' ?>
		<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="homemobileinpagenav-item-arrow">
	</div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item"<?php
		if( !empty($tile_images[5]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[5]['url'] . "')"
			));
		endif;
		?>></div>
	<div class="homemobileinpagenav-item orange" data-target="3">
		<?php echo get_field('home_inpagenav_title3', $post->ID) ? get_field('home_inpagenav_title3', $post->ID) : 'Events Calendar' ?>
		<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="homemobileinpagenav-item-arrow">
	</div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item blue" data-target="4">
		<?php echo get_field('home_inpagenav_title4', $post->ID) ? get_field('home_inpagenav_title4', $post->ID) : 'News & updates' ?>
		<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="homemobileinpagenav-item-arrow">
	</div>
	<div class="homemobileinpagenav-item"<?php
		if( !empty($tile_images[6]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[6]['url'] . "')"
			));
		endif;
		?>></div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item"></div>
	<div class="homemobileinpagenav-item green"></div>
</section>
<section class="homedesktopinpagenav">
	<div class="homedesktopinpagenav-wrapper">
		<div class="homedesktopinpagenav-wrapper-item active" data-target="1">
			<?php echo get_field('home_inpagenav_title1', $post->ID) ? get_field('home_inpagenav_title1', $post->ID) : 'Lesson Plans' ?>
			<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="homedesktopinpagenav-wrapper-item-arrow">
		</div>
		<div class="homedesktopinpagenav-wrapper-item image"<?php
		if( !empty($tile_images[5]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[5]['url'] . "')"
			));
		endif;
		?>></div>
		<div class="homedesktopinpagenav-wrapper-item" data-target="2">
			<?php echo get_field('home_inpagenav_title2', $post->ID) ? get_field('home_inpagenav_title2', $post->ID) : 'Training Videos' ?>
			<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="homedesktopinpagenav-wrapper-item-arrow">
		</div>
		<div class="homedesktopinpagenav-wrapper-item image"<?php
		if( !empty($tile_images[6]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[6]['url'] . "')"
			));
		endif;
		?>></div>
		<div class="homedesktopinpagenav-wrapper-item" data-target="3">
			<?php echo get_field('home_inpagenav_title3', $post->ID) ? get_field('home_inpagenav_title3', $post->ID) : 'Events Calendar' ?>
			<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="homedesktopinpagenav-wrapper-item-arrow">
		</div>
		<div class="homedesktopinpagenav-wrapper-item image"<?php
		if( !empty($tile_images[7]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[7]['url'] . "')"
			));
		endif;
		?>></div>
		<div class="homedesktopinpagenav-wrapper-item" data-target="4">
			<?php echo get_field('home_inpagenav_title4', $post->ID) ? get_field('home_inpagenav_title4', $post->ID) : 'News updates' ?>
			<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="homedesktopinpagenav-wrapper-item-arrow">
		</div>
	</div>
</section>
<section class="homecontent">
	<?php
	$mb = new ModuleBuilder(array(), 'home_module_builder', $post->ID);
	if( $mb->has_modules() ) $mb->render();
	?>
</section>
<?php

ComponentUtil::get('nav.Footer');

get_footer();

?>