<?php
/*
Template Name: Module Builder
*/

get_header();

PartialUtil::get('tiles.Main');

ComponentUtil::get('nav.Main');
if( $post->post_name == 'get-involved' ){
	PartialUtil::get('logo.GetInvolved', array( 'bg_color' => MiscUtil::get_color(rand(3,7)) ));
}
else{
	PartialUtil::get('logo.Main', array( 'bg_color' => MiscUtil::get_color(rand(3,7)) ));

	?><h1 class="pagetitle"><?php echo $post->post_title; ?></h1><?php
}

?>


<?php

$mb = new ModuleBuilder(array(), 'module_builder', $post->ID);

if( $mb->has_modules() ): ?>
	<div class="pagewrap">
		<?php $mb->render(); ?>
	</div>
<?php

endif;

ComponentUtil::get('nav.Footer');

get_footer();

?>