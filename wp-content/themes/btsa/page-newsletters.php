<?php

get_header();

PartialUtil::get('tiles.Main');

ComponentUtil::get('nav.Main');

PartialUtil::get('logo.Main', array( 'bg_color' => MiscUtil::get_color('green') ));

// get the newsletters
$newsletters = get_posts(array(
	'posts_per_page' => -1,
	'post_status' => 'inherit',
	'post_type' => 'attachment',
	'tax_query' => array(
		array(
			'taxonomy'  => 'category',
			'field'     => 'slug',
			'terms'     => array('newsletter'),
		)
	),
));

// get all the years
$years = array();

foreach( $newsletters as $newsletter ){
	$years[] = $year = date('Y', strtotime(get_field('newsletter_date', $newsletter->ID)));
}

// dedupe
$years = array_reverse(array_values(array_unique($years)));

$latest_year = max($years);

// get the year we're filtering for
if( !empty($_GET['nyear']) ){
	$current_year = $_GET['nyear'];
}
// if no year we're filtering get the latest year for the newsletters
else{
	$current_year = $latest_year;
}

?>

<h1 class="pagetitle"><?php echo $post->post_title; ?></h1>
<div class="pagewrap">
	<div class="newsletters">
		<section class="newsletters-header">
			<h2 class="newsletters-header-year">
				<?php
					echo $current_year;
				?>
			</h2>
			<div class="newsletters-header-arrows">
				<?php
					// if there's a previous year
					if( array_search($current_year, $years) !== 0 ){
						$prev_year = $years[array_search($current_year, $years) - 1];
					}

					// if there's a next year
					if( array_search($current_year, $years) !== count($years) - 1 ){
						$next_year = $years[array_search($current_year, $years) + 1];
					}
				?>
				<a href="<?php echo get_permalink( $post->ID ) . (isset($prev_year) ? '?nyear=' . $prev_year : '') ?>" class="newsletters-header-arrows-arrow prev<?php echo isset($prev_year) ? '' : ' disabled' ?>">
					<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-left.png" class="newsletters-header-arrows-arrow-icon">
				</a>
				<a href="<?php echo get_permalink( $post->ID ) . (isset($next_year) ? '?nyear=' . $next_year : '') ?>" class="newsletters-header-arrows-arrow next<?php echo isset($next_year) ? '' : ' disabled' ?>">
					<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-right.png" class="newsletters-header-arrows-arrow-icon">
				</a>
			</div>
		</section>
		<section class="newsletters-items">
			<?php foreach( $newsletters as $newsletter ) : if( date('Y', strtotime(get_field('newsletter_date', $newsletter->ID))) != $current_year ) continue; ?>
				<a target="_blank" href="<?php echo wp_get_attachment_url( $newsletter->ID ); ?>" class="newsletters-items-item">
					<div class="newsletters-items-item-top">
						<span class="newsletters-items-item-top-issue">#<?php the_field('newsletter_issue', $newsletter->ID); ?></span>
						<span class="newsletters-items-item-top-date"><?php echo date('F Y', strtotime(get_field('newsletter_date', $newsletter->ID))); ?></span>
					</div>
					<div class="newsletters-items-item-bottom">
						<?php the_field('newsletter_title', $newsletter->ID); ?>
					</div>
				</a>
			<?php endforeach; ?>
		</section>
		<section class="newsletters-footer">
			<?php the_field('newsletters_footer', $post->ID); ?>
		</section>
	</div> 
</div>
<?php

ComponentUtil::get('nav.Footer');

get_footer();

?>