<?php
get_header();

PartialUtil::get('tiles.Main');

ComponentUtil::get('nav.Main');

PartialUtil::get('logo.Main', array( 'bg_color' => MiscUtil::get_color('green') ));
?>
<div class="pagewrap">
	<h1 class="pagetitle"><?php echo $post->post_title; ?></h1>
	<section class="lpoverview">
		<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0]; ?>" class="lpoverview-image" alt="<?php echo get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true); ?>">
		<table class="lpoverview-table">
			<tbody>
				<tr class="lpoverview-table-row">
					<td class="lpoverview-table-row-cell label">Author: </td>
					<td class="lpoverview-table-row-cell">
						<?php
							if( !empty( $terms = wp_get_post_terms( $post->ID, 'specialist' ) ) ){
								echo MiscUtil::comma_separate_with_ampersand( array_column($terms, 'name') );
							}
						?>
					</td>
				</tr>
				<tr class="lpoverview-table-row">
					<td class="lpoverview-table-row-cell label">Year: </td>
					<td class="lpoverview-table-row-cell">
						<?php
							the_field('lessonplan_year', $post->ID);
						?>
					</td>
				</tr>
				<tr class="lpoverview-table-row">
					<td class="lpoverview-table-row-cell label">Artform: </td>
					<td class="lpoverview-table-row-cell">
						<?php
							if( !empty( $terms = wp_get_post_terms( $post->ID, 'art_form' ) ) ){
								echo MiscUtil::comma_separate_with_ampersand( array_column($terms, 'name') );
							}
						?>
					</td>
				</tr>
				<tr class="lpoverview-table-row">
					<td class="lpoverview-table-row-cell label">Subjects: </td>
					<td class="lpoverview-table-row-cell">
						<?php
							if( !empty( $terms = wp_get_post_terms( $post->ID, 'subject' ) ) ){
								echo MiscUtil::comma_separate_with_ampersand( array_column($terms, 'name') );
							}
						?>
					</td>
				</tr>
				<tr class="lpoverview-table-row">
					<td class="lpoverview-table-row-cell label">Grade: </td>
					<td class="lpoverview-table-row-cell">
						<?php
							if( !empty( $terms = wp_get_post_terms( $post->ID, 'grade_level' ) ) ){
								echo MiscUtil::comma_separate_with_ampersand( array_column($terms, 'name') );
							}
						?>
					</td>
				</tr>
				<tr class="lpoverview-table-row">
					<td class="lpoverview-table-row-cell label">Duration: </td>
					<td class="lpoverview-table-row-cell">
						<?php
							if( !empty( $terms = wp_get_post_terms( $post->ID, 'duration' ) ) ){
								echo MiscUtil::comma_separate_with_ampersand( array_column($terms, 'name') );
							}
						?>
					</td>
				</tr>
				<tr class="lpoverview-table-row">
					<td class="lpoverview-table-row-cell label">Overview: </td>
					<td class="lpoverview-table-row-cell"><?php the_field('lessonplan_overview', $post->ID); ?></td>
				</tr>
			</tbody>
		</table>
	</section>
	<section class="lpinfobox">
		<div class="lpinfobox-wrapper">
			<?php
			the_field('lessonplan_infobox', $post->ID);
			if( !empty( $pdf = get_field('lessonplan_pdf', $post->ID) ) ): ?>
				<a target="_blank" href="<?php echo $pdf['url']; ?>" class="lpinfobox-pdf">Download PDF Lesson Plan</a>
			<?php endif; ?>
		</div>
	</section>
	<section class="lpstandards">
		<h2 class="lpstandards-title">Standards &amp; Objectives</h2>
		<div class="lpstandards-tabs">
			<?php foreach( array( 'Fine Art Standards', 'Integrated Standards', 'Objectives' ) as $index => $label ): ?>
				<div class="lpstandards-tabs-tab" data-target="<?php echo $index; ?>">
					<?php echo $label; ?>
					<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="lpstandards-tabs-tab-arrow">
				</div>
			<?php endforeach; ?>
		</div>
		<div class="lpstandards-items">
			<?php
				foreach( $items = array(
					get_field('lessonplan_fine_art_standards', $post->ID),
					get_field('lessonplan_integrated_standards', $post->ID),
					get_field('lessonplan_objectives', $post->ID)
				) as $index => $item ):
			?>
			<div class="lpstandards-items-item" data-target="<?php echo $index; ?>"><?php echo $item; ?></div>
			<?php endforeach; ?>
		</div>
	</section>
	<h2 class="lph1">Teaching and Timeline</h2>
	<?php if( !empty( $content = get_field('lessonplan_intro', $post->ID) ) ): ?>
		<section class="lpintro">
			<h3 class="lpintro-title">Introduction</h3>
			<div class="lpintro-wrapper<?php echo str_word_count($content) > 250 ? ' twocol' : ''; ?>">
				<?php echo $content; ?>
			</div>
		</section>
	<?php endif; ?>
	<?php if( !empty( $content = get_field('lessonplan_demo', $post->ID) ) ): ?>
		<section class="lpsection">
			<h3 class="lpsection-title">Demonstration</h3>
			<?php echo $content; ?>
		</section>
	<?php endif; ?>
	<?php if( !empty( $content = get_field('lessonplan_work_period', $post->ID) ) ): ?>
		<section class="lpsection">
			<h3 class="lpsection-title">Work Period</h3>
			<?php echo $content; ?>
		</section>
	<?php endif; ?>
	<?php if( !empty( $content = get_field('lessonplan_closure', $post->ID) ) ): ?>
		<section class="lpsection">
			<h3 class="lpsection-title">Closure/Summary</h3>
			<?php echo $content; ?>
		</section>
	<?php endif; ?>
	<section class="lpfootertabs">
		<div class="lpfootertabs-wrapper">
			<?php
			$footertabs = get_field('lessonplan_footer_tabs', $post->ID);
			if( !empty($footertabs) ): ?>
				<div class="lpfootertabs-wrapper-items">
					<?php foreach( $footertabs as $index => $footertab ):
							// loop through the colors
							$color_index = 0;
							$color_count = count(MiscUtil::get_color('', true));
							if( $index >= $color_count ){
								$color_index = $index === 0 ? 0 : $index - $color_count;
							}
							else{
								$color_index = (int) $index - floor($index / $color_count) * $color_count;
							}
							$bg_color = array_values(MiscUtil::get_color('', true))[ $color_index ];
						?>
						<div class="lpfootertabs-wrapper-items-item">
							<div class="lpfootertabs-wrapper-items-item-tab" data-target="<?php echo $index; ?>"<?php MiscUtil::maybe_render_inline_style(array(
									'background-color' => $bg_color,
								)); ?>>
								<?php echo $footertab['lessonplan_footer_tab_title']; ?>
								<img src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-down.png" class="lpfootertabs-wrapper-items-item-tab-arrow">
							</div>
							<div class="lpfootertabs-wrapper-items-item-content" data-target="<?php echo $index; ?>"><?php echo $footertab['lessonplan_footer_tab_content']; ?></div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php
			endif;
			?>
		</div>
	</section>
	<?php
	ComponentUtil::get('module.Gallery', array(
		'gallery_module_gallery' => get_field('lessonplan_misc_images', $post->ID),
		'gallery_module_type' => 'all',
	));
	?>
</div>
<?php
ComponentUtil::get('nav.Footer');

get_footer();
?>