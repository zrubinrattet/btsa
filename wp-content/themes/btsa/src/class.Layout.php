<?php 
	
	class Layout{
		/**
		 * [$layout the layout for the module builder]
		 * @var array
		 */
		protected $layout = array();
		/**
		 * [$layouts list of all layouts]
		 * @var array
		 */
		public static $layouts = array(
			array(
				'path' => '/src/layouts/class.CalendarModule.php',
				'class' => 'CalendarModule',
			),
			array(
				'path' => '/src/layouts/class.CaseStudyList.php',
				'class' => 'CaseStudyList',
			),
			array(
				'path' => '/src/layouts/class.CTAModule.php',
				'class' => 'CTAModule',
			),
			array(
				'path' => '/src/layouts/class.FeedModule.php',
				'class' => 'FeedModule',
			),
			array(
				'path' => '/src/layouts/class.GalleryModule.php',
				'class' => 'GalleryModule',
			),
			array(
				'path' => '/src/layouts/class.NUpModule.php',
				'class' => 'NUpModule',
			),
			array(
				'path' => '/src/layouts/class.OembedGrid.php',
				'class' => 'OembedGrid',
			),
			array(
				'path' => '/src/layouts/class.WYSIWYG.php',
				'class' => 'WYSIWYG',
			),
		);
		/**
		 * [get_layout retrieve a layout from a layout class]
		 * @return array the layout
		 */
		public function get_layout(){
			return $this->layout;
		}
		/**
		 * [get_layouts retrieve layouts for module builder]
		 * @return array the layouts array
		 */
		final public static function get_layouts(){
			$layouts_arr = array();
			foreach( Layout::$layouts as $layout ){
				require_once(get_template_directory() . $layout['path']);
				$classname = $layout['class'];
				$class = new $classname;
				$layouts_arr[] = $class->get_layout();
			}
			return $layouts_arr;
		}
	}

?>