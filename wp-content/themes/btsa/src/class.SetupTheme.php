<?php 

class SetupTheme{
	public static function _init(){
		// remove the admin bar
		show_admin_bar( false );
		
		// remove the theme edior
		define( 'DISALLOW_FILE_EDIT', true );

		add_action( 'after_setup_theme', 'SetupTheme::after_setup_theme' );
		add_action( 'wp_enqueue_scripts', 'SetupTheme::wp_enqueue_scripts' );
		add_action( 'init', 'SetupTheme::init' );
		// remove customizer from the Appearance menu
		add_action( 'admin_menu', 'SetupTheme::remove_menu_clutter', 9999 );
		// remove the customize button from the themes.php admin page
		add_action( 'admin_head-themes.php', 'SetupTheme::hide_customize_button' );
		// remove the default widgets from the dashboard
		add_action( 'wp_dashboard_setup', 'SetupTheme::disable_default_dashboard_widgets', 999);

		// add add archive pages to wplink
		add_filter( 'wp_link_query', 'SetupTheme::add_archive_pages_to_wplink', 20, 2);

		 // add site styles to wysiwyg editor
		add_filter( 'tiny_mce_before_init', function($mceInit, $editor_id){
			$mceInit['content_css'] = $mceInit['content_css'] . ',' . get_template_directory_uri() . '/build/css/build.css';
			$mceInit['font_formats'] = 'Roboto=roboto, sans-serif; Myriad=myriad pro, sans-serif; Acumin=Acumin Pro, sans-serif;';
			return $mceInit;
		}, 2, 20 );

		// add special font weight menu to tinymce
		add_action('admin_head', function() {
			// check user permissions
			if ( !current_user_can( 'edit_posts' ) &&  !current_user_can( 'edit_pages' ) ) {
				return;
			}
			// check if WYSIWYG is enabled
			if ( 'true' == get_user_option( 'rich_editing' ) ) {
				// add the plugin to tinymce to build the font weights menu
				add_filter( 'mce_external_plugins', function( $plugin_array ) {
					$plugin_array['font_weights'] = get_stylesheet_directory_uri() .'/build/js/tinymce.js';
					return $plugin_array;
				}, 9999 );
				// register new button in the editor
				add_filter( 'mce_buttons', function( $buttons ) {
					array_push( $buttons, 'font_weights' );
					return $buttons;
				}, 9999 );
			}
		});

		add_shortcode('findlegislator', 'SetupTheme::findlegislator_shortcode');

		add_shortcode('finddistrict', 'SetupTheme::finddistrict_shortcode');

		add_action('wp_mail_failed', function( $wp_error ){
		  error_log(var_export($wp_error->get_error_message(), true));
		}, 10, 1);

		
	}
	public static function finddistrict_shortcode($args, $content){
		ob_start();
		$members = get_field('edu_board_members', 'option');
		if( !empty($members) ): ?>
			<script type="text/javascript">
				var eduBoardMembers = <?php echo json_encode($members); ?>;
			</script>
			<div class="eduboardmembers">
				<h2 class="eduboardmembers-title">Write Your State School Board Member</h2>
				<form class="eduboardmembers-form">
					<div class="eduboardmembers-form-selectfield">
						<select class="eduboardmembers-form-selectfield-select">
							<?php foreach( $members as $member ): ?>
								<option value="<?php echo strval($member['edu_board_member_district']); ?>"><?php echo $member['edu_board_member_name'] . ' (District #' . strval($member['edu_board_member_district']) . ')'; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<input type="submit" class="eduboardmembers-form-submit" value="Select District">
				</form>
				<div class="eduboardmembers-members"></div>
				<form class="eduboardmembers-email">
					<input type="text" class="eduboardmembers-email-input" name="name" placeholder="Your Name" required>
					<input type="email" class="eduboardmembers-email-input" name="email" placeholder="Your Email" required>
					<textarea id="message" class="eduboardmembers-email-textarea" required></textarea>
					<input type="hidden" class="eduboardmembers-email-nonce" name="nonce" value="<?php echo wp_create_nonce( 'emaillegislator' ); ?>">
					<input type="submit" class="eduboardmembers-email-submit" value="Send Email">
				</form>
				<div class="eduboardmembers-messages"></div>
			</div>
		<?php
		endif;
		return ob_get_clean();
	}
	public static function findlegislator_shortcode($args, $content){
		ob_start();
		?>
		<div class="findlegislator">
			<h2 class="findlegislator-title">Find Your Legislator and Write to Them</h2>
			<form class="findlegislator-form">
				<input type="text" class="findlegislator-form-input" name="address" placeholder="Street Address" required>
				<input type="text" class="findlegislator-form-input" name="zip" placeholder="Zip Code" required>
				<input type="submit" class="findlegislator-form-submit" value="Find Legislators">
			</form>
			<div class="findlegislator-legislators"></div>
			<form class="findlegislator-email">
				<input type="text" class="findlegislator-email-input" name="name" placeholder="Your Name" required>
				<input type="email" class="findlegislator-email-input" name="email" placeholder="Your Email" required>
				<textarea id="message" class="findlegislator-email-textarea" required></textarea>
				<input type="hidden" class="findlegislator-email-nonce" name="nonce" value="<?php echo wp_create_nonce( 'emaillegislator' ); ?>">
				<input type="submit" class="findlegislator-email-submit" value="Send Email">
			</form>
			<div class="findlegislator-messages"></div>
		</div>
		<?php
		return ob_get_clean();
	}
	public static function customize_post_type($args, $post_type){
		if( 'post' == $post_type ) {
			$args['rewrite'] = array('slug' => 'blog');
		}
		return $args;
	}
	public static function init(){
		SetupTheme::clean_head();
		register_nav_menu( 'main-nav', 'Main Navigation' );

		// remove comment support
	    remove_post_type_support( 'page', 'comments' );

	    if( isset($_GET['post']) ){
	    	$page = get_post($_GET['post']);
		    // remove support for editor on homepage
	        if( $page->post_name === 'home' || get_post_meta( $_GET['post'], '_wp_page_template', true ) === 'page-mb.php' ){
	        	remove_post_type_support('page', 'editor');
	        	remove_post_type_support('page', 'thumbnail');
	        }
	        if( $page->post_name === 'newsletters' ){
	        	remove_post_type_support('page', 'editor');
	        }
	    }
	    // add custom taxonomies/post types
	    SetupTheme::register_taxonomies();
	    SetupTheme::register_post_types();

	    // change labels for default post type to blog post
	    global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = 'Blog Posts';
		$labels->singular_name = 'Blog Post';
		$labels->add_new = 'Add Blog Post';
		$labels->add_new_item = 'Add Blog Post';
		$labels->edit_item = 'Edit Blog Post';
		$labels->new_item = 'Blog Post';
		$labels->view_item = 'View Blog Post';
		$labels->search_items = 'Search Blog Posts';
		$labels->not_found = 'No Blog Posts found';
		$labels->not_found_in_trash = 'No Blog Posts found in Trash';
		$labels->all_items = 'All Blog Posts';
		$labels->menu_name = 'Blog Posts';
		$labels->name_admin_bar = 'Blog Posts';
		$wp_post_types['post']->has_archive = true;

		// give media the category taxonomy
		register_taxonomy_for_object_type( 'category', 'attachment' );

	}
	public static function register_taxonomies(){
		$labels = [
			"name" => "Grade Levels",
			"singular_name" => "Grade Level",
		];

		$args = [
			"label" => "Grade Levels",
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'grade_level', 'with_front' => true, ],
			"show_admin_column" => false,
			"show_in_rest" => true,
			"rest_base" => "grade_level",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
		];
		register_taxonomy( "grade_level", [ "lessonplans" ], $args );

		/**
		 * Taxonomy: Subjects.
		 */

		$labels = [
			"name" => "Subjects",
			"singular_name" => "Subject",
		];

		$args = [
			"label" => "Subjects",
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'subject', 'with_front' => true, ],
			"show_admin_column" => false,
			"show_in_rest" => true,
			"rest_base" => "subject",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
		];
		register_taxonomy( "subject", [ "lessonplans" ], $args );

		/**
		 * Taxonomy: Art Forms.
		 */

		$labels = [
			"name" => "Art Forms",
			"singular_name" => "Art Form",
		];

		$args = [
			"label" => "Art Forms",
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'art_form', 'with_front' => true, ],
			"show_admin_column" => false,
			"show_in_rest" => true,
			"rest_base" => "art_form",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
		];
		register_taxonomy( "art_form", [ "lessonplans" ], $args );

		/**
		 * Taxonomy: Duration.
		 */

		$labels = [
			"name" => "Duration",
			"singular_name" => "Duration",
		];

		$args = [
			"label" => "Duration",
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'duration', 'with_front' => true, ],
			"show_admin_column" => false,
			"show_in_rest" => true,
			"rest_base" => "duration",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
		];
		register_taxonomy( "duration", [ "lessonplans" ], $args );

		/**
		 * Taxonomy: Specialists.
		 */

		$labels = [
			"name" => "Specialists",
			"singular_name" => "Specialist",
		];

		$args = [
			"label" => "Specialists",
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'specialist', 'with_front' => true, ],
			"show_admin_column" => false,
			"show_in_rest" => true,
			"rest_base" => "specialist",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
		];
		register_taxonomy( "specialist", [ "lessonplans" ], $args );
	}
	public static function register_post_types(){
		$labels = array(
			'name'                     => 'Lesson Plans',
			'singular_name'            => 'Lesson Plan',
			'add_new'                  => 'Add New Lesson Plan',
			'add_new_item'             => 'Add New Lesson Plan',
			'edit_item'                => 'Edit Lesson Plan',
			'new_item'                 => 'New Lesson Plan',
			'view_item'                => 'View Lesson Plan',
			'view_items'               => 'View Lesson Plans',
			'search_items'             => 'Search Lesson Plans',
			'not_found'                => 'No Lesson Plans found',
			'not_found_in_trash'       => 'No Lesson Plans found in Trash',
			'parent_item_colon'        => 'Parent Lesson Plan:',
			'menu_name'                => 'Lesson Plans',
			'all_items'                => 'All Lesson Plans',
			'archives'                 => 'Lesson Plan Archives',
			'attributes'               => 'Lesson Plan Attributes',
			'insert_into_item'         => 'Insert into Lesson Plan',
			'uploaded_to_this_item'    => 'Uploaded to this post',
			'filter_items_list'        => 'Filter Lesson Plans list',
			'items_list_navigation'    => 'Lesson Plans list navigation',
			'items_list'               => 'Lesson Plans list',
			'item_published'           => 'Lesson Plan published',
			'item_published_privately' => 'Lesson Plan published privately',
			'item_reverted_to_draft'   => 'Lesson Plan reverted to draft',
			'item_scheduled'           => 'Lesson Plan scheduled',
			'item_updated'             => 'Lesson Plan updated',
		);

		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => 'btsa\'s lesson plans',
			'taxonomies'          => array("grade_level", "subject", "art_form", "duration", "specialist"),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-media-text',
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => 'lessonplans',
			'query_var'           => true,
			'map_meta_cap'        => true,
			'can_export'          => true,
			'rewrite'             => array(
				'slug' => 'lessonplans'
			),
			'capability_type'     => 'post',
			'supports'            => array(
				'title', 'thumbnail', 'editor'
			)
		);

		register_post_type( 'lessonplans', $args );

		$labels = array(
			'name'                     => 'Case Studies',
			'singular_name'            => 'Case Study',
			'add_new'                  => 'Add New Case Study',
			'add_new_item'             => 'Add New Case Study',
			'edit_item'                => 'Edit Case Study',
			'new_item'                 => 'New Case Study',
			'view_item'                => 'View Case Study',
			'view_items'               => 'View Case Studies',
			'search_items'             => 'Search Case Studies',
			'not_found'                => 'No Case Studies found',
			'not_found_in_trash'       => 'No Case Studies found in Trash',
			'parent_item_colon'        => 'Parent Case Study:',
			'menu_name'                => 'Case Studies',
			'all_items'                => 'All Case Studies',
			'archives'                 => 'Case Study Archives',
			'attributes'               => 'Case Study Attributes',
			'insert_into_item'         => 'Insert into Case Study',
			'uploaded_to_this_item'    => 'Uploaded to this post',
			'filter_items_list'        => 'Filter Case Studies list',
			'items_list_navigation'    => 'Case Studies list navigation',
			'items_list'               => 'Case Studies list',
			'item_published'           => 'Case Study published',
			'item_published_privately' => 'Case Study published privately',
			'item_reverted_to_draft'   => 'Case Study reverted to draft',
			'item_scheduled'           => 'Case Study scheduled',
			'item_updated'             => 'Case Study updated',
		);

		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => 'btsa\'s case studies',
			'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-index-card',
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => false,
			'query_var'           => true,
			'map_meta_cap'        => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
			)
		);

		register_post_type( 'casestudy', $args );
	}
	public static function after_setup_theme(){
		add_theme_support( 'html5' );
		add_theme_support( 'post-thumbnails' );	
	}
	public static function wp_enqueue_scripts(){
		SetupTheme::register_styles();
		SetupTheme::enqueue_styles();
		SetupTheme::register_javascript();
		SetupTheme::enqueue_javascript();	

		wp_localize_script( 'theme', 'ajax_url', admin_url( 'admin-ajax.php' ) );
	}
	/*
		Disable Default Dashboard Widgets
		@ https://digwp.com/2014/02/disable-default-dashboard-widgets/
	*/
	public static function disable_default_dashboard_widgets() {
		global $wp_meta_boxes;
		// wp..
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_site_health']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
		// bbpress
		unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);
		// yoast seo
		unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);
		// gravity forms
		unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);
	}
	/**
	 * [add_archive_pages_to_wplink adds post type archive links to the wplink dialog]
	 * @param array $results the results from the search
	 * @param array $query   the wp_query object
	 * @return array the filtered results
	 */
	public static function add_archive_pages_to_wplink($results, $query){
		$arr = array(
			array(
				'id' => 0,
				'title' => 'Lesson Plans',
				'permalink' => get_post_type_archive_link( 'lessonplans' ),
				'info' => 'Archive',
			),
		);
		return array_merge($results, $arr);
	}
	public static function remove_menu_clutter(){
		global $submenu;
		// remove customizer
		if ( isset( $submenu[ 'themes.php' ] ) ) {
			foreach ( $submenu[ 'themes.php' ] as $index => $menu_item ) {
				if ( in_array( 'customize', $menu_item ) ) {
					unset( $submenu[ 'themes.php' ][ $index ] );
				}
			}
		}
	}
	public static function hide_customize_button(){
	    ?>
	    <style type="text/css">.theme-actions .button.hide-if-no-customize {display:none;} </style>
	    <?php
	}
	private static function clean_head(){
		// removes generator tag
		remove_action( 'wp_head' , 'wp_generator' );
		// removes dns pre-fetch
		remove_action( 'wp_head', 'wp_resource_hints', 2 );
		// removes weblog client link
		remove_action( 'wp_head', 'rsd_link' );
		// removes windows live writer manifest link
		remove_action( 'wp_head', 'wlwmanifest_link');	
	}
	private static function enqueue_javascript(){
		wp_enqueue_script( 'theme' );
	}
	private static function enqueue_styles(){
		wp_enqueue_style( 'theme', '', array(), filemtime(get_template_directory() . '/build/css/build.css') );
	}

	private static function register_javascript(){
		wp_register_script( 'theme', get_template_directory_uri() . '/build/js/build.js' );
	}

	private static function register_styles(){
		wp_register_style( 'theme', get_template_directory_uri() . '/build/css/build.css', array(), filemtime(get_template_directory() . '/build/css/build.css') );
	}
}

SetupTheme::_init();

?>