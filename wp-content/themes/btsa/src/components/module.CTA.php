<?php
$id = MiscUtil::get_field('cta_module_id', $post_id, $loader_vars);
$title = MiscUtil::get_field('cta_module_title', $post_id, $loader_vars);
$content = MiscUtil::get_field('cta_module_content', $post_id, $loader_vars, false);
$cta = MiscUtil::get_field('cta_module_cta', $post_id, $loader_vars, false);
$bg_color = MiscUtil::get_field('cta_module_bg_color', $post_id, $loader_vars, false);
?>
<section class="cta"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; MiscUtil::maybe_render_inline_style(array('background-color' => $bg_color)); ?>>
	<div class="cta-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h2 class="cta-wrapper-title"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php if( !empty($content) ): ?>
			<div class="cta-wrapper-content">
				<?php echo apply_filters( 'the_content', do_shortcode($content) ); ?>
			</div>
		<?php endif; ?>
		<?php if( !empty($cta) ): ?>
			<a href="<?php echo $cta['url'] ?>" target="<?php echo $cta['target'] ?>" class="cta-wrapper-cta">
				<?php echo $cta['title']; ?>
			</a>
		<?php endif; ?>
	</div>
</section>