<?php
$id = MiscUtil::get_field('calendar_module_id', $post_id, $loader_vars);
$type = MiscUtil::get_field('calendar_module_type', $post_id, $loader_vars);
$cta = MiscUtil::get_field('calendar_module_cta', $post_id, $loader_vars);
?>
<section class="calendarmodule"<?php MiscUtil::maybe_echo_id($id); ?>>
	<div class="calendarmodule-wrapper section-wrapper">
		<?php echo do_shortcode( '[calendarize' . ( $type === 'week' ? ' defaultview="basicWeek"' : '' ) . ']' ); ?>
		<?php if( !empty($cta) ): ?>
			<a target="<?php echo $cta['target']; ?>" href="<?php echo $cta['url']; ?>" class="calendarmodule-wrapper-cta">
				<?php echo $cta['title']; ?>
			</a>
		<?php endif; ?>
	</div>
</section>