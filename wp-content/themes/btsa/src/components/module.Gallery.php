<?php
$id = MiscUtil::get_field('gallery_module_id', $post_id, $loader_vars);
$gallery = MiscUtil::get_field('gallery_module_gallery', $post_id, $loader_vars);
$type = MiscUtil::get_field('gallery_module_type', $post_id, $loader_vars);
$cta = MiscUtil::get_field('gallery_module_cta', $post_id, $loader_vars);
$seemore = MiscUtil::get_field('gallery_module_seemore', $post_id, $loader_vars);
?>
<section class="gallerymodule"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="gallerymodule-wrapper section-wrapper<?php echo count($gallery) < 5 ? ' nano' : ''; ?>">
		<?php
		if( !empty($gallery) ):
			foreach( $images = $type == 'small' ? array_splice($gallery, 0, ( !empty($cta['url']) || !empty($cta['title']) ? 8 : 9 ) ) : $gallery as $index => $image ): ?>
				<div class="gallerymodule-wrapper-item" style="background-image: url('<?php echo $image['url'] ?>');" data-src="<?php echo $image['url'] ?>'"></div>
		<?php endforeach;
			if( $type === 'small' && ( !empty($cta['url']) || !empty($cta['title']) ) ): ?>
				<a target="<?php echo $cta['target'] ?>" href="<?php echo $cta['url'] ?>" class="gallerymodule-wrapper-item cta"<?php MiscUtil::maybe_render_inline_style(array('background-color' => MiscUtil::get_color(rand(3, 7)))); ?>>
					<div class="gallerymodule-wrapper-item-text"><?php echo $cta['title']; ?></div>
					<?php if( $seemore ): ?>
						<div class="gallerymodule-wrapper-item-seemore">See More <img class="gallerymodule-wrapper-item-seemore-arrow" src="<?php echo get_template_directory_uri() ?>/lib/img/arrow-right.png"></div>
					<?php endif; ?>
				</a>
		<?php
			endif;
		endif;
		?>
	</div>
</section>