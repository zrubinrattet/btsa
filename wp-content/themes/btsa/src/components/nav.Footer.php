<section class="footernav">
	<div class="footernav-band">
		<div class="footernav-band-grayblock"></div>
		<div class="footernav-band-logocontainer">
			<?php
			global $post;
			if( $post->post_name == 'get-involved' ){
				PartialUtil::get('logo.GetInvolved', array(
					'bg_color' => MiscUtil::get_color('blue'),
				));
			}
			else{
				PartialUtil::get('logo.Main', array(
					'bg_color' => MiscUtil::get_color('blue'),
					'no_text' => true,
				));
			}
			?>
		</div>
		<div class="footernav-band-main">
			<div class="footernav-band-main-left">
				<h4 class="footernav-band-main-left-header">Subscribe to Our Newsletter</h4>
				<div class="footernav-band-main-left-description">Sign up to get important news and updates from BTSALP delivered to your inbox. Foster your students' learning with resources for workshops, events and projects.</div>
				<div class="footernav-band-main-left-signup">
					<a target="_blank" href="https://visitor.r20.constantcontact.com/d.jsp?llr=kjm8pcsab&p=oi&m=1118922715434&sit=eqwcnjfjb&f=0220cbd7-bffa-4c2f-a282-8a93131170dd" class="footernav-band-main-left-signup-link">Sign Up</a>
					<a href="<?php echo site_url() ?>/get-involved#contactus" class="footernav-band-main-left-signup-link">Contact Us</a>
				</div>
			</div>
			<div class="footernav-band-main-right">
				<ul class="footernav-band-main-right-social">
					<?php
						$social_icons = array(
							'facebook' => array(
								'img' => get_template_directory_uri() . '/lib/img/facebook.png',
								'href' => 'https://www.facebook.com/artworksforkids?fref=ts',
							),
							'twitter' => array(
								'img' => get_template_directory_uri() . '/lib/img/twitter.png',
								'href' => 'https://twitter.com/btsalp',
							),
							'youtube' => array(
								'img' => get_template_directory_uri() . '/lib/img/youtube.png',
								'href' => 'https://www.youtube.com/channel/UCjJT-2GNMBEUm722AsOA9uw',
							),
							'pinterest' => array(
								'img' => get_template_directory_uri() . '/lib/img/pinterest.png',
								'href' => 'https://www.pinterest.com/btsalp/',
							),
							'instagram' => array(
								'img' => get_template_directory_uri() . '/lib/img/instagram.png',
								'href' => 'http://instagram.com/btsalp',
							),
						);
						foreach( $social_icons as $alt => $social_icon ):
					?>
						<li class="footernav-band-main-right-social-item">
							<a target="_blank" href="<?php echo $social_icon['href']; ?>" class="footernav-band-main-right-social-item-link">
								<img src="<?php echo $social_icon['img'] ?>" class="footernav-band-main-right-social-item-link-icon" alt="<?php echo $alt; ?>">
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
	<div class="footernav-copyright">
		<div class="footernav-copyright-links">
			<a href="<?php echo site_url('terms-conditions') ?>" class="footernav-copyright-links-link">Terms &amp; Conditions</a>
			<a href="<?php echo site_url('privacy-policy') ?>" class="footernav-copyright-links-link">Privacy Policy</a>
		</div>
		<div class="footernav-copyright-text">
			&copy; <?php echo date('Y'); ?> beverley taylor sorenson arts <br/>learning program. all rights reserved.
		</div>
	</div>
</section>