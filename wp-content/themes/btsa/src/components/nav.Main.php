<nav class="mainnav">
	<div class="mainnav-wrapper">
		<div class="mainnav-wrapper-mobilebar">
			<button class="mainnav-wrapper-mobilebar-toggle hamburger hamburger--squeeze" type="button">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button>
		</div>
		<div class="mainnav-wrapper-menu">
			<ul class="mainnav-wrapper-menu-items">
				<?php
				// loop through the nav items
				foreach( $nav_items = wp_get_nav_menu_items( 'main-nav' ) as $parent_item ):
					// bail if it's not a parent item
					if( $parent_item->menu_item_parent != 0 ) continue;
					?>
					<li class="mainnav-wrapper-menu-items-item">
						<a href="<?php echo $parent_item->url; ?>" class="mainnav-wrapper-menu-items-item-link">
							<?php echo $parent_item->title; ?>
						</a>
						<?php if( in_array($parent_item->ID, array_column($nav_items, 'menu_item_parent')) !== false ): ?>
							<ul class="mainnav-wrapper-menu-items-item-subitems">
								<?php
								// get the child items
								$child_items = array_filter($nav_items, function($nav_item) use ($parent_item){
									if( $nav_item->menu_item_parent == $parent_item->ID ) return $nav_item;
								});
								// loop through the child items
								foreach( $child_items as $child_item ): ?>
									<li class="mainnav-wrapper-menu-items-item-subitems-item">
										<a href="<?php echo $child_item->url ?>" class="mainnav-wrapper-menu-items-item-subitems-item-link">
											<?php echo $child_item->title; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</nav>
<div class="mainnavbuffer"></div>