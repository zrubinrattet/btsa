<?php
	/**
	 * Setting up acf related stuff here
	 */
	class ACFHandler {
		/**
		 * [_init entry point]
		 */
		public static function _init(){
			// register the fields
			add_action('init', 'ACFHandler::register_fields', 50);

			// register options pages
			ACFHandler::build_options_pages();

			// add the color palette to the color picker
			add_action('acf/input/admin_footer', 'ACFHandler::add_palette_to_color_picker');
		}
		public static function build_options_pages(){
			acf_add_options_page(array(
				'page_title'    => 'Theme Settings',
				'menu_title'    => 'Theme Settings',
				'menu_slug'     => 'theme-settings',
				'capability'    => 'edit_posts',
				'redirect'      => false
			));

			acf_add_options_page(array(
				'page_title'    => 'Lesson Plans Settings',
				'menu_title'    => 'Lesson Plans Settings',
				'menu_slug'     => 'lesson-plans-settings',
				'capability'    => 'edit_posts',
				'redirect'      => false,
				'parent_slug'   => 'edit.php?post_type=lessonplans',
			));
		}
		/**
		 * [add_palette_to_color_picker adds the house canary color palette to the acf color picker field]
		 */
		public static function add_palette_to_color_picker(){
			?>
			<script type="text/javascript">
				acf.add_filter('color_picker_args', function( args, $field ){
					// add the hexadecimal codes here for the colors you want to appear as swatches
					args.palettes = <?php echo json_encode( array_values( MiscUtil::get_color() ) ); ?>;
					// return colors
					return args;
				});
			</script>
			<?php
		}
		/**
		 * [register_fields register the fields]
		 */
		public static function register_fields(){
			if( function_exists('acf_add_local_field_group') ){
				// register the module builder for the module builder template
				acf_add_local_field_group(array(
					'key' => 'group_default_template',
					'title' => 'Module Builder',
					'fields' => array(
						array(
							'key' => 'field_module_buidler',
							'label' => 'Module Builder',
							'name' => 'module_builder',
							'type' => 'flexible_content',
							'layouts' => Layout::get_layouts(),
							'button_label' => 'Add Module',
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'page_template',
								'operator' => '==',
								'value' => 'page-mb.php'
							),
						),
					),
				));

				// add the theme settings options page stuff
				acf_add_local_field_group(array(
					'key' => 'group_theme_settings',
					'title' => 'Theme Settings',
					'fields' => array(
						array(
							'key' => 'field_tile_images',
							'label' => 'Tile Images',
							'name' => 'tile_images',
							'type' => 'gallery',
							'instructions' => 'The images that appear in the decorative tiles throughout the site',
						),
						array(
							'key' => 'field_edu_board_members',
							'label' => 'Education Board Members',
							'name' => 'edu_board_members',
							'type' => 'repeater',
							'button_label' => 'Add New Education Board Member',
							'sub_fields' => array(
								array(
									'key' => 'field_edu_board_member_name',
									'label' => 'Name',
									'name' => 'edu_board_member_name',
									'type' => 'text',
								),
								array(
									'key' => 'field_edu_board_member_district',
									'label' => 'District',
									'name' => 'edu_board_member_district',
									'type' => 'number',
								),
								array(
									'key' => 'field_edu_board_member_email',
									'label' => 'Email',
									'name' => 'edu_board_member_email',
									'type' => 'email',
								),
								array(
									'key' => 'field_edu_board_member_image',
									'label' => 'Image',
									'name' => 'edu_board_member_image',
									'type' => 'image',
								),
							),
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'options_page',
								'operator' => '==',
								'value' => 'theme-settings',
							),
						),
					),
				));

				// add the lesson plans settings options page stuff
				acf_add_local_field_group(array(
					'key' => 'group_lesson_plans_settings',
					'title' => 'Lesson Plans Settings',
					'fields' => array(
						array(
							'key' => 'field_lesson_plans_heading_title',
							'label' => 'Heading Title',
							'name' => 'lesson_plans_heading_title',
							'type' => 'text',
							'instructions' => 'Renders just below the page title on the archive page',
						),
						array(
							'key' => 'field_lesson_plans_heading_subtitle',
							'label' => 'Heading Subtitle',
							'name' => 'lesson_plans_heading_subtitle',
							'type' => 'textarea',
							'instructions' => 'Renders just below the heading title on the archive page',
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'options_page',
								'operator' => '==',
								'value' => 'lesson-plans-settings',
							),
						),
					),
				));

				// add the home template stuff
				acf_add_local_field_group(array(
					'key' => 'group_home',
					'title' => 'Home Page Meta',
					'fields' => array(
						array(
							'key' => 'field_home_hero_image',
							'label' => 'Hero Image',
							'name' => 'home_hero_image',
							'type' => 'image',
						),
						array(
							'key' => 'field_home_hero_copy',
							'label' => 'Hero Copy',
							'name' => 'home_hero_copy',
							'type' => 'textarea',
						),
						array(
							'key' => 'field_home_modal_video',
							'label' => 'Modal Video',
							'name' => 'home_modal_video',
							'type' => 'oembed',
						),
						array(
							'key' => 'field_home_inpagenav_title1',
							'label' => 'In-Page Nav Title 1',
							'name' => 'home_inpagenav_title1',
							'type' => 'text',
							'wrapper' => array(
								'width' => 50
							),
						),
						array(
							'key' => 'field_home_inpagenav_title2',
							'label' => 'In-Page Nav Title 2',
							'name' => 'home_inpagenav_title2',
							'type' => 'text',
							'wrapper' => array(
								'width' => 50
							),
						),
						array(
							'key' => 'field_home_inpagenav_title3',
							'label' => 'In-Page Nav Title 3',
							'name' => 'home_inpagenav_title3',
							'type' => 'text',
							'wrapper' => array(
								'width' => 50
							),
						),
						array(
							'key' => 'field_home_inpagenav_title4',
							'label' => 'In-Page Nav Title 4',
							'name' => 'home_inpagenav_title4',
							'type' => 'text',
							'wrapper' => array(
								'width' => 50
							),
						),
						array(
							'key' => 'field_home_module_buidler',
							'label' => 'Module Builder',
							'name' => 'home_module_builder',
							'type' => 'flexible_content',
							'layouts' => Layout::get_layouts(),
							'button_label' => 'Add Module',
							'min' => 4,
							'max' => 4,
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'page',
								'operator' => '==',
								'value' => get_page_by_path( 'home', OBJECT, 'page' )->ID,
							),
						),
					),
				));

				// add newsletter attachment stuff
				acf_add_local_field_group(array(
					'key' => 'group_newsletters',
					'title' => 'Newsletters Meta',
					'fields' => array(
						array(
							'key' => 'field_newsletters_message',
							'label' => '<h1>This page expresses the newsletters and a pagination UI. To edit newsletters, go <a href="' . admin_url( 'upload.php?taxonomy=category&term=newsletter' ) . '">here</a>.</h1><h2 style="padding-left: 0px">Note: if a media object is a pdf and has the category "Newsletter" then it is a newsletter.</h2>',
							'name' => 'newsletters_message',
							'type' => 'message',
						),
						array(
							'key' => 'field_newsletters_footer',
							'label' => 'Footer text',
							'name' => 'newsletters_footer',
							'type' => 'wysiwyg',
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'post',
								'operator' => '==',
								'value' => get_page_by_path( 'news/newsletters', OBJECT, 'page' )->ID,
							),
						),
					),
				));

				// add case study stuff
				acf_add_local_field_group(array(
					'key' => 'group_casestudies',
					'title' => 'Case Studies Meta',
					'fields' => array(
						array(
							'key' => 'field_casestudy_stats',
							'label' => 'Stats',
							'name' => 'casestudy_stats',
							'type' => 'wysiwyg',
							'instructions' => 'Percentage numbers (63%, 5% to 35%) automatically have special style applied',
						),
						array(
							'key' => 'field_casestudy_copy',
							'label' => 'Copy',
							'name' => 'casestudy_copy',
							'type' => 'wysiwyg',
						),
						array(
							'key' => 'field_casestudy_school',
							'label' => 'School',
							'name' => 'casestudy_school',
							'type' => 'text',
						),
						array(
							'key' => 'field_casestudy_district',
							'label' => 'District',
							'name' => 'casestudy_district',
							'type' => 'text',
						),
						array(
							'key' => 'field_casestudy_authors',
							'label' => 'Authors',
							'name' => 'casestudy_authors',
							'type' => 'repeater',
							'button_label' => 'Add New Case Study Author',
							'sub_fields' => array(
								array(
									'key' => 'field_casestudy_author',
									'label' => 'Author Name',
									'name' => 'casestudy_author',
									'type' => 'text',
								),
							),
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'casestudy',
							),
						),
					),
				));

				// add newsletter attachment stuff
				acf_add_local_field_group(array(
					'key' => 'group_newsletter',
					'title' => 'Newsletter Meta',
					'fields' => array(
						array(
							'key' => 'field_newsletter_date',
							'label' => 'Date',
							'name' => 'newsletter_date',
							'type' => 'date_picker',
							'display_format' => 'm/d/Y',
							'return_format' => 'F j, Y',
							'first_day' => 0,
						),
						array(
							'key' => 'field_newsletter_issue',
							'label' => 'Issue Number',
							'name' => 'newsletter_issue',
							'type' => 'number',
						),
						array(
							'key' => 'field_newsletter_title',
							'label' => 'Title',
							'name' => 'newsletter_title',
							'type' => 'text',
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'attachment',
								'operator' => '==',
								'value' => 'application/pdf',
							),
							array(
								'param' => 'post_taxonomy',
								'operator' => '==',
								'value' => 'category:newsletter',
							),
						),
					),
				));

				// add the lessonplans stuff
				acf_add_local_field_group(array(
					'key' => 'group_lessonplans',
					'title' => 'Lesson Plan Meta',
					'fields' => array(
						array(
							'key' => 'field_lessonplan_year',
							'label' => 'Year',
							'name' => 'lessonplan_year',
							'type' => 'number',
						),
						array(
							'key' => 'field_lessonplan_overview',
							'label' => 'Overview',
							'name' => 'lessonplan_overview',
							'type' => 'wysiwyg',
						),
						array(
							'key' => 'field_lessonplan_sidebar',
							'label' => '<h1 style="font-weight: bold;">Sidebar</h1>',
							'name' => 'lessonplan_sidebar',
							'type' => 'message',
						),
						array(
							'key' => 'field_lessonplan_pdf',
							'label' => 'PDF',
							'name' => 'lessonplan_pdf',
							'type' => 'file',
						),
						array(
							'key' => 'field_lessonplan_infobox',
							'label' => 'Infobox',
							'name' => 'lessonplan_infobox',
							'type' => 'wysiwyg',
							'instructions' => 'Headings should be using the <a target="_blank" href="' . get_template_directory_uri() . '/lib/img/h3.png">Heading 3</a> format. Lists should be a bulleted list. List items can be links or contain images too. Do NOT nest lists.'
						),
						// array(
						// 	'key' => 'field_lessonplan_checklists',
						// 	'label' => 'Checklists',
						// 	'name' => 'lessonplan_checklists',
						// 	'type' => 'repeater',
						// 	'layout' => 'row',
						// 	'button_label' => 'Add New Checklist',
						// 	'sub_fields' => array(
						// 		array(
						// 			'key' => 'field_lessonplan_checklist_title',
						// 			'label' => 'Title',
						// 			'name' => 'lessonplan_checklist_title',
						// 			'type' => 'text',
						// 		),
						// 		array(
						// 			'key' => 'field_lessonplan_checklist_subtitle',
						// 			'label' => 'Subtitle',
						// 			'name' => 'lessonplan_checklist_subtitle',
						// 			'type' => 'text',
						// 		),
						// 		array(
						// 			'key' => 'field_lessonplan_checklist_items',
						// 			'label' => 'Checklist Items',
						// 			'name' => 'lessonplan_checklist_items',
						// 			'type' => 'repeater',
						// 			'button_label' => 'Add New Checklist Item',
						// 			'layout' => 'block',
						// 			'sub_fields' => array(
						// 				array(
						// 					'key' => 'field_lessonplan_checklist_item',
						// 					'label' => 'Checklist Item',
						// 					'name' => 'lessonplan_checklist_item',
						// 					'type' => 'wysiwyg',
						// 				),
						// 			),
						// 		),
						// 	),
						// ),
						array(
							'key' => 'field_lessonplan_standards_objectives',
							'label' => '<h1 style="font-weight: bold;">Standards and Objectives</h1>',
							'name' => 'lessonplan_standards_objectives',
							'type' => 'message',
						),
						array(
							'key' => 'field_lessonplan_fine_art_standards',
							'label' => 'Fine Art Standards',
							'name' => 'lessonplan_fine_art_standards',
							'type' => 'wysiwyg',
						),
						array(
							'key' => 'field_lessonplan_integrated_standards',
							'label' => 'Integrated Standards',
							'name' => 'lessonplan_integrated_standards',
							'type' => 'wysiwyg',
						),
						array(
							'key' => 'field_lessonplan_objectives',
							'label' => 'Objectives',
							'name' => 'lessonplan_objectives',
							'type' => 'wysiwyg',
						),
						array(
							'key' => 'field_lessonplan_teaching_timeline',
							'label' => '<h1 style="font-weight: bold;">Teaching and Timeline</h1>',
							'name' => 'lessonplan_teaching_timeline',
							'type' => 'message',
						),
						array(
							'key' => 'field_lessonplan_intro',
							'label' => 'Introduction',
							'name' => 'lessonplan_intro',
							'type' => 'wysiwyg',
						),
						array(
							'key' => 'field_lessonplan_demo',
							'label' => 'Demonstration',
							'name' => 'lessonplan_demo',
							'type' => 'wysiwyg',
						),
						array(
							'key' => 'field_lessonplan_work_period',
							'label' => 'Work Period',
							'name' => 'lessonplan_work_period',
							'type' => 'wysiwyg',
						),
						array(
							'key' => 'field_lessonplan_closure',
							'label' => 'Closure/Summary',
							'name' => 'lessonplan_closure',
							'type' => 'wysiwyg',
						),
						array(
							'key' => 'field_lessonplan_footer_tabs',
							'label' => 'Footer Tabs',
							'name' => 'lessonplan_footer_tabs',
							'type' => 'repeater',
							'button_label' => 'Add New Footer Tab',
							'layout' => 'row',
							'collapsed' => 'field_lessonplan_footer_tab_title',
							'sub_fields' => array(
								array(
									'key' => 'field_lessonplan_footer_tab_title',
									'label' => 'Title',
									'name' => 'lessonplan_footer_tab_title',
									'type' => 'text',
								),
								array(
									'key' => 'field_lessonplan_footer_tab_content',
									'label' => 'Content',
									'name' => 'lessonplan_footer_tab_content',
									'type' => 'wysiwyg',
								),
							),
						),
						array(
							'key' => 'field_lessonplan_misc_images',
							'label' => 'Misc. Images',
							'name' => 'lessonplan_misc_images',
							'type' => 'gallery',
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'lessonplans',
							),
						),
					),
				));
			}
		}
	}

	ACFHandler::_init();
?>