<?php

class AjaxHandler{
	public static function _init(){
		// archive feed stuff
		add_action( 'wp_ajax_archive_feed', 'AjaxHandler::archive_feed' );
		add_action( 'wp_ajax_nopriv_archive_feed', 'AjaxHandler::archive_feed' );

		// email legislator stuff
		add_action( 'wp_ajax_email_legislator', 'AjaxHandler::email_legislator' );
		add_action( 'wp_ajax_nopriv_email_legislator', 'AjaxHandler::email_legislator' );
	}
	public static function archive_feed(){
		$post_type = $_POST['post_type'];
		$terms = $_POST['terms'];
		$offset = $_POST['offset'];
		$length = 10;

		$posts = get_posts(array(
			'post_type' => $post_type,
			'tax_query' => MiscUtil::build_tax_query_from_terms($terms),
			'posts_per_page' => -1,
		));


		$response = array(
			'posts' => (function($posts, $offset, $length){
				$return = array();
				foreach( array_slice($posts, $offset, $length) as $post ){
					$return[] = PostHandler::get_feed_post($post);
				}
				return $return;
			})($posts, $offset, $length),
			'remaining' => max( count( array_slice($posts, $offset) ) - $length, 0),
		);

		echo json_encode($response);

		wp_die();
	}
	public static function email_legislator(){
		if( wp_verify_nonce( $_POST['nonce'], 'emaillegislator' ) ){
			// init headers
			$headers = array();
			// so recipient can t reply to sender
			$headers[] = 'From: ' . $_POST['fromName'] . ' <' . $_POST['fromEmail'] . '>';
			// for html email
			$headers[] = 'Content-Type: text/html; charset=UTF-8';
			// need to remove this line when live
			// $headers[] = 'Cc: zach@modernfuture.net <zach@modernfuture.net>';
			// need to switch the to to $_POST['to'] when live
			echo json_encode(wp_mail( $_POST['to'], 'Beverley Taylor Sorenson Arts Learning Program For Kids', nl2br($_POST['message']), $headers ));
		}
		else{
			// maybe block ip?
			echo json_encode(false);
		}
		wp_die();
	}
}

AjaxHandler::_init();

?>