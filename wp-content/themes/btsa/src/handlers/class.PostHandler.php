<?php

class PostHandler{
	public static function get_feed_post($post){
		ob_start();
		?>
			<a href="<?php the_permalink($post->ID); ?>" class="feedmodule-wrapper-posts-post">
				<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0]; ?>" class="feedmodule-wrapper-posts-post-image">
				<div class="feedmodule-wrapper-posts-post-text">
					<h3 class="feedmodule-wrapper-posts-post-text-title"><?php echo $post->post_title; ?></h3>
					<div class="feedmodule-wrapper-posts-post-text-description">
						<?php
							if( !empty($post->post_excerpt) ){
								echo $post->post_excerpt;
							}
							else{
								echo wp_trim_words( wp_strip_all_tags( $post->post_content ) );
							}
						?>
					</div>
				</div>
			</a>
		<?php
		return ob_get_clean();
	}
}

?>