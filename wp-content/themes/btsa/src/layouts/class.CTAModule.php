<?php 
	
	class CTAModule extends Layout{
		protected $layout = array(
			'key' => 'field_cta_module',
			'name' => 'cta_module',
			'label' => '<strong>CTA</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_cta_module_id',
					'label' => 'ID',
					'name' => 'cta_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_cta_module_bg_color',
					'label' => 'Background Color',
					'name' => 'cta_module_bg_color',
					'type' => 'color_picker',
				),
				array(
					'key' => 'field_cta_module_title',
					'label' => 'Title',
					'name' => 'cta_module_title',
					'type' => 'text',
				),
				array(
					'key' => 'field_cta_module_content',
					'label' => 'Content',
					'name' => 'cta_module_content',
					'type' => 'wysiwyg',
				),
				array(
					'key' => 'field_cta_module_cta',
					'label' => 'CTA',
					'name' => 'cta_module_cta',
					'type' => 'link',
				),
			)
		);
	}

?>