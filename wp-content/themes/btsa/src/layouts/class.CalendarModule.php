<?php 
	
	class CalendarModule extends Layout{
		protected $layout = array(
			'key' => 'field_calendar_module',
			'name' => 'calendar_module',
			'label' => '<strong>Calendar</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_calendar_module_id',
					'label' => 'ID',
					'name' => 'calendar_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_calendar_module_type',
					'label' => 'Type',
					'name' => 'calendar_module_type',
					'type' => 'radio',
					'choices' => array(
						'week' => 'Week',
						'month' => 'Month',
					),
				),
				array(
					'key' => 'field_calendar_module_cta',
					'label' => 'CTA',
					'name' => 'calendar_module_cta',
					'type' => 'link',
				),
			)
		);
	}

?>