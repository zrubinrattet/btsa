<?php 
	
	class CaseStudyList extends Layout{
		public function __construct(){
			$this->layout = array(
				'key' => 'field_casestudy_list',
				'name' => 'casestudy_list',
				'label' => '<strong>Case Study List</strong>',
				'display' => 'block',
				'sub_fields' => array(
					array(
						'key' => 'field_casestudy_list_id',
						'label' => 'ID',
						'name' => 'casestudy_list_id',
						'type' => 'text',
						'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
					),
					array(
						'key' => 'field_casestudy_list_message',
						'label' => '<h1>Lists off all case studies. They can be edited <a href="' . admin_url('edit.php?post_type=casestudy') . '">here</a>.</h1>',
						'name' => 'casestudy_list_message',
						'type' => 'message',
					),
				)
			);
		}
	}

?>