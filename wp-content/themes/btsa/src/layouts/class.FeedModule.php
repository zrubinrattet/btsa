<?php 
	
	class FeedModule extends Layout{
		public function __construct(){
			$this->layout = array(
				'key' => 'field_feed_module',
				'name' => 'feed_module',
				'label' => '<strong>Feed</strong>',
				'display' => 'block',
				'sub_fields' => array(
					array(
						'key' => 'field_feed_module_id',
						'label' => 'ID',
						'name' => 'feed_module_id',
						'type' => 'text',
						'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
					),
					array(
						'key' => 'field_feed_module_title',
						'label' => 'Title',
						'name' => 'feed_module_title',
						'type' => 'text',
					),
					array(
						'key' => 'field_feed_module_post_type',
						'label' => 'Post Type',
						'name' => 'feed_module_post_type',
						'type' => 'select',
						'instructions' => 'Only posts with selected post type will surface.',
						'choices' => array(
							'lessonplans' => 'Lesson Plans',
							'post' => 'Blog Posts',
						),
						'ui' => 1,
					),
					array(
						'key' => 'field_feed_module_post_terms',
						'label' => 'Terms',
						'name' => 'feed_module_post_terms',
						'type' => 'select',
						'instructions' => 'Only posts with selected terms will surface. If left blank posts from all terms will surface. Multiple terms will be clumped into an AND clause (not an OR).',
						'ui' => 1,
						'multiple' => 1,
						'choices' => (function(){
							$return = array();

							foreach( get_terms() as $term ){
								$return[$term->term_id] = $term->taxonomy . ' - ' . $term->name;
							}

							return $return;
						})(),
					),
					array(
						'key' => 'field_feed_module_max_posts',
						'label' => 'Max Posts',
						'name' => 'feed_module_max_posts',
						'type' => 'number',
						'instructions' => 'Use -1 if you want all posts to show. If blank this will default to 5',
					),
					array(
						'key' => 'field_feed_module_cta',
						'label' => 'CTA Button',
						'name' => 'feed_module_cta',
						'type' => 'link',
					),
				)
			);
		}
	}

?>