<?php 
	
	class GalleryModule extends Layout{
		protected $layout = array(
			'key' => 'field_gallery_module',
			'name' => 'gallery_module',
			'label' => '<strong>Gallery</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_gallery_module_id',
					'label' => 'ID',
					'name' => 'gallery_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_gallery_module_type',
					'label' => 'Type',
					'name' => 'gallery_module_type',
					'type' => 'radio',
					'choices' => array(
						'small' => 'Small',
						'all' => 'All',
					),
					'instructions' => 'Small shows up to 9 and has a custom cta option (shows up to 8 with cta). All shows all images.',
				),
				array(
					'key' => 'field_gallery_module_cta',
					'label' => 'CTA',
					'name' => 'gallery_module_cta',
					'type' => 'link',
					'instructions' => 'If both url and text are empty cta won\'t be used.',
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_gallery_module_type',
								'operator' => '==',
								'value' => 'small',
							)
						),
					),
					'wrapper' => array(
						'width' => 50,
					),
				),
				array(
					'key' => 'field_gallery_module_seemore',
					'label' => 'Show See More?',
					'name' => 'gallery_module_seemore',
					'type' => 'true_false',
					'ui' => 1,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_gallery_module_type',
								'operator' => '==',
								'value' => 'small',
							)
						),
					),
					'wrapper' => array(
						'width' => 50,
					),
				),
				array(
					'key' => 'field_gallery_module_gallery',
					'label' => 'Gallery',
					'name' => 'gallery_module_gallery',
					'type' => 'gallery',
				),
			)
		);
	}

?>