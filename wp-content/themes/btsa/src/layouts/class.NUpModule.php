<?php 
	
	class NUpModule extends Layout{
		public function __construct(){
			$this->layout = array(
				'key' => 'field_nup_module',
				'name' => 'nup_module',
				'label' => '<strong>N Up</strong>',
				'display' => 'block',
				'sub_fields' => array(
					array(
						'key' => 'field_nup_module_id',
						'label' => 'ID',
						'name' => 'nup_module_id',
						'type' => 'text',
						'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
					),
					array(
						'key' => 'field_nup_module_title',
						'label' => 'Title',
						'name' => 'nup_module_title',
						'type' => 'text',
					),
					array(
						'key' => 'field_nup_module_max_column_count',
						'label' => 'Max Column Count',
						'name' => 'nup_module_max_column_count',
						'type' => 'number',
						'instructions' => 'Max number of columns on desktop',
						'wrapper' => array(
							'width' => 50,
						),
					),
					array(
						'key' => 'field_nup_module_cta',
						'label' => 'CTA',
						'name' => 'nup_module_cta',
						'type' => 'link',
						'instructions' => 'If empty no CTA will render.',
						'wrapper' => array(
							'width' => 50,
						),
					),
					array(
						'key' => 'field_nup_module_full_width',
						'label' => 'Full Width?',
						'name' => 'nup_module_full_width',
						'type' => 'true_false',
						'ui' => 1,
						'wrapper' => array(
							'width' => 50,
						),
					),
					array(
						'key' => 'field_nup_module_hover_mode',
						'label' => 'Hover Mode?',
						'name' => 'nup_module_hover_mode',
						'type' => 'true_false',
						'ui' => 1,
						'wrapper' => array(
							'width' => 50,
						),
						'instructions' => 'When hover mode is enabled this module will behave like <a target="_blank" href="' . get_template_directory_uri() . '/lib/img/hovermode.png' . '">this</a>. Background Type is ignored, text & background color are used for the hover state and the background image is used for the normal state.',
					),
					array(
						'key' => 'field_nup_module_items',
						'label' => 'Items',
						'name' => 'nup_module_items',
						'type' => 'repeater',
						'button_label' => 'Add New Item',
						'layout' => 'block',
						'sub_fields' => array(
							array(
								'key' => 'field_nup_module_item_link',
								'label' => 'Link',
								'name' => 'nup_module_item_link',
								'type' => 'link',
								'wrapper' => array(
									'width' => 50,
								),
								'instructions' => 'If left blank no link shows. Only the URL & "open link in new tab" are used.'
							),
							array(
								'key' => 'field_nup_module_item_bg_type',
								'label' => 'Background Type',
								'name' => 'nup_module_item_bg_type',
								'type' => 'radio',
								'choices' => array(
									'image' => 'Image',
									'color' => 'Color',
								),
								'wrapper' => array(
									'width' => 50,
								),
							),
							array(
								'key' => 'field_nup_module_item_bg_color',
								'label' => 'Background Color',
								'name' => 'nup_module_item_bg_color',
								'type' => 'color_picker',
								'wrapper' => array(
									'width' => 50,
								),
							),
							array(
								'key' => 'field_nup_module_item_bg_image',
								'label' => 'Background Image',
								'name' => 'nup_module_item_bg_image',
								'type' => 'image',
								'wrapper' => array(
									'width' => 50,
								),
							),
							array(
								'key' => 'field_nup_module_item_bg_image_size',
								'label' => 'Background Size',
								'name' => 'nup_module_item_bg_image_size',
								'type' => 'radio',
								'instructions' => 'Changes how the background image fits in it\'s container. See <a target="_blank" href="' . get_template_directory_uri() . '/lib/img/cover-contain.jpg">this</a> for visual guide.',
								'choices' => array(
									'cover' => 'Cover',
									'contain' => 'Contain',
								),
							),
							array(
								'key' => 'field_nup_module_item_text',
								'label' => 'Text',
								'name' => 'nup_module_item_text',
								'type' => 'wysiwyg',
							),
							array(
								'key' => 'field_nup_module_item_no_filter',
								'label' => 'Disable the_content Filter?',
								'name' => 'nup_module_item_no_filter',
								'type' => 'true_false',
								'ui' => 1,
								'instructions' => 'This filter prettifies/standardizes output from the wysiwyg field. Turning it off is sometimes useful to display html content more literally, like when using shortcodes for instance. See <a href="https://developer.wordpress.org/reference/hooks/the_content/">here</a> for more details.',
							),
						),
					),
				)
			);	
		}
		
	}

?>