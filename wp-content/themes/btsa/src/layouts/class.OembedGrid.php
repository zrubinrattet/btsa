<?php 
	
	class OembedGrid extends Layout{
		protected $layout = array(
			'key' => 'field_oembed_grid',
			'name' => 'oembed_grid',
			'label' => '<strong>Oembed Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_oembed_grid_id',
					'label' => 'ID',
					'name' => 'oembed_grid_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_oembed_grid_title',
					'label' => 'Title',
					'name' => 'oembed_grid_title',
					'type' => 'text',
				),
				array(
					'key' => 'field_oembed_grid_items',
					'label' => 'Items',
					'name' => 'oembed_grid_items',
					'type' => 'repeater',
					'button_label' => 'Add Oembed Grid Item',
					'layout' => 'row',
					'sub_fields' => array(
						array(
							'key' => 'field_oembed_grid_item_title',
							'label' => 'Title',
							'name' => 'oembed_grid_item_title',
							'type' => 'text',
						),
						array(
							'key' => 'field_oembed_grid_item_desc',
							'label' => 'Description',
							'name' => 'oembed_grid_item_desc',
							'type' => 'textarea',
						),
						array(
							'key' => 'field_oembed_grid_item_oembed',
							'label' => 'Oembed',
							'name' => 'oembed_grid_item_oembed',
							'type' => 'oembed',
						),
					),
				),
			)
		);
	}

?>