<?php 
	
	class WYSIWYG extends Layout{
		protected $layout = array(
			'key' => 'field_wysiwyg_module',
			'name' => 'wysiwyg_module',
			'label' => '<strong>WYSIWYG</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_wysiwyg_module_id',
					'label' => 'ID',
					'name' => 'wysiwyg_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_wysiwyg_module_content',
					'label' => 'Content',
					'name' => 'wysiwyg_module_content',
					'type' => 'wysiwyg',
				),
				array(
					'key' => 'field_wysiwyg_module_disable_filter',
					'label' => 'Disable the_content filter',
					'name' => 'wysiwyg_module_disable_filter',
					'type' => 'true_false',
					'ui' => 1,
					'instructions' => 'This filter prettifies/standardizes output from the wysiwyg field. Turning it off is sometimes useful to display html content more literally, like when using shortcodes for instance. See <a href="https://developer.wordpress.org/reference/hooks/the_content/">here</a> for more details.',
				),
			)
		);
	}

?>