For registering field layouts for the ACF flexible content field that is the module builder's backend GUI.

Examples of each with loader vars:

## Artwork Grid ##
```
PartialUtil::get('grid.Artwork', array(
    'whitecard_style' => true,
    'from_fc' => false,
    // args passed to WP_Query instance
    'query_vars' => array(
        'post__not_in' => array($post->ID),
        'posts_per_page' => 4,
        'post_type' => 'artwork',
        'post_status' => 'publish',
        'meta_query' => array(
            array(
                'key' => 'artwork_artist_relationships',
                'value' => $artists[0]->ID,
                'compare' => 'LIKE',
            )
        )
    ),
));
```

## Series Grid ##
```
PartialUtil::get('grid.SeriesTerm', array(
    'list_override' => false,
    'from_fc' => false,
    'term_id' => array($term->term_id),
));
```

## Navigation Block ##
```
ComponentUtil::get('block.Navigation', array(
    'from_fc' => false,
    'sub_fields' => array(
        'title' => 'Bombs Away',
        'description' => 'Silver bowl reflection sparkle',
        'links' => array(
            array(
                'image' => 333475,
                'supertitle' => 'So what if',
                'title' => 'Not doo beeein',
                'description' => 'Blah boo deeedep',
                'link' => array(
                    'title' => '',
                    'url' => get_permalink(get_page_by_path( 'about' )->ID),
                    'target' => ''
                ),
            ),
        ),
    )
));
```