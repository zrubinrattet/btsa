<?php
	function swap_images_for_existing_ones($content){
		$dom = new DOMDocument();
		$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
		$images = $dom->getElementsByTagName('img');
		if( !empty($images) ){
			foreach( $images as $image ){
				$attachment_id = get_attachment_id_by_filepath($image->getAttribute('src'));
				$image->setAttribute('src', wp_get_attachment_image_src( $attachment_id, 'full' )[0]);
			}
		}
		$content = $dom->saveHTML();
		return $content;
	}

	function get_attachment_id_by_filepath($url){
		global $wpdb;
		// get the filepath
		$filepath = explode('/uploads/', $url)[1];
		// query for the attachment id
		$query = 'select post_id from wp_postmeta
						where
							post_id in (
								select ID from wp_posts
									where
										post_type = "attachment"
							)
						and
						meta_key = "_wp_attached_file"
						and
						meta_value = %s';
		$attachment_id = @$wpdb->get_results( $wpdb->prepare( $query, $filepath ) )[0]->post_id;

		return $attachment_id;
	}
	
	// relate this image to the misc image items
	function relate_attachment($url, $lessonplan, $key, $gallery = true){

		$attachment_id = get_attachment_id_by_filepath($url);

		// if an attachment_id was found
		if( !empty($attachment_id) ){
			// get the existing images
			$misc_images = get_field( $key, $lessonplan->ID );

			// if it's a gallery
			if( $gallery ){
				// make it an array if there's nothing
				if( empty($misc_images) ) $misc_images = array();
				// add the new attachment
				$misc_images[] = $attachment_id;
				// dedupe
				array_values($misc_images);
				// update the field
				update_field($key, $misc_images, $lessonplan->ID);
			}
			else{
				update_field($key, $attachment_id, $lessonplan->ID);
			}
		}
	}

	// load WP
	include '../../../../../wp-load.php';

	// get the published lessonplans
	$lessonplans = get_posts(array(
		'post_type' => 'lessonplans',
		'posts_per_page' => -1,
		'post_status' => 'publish',
		// 'post__in' => array(3762),
	));

	// loop through the lessonplans
	foreach( $lessonplans as $index => $lessonplan ){
		// provide some feedback
		error_log( 'At lessonplan id ' . strval($lessonplan->ID) . ' (' . strval($index + 1) . '/' . strval(count($lessonplans)) . ')' );
		// get the muffin builder data
		$muffin_data = get_post_meta( $lessonplan->ID, 'mfn-page-items', true )[0];
		// loop through the "wraps"
		foreach( $muffin_data['wraps'] as $muffin_wrap ){
			// loop through the items
			foreach( $muffin_wrap['items'] as $muffin_item ){
				// do things based on the type of item
				switch ($muffin_item['type']) {
					case 'image':
						relate_attachment($muffin_item['fields']['src'], $lessonplan, 'lessonplan_misc_images');
						break;
					case 'column':
						// get the overview & year
						if( strtolower($muffin_item['fields']['title']) == 'info' ){
							// get the overview
							$explosion = explode('</b>', $muffin_item['fields']['content']);
							error_log(var_export($muffin_item, true));
							$overview = $explosion[count($explosion) - 1];
							if( !empty($overview) ){
								update_field('lessonplan_overview', $overview, $lessonplan->ID);
							}

							// get the year
							$explosion = explode('<br />', $muffin_item['fields']['content']);
							foreach( $explosion as $item ){
								if( strpos($item, 'Year:') !== false ){
									$year = trim(wp_strip_all_tags( str_replace('Year: ', '', $item) ));
								}
							}
							if( isset($year) && !empty($year) ){
								update_field('lessonplan_year', $year, $lessonplan->ID);
							}
						}
						// get the introduction
						if( strpos($muffin_item['fields']['content'], '<h3>INTRODUCTION</h3>') !== false ){
							update_field('lessonplan_intro', swap_images_for_existing_ones( str_replace('<h3>INTRODUCTION</h3>', '', $muffin_item['fields']['content']) ), $lessonplan->ID);
						}
						// get the domonstration & work period
						if( strpos($muffin_item['fields']['content'], '<h3>DEMONSTRATION & WORK PERIOD</h3>') !== false ){
							update_field('lessonplan_demo', swap_images_for_existing_ones( str_replace('<h3>DEMONSTRATION & WORK PERIOD</h3>', '', $muffin_item['fields']['content']) ), $lessonplan->ID);
						}

						// get the work period
						if( strpos($muffin_item['fields']['content'], '<h3>WORK PERIOD</h3>') !== false ){
							update_field('lessonplan_work_period', swap_images_for_existing_ones( str_replace('<h3>WORK PERIOD</h3>', '', $muffin_item['fields']['content']) ), $lessonplan->ID);
						}

						// get the CLOSURE/SUMMARY
						if( strpos($muffin_item['fields']['content'], '<h3>CLOSURE/SUMMARY</h3>') !== false ){
							update_field('lessonplan_closure', swap_images_for_existing_ones( str_replace('<h3>CLOSURE/SUMMARY</h3>', '', $muffin_item['fields']['content']) ), $lessonplan->ID);
						}

						break;
					case 'tabs':
						// get the standards & objectives stuff
						if( strtolower($muffin_item['fields']['title']) === 'standards and objectives' ){
							foreach( $muffin_item['fields']['tabs'] as $tab ){
								if( strtolower($tab['title']) === 'fine art standards' ){
									update_field('lessonplan_fine_art_standards', swap_images_for_existing_ones($tab['content']), $lessonplan->ID);
								}
								elseif( strtolower($tab['title']) === 'integrated standards' ){
									update_field('lessonplan_integrated_standards', swap_images_for_existing_ones($tab['content']), $lessonplan->ID);
								}
								elseif( strtolower($tab['title']) === 'objectives' ){
									update_field('lessonplan_objectives', swap_images_for_existing_ones($tab['content']), $lessonplan->ID);
								}
							}
						}
						// get the footer tabs
						if( $muffin_item['fields']['type'] === 'vertical' ){
							// init new tabs
							$new_tabs = array();
							// loop through the tabs
							foreach( $muffin_item['fields']['tabs'] as $tab ){
								$new_tabs[] = array(
									'lessonplan_footer_tab_title' => $tab['title'],
									'lessonplan_footer_tab_content' => swap_images_for_existing_ones( $tab['content'] ),
								);
							}
							// update the field
							update_field('lessonplan_footer_tabs', $new_tabs, $lessonplan->ID);
						}
						break;
					case 'list':
						if( $muffin_item['fields']['title'] === 'Download PDF Lesson Plan' ){
							relate_attachment($muffin_item['fields']['link'], $lessonplan, 'lessonplan_pdf', false);
						}
						break;
					
					case 'info_box':
						$content = swap_images_for_existing_ones( $muffin_item['fields']['content'] );
						error_log($content);
						update_field('lessonplan_infobox', '<h3>' . $muffin_item['fields']['title'] . '</h3>' . $content, $lessonplan->ID);
						break;
				}
			}
		}
	}
?>