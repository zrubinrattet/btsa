<?php

function get_attachment_id_by_filepath($url){
	global $wpdb;
	// get the filepath
	$filepath = explode('/uploads/', $url)[1];
	// query for the attachment id
	$query = 'select post_id from wp_postmeta
					where
						post_id in (
							select ID from wp_posts
								where
									post_type = "attachment"
						)
					and
					meta_key = "_wp_attached_file"
					and
					meta_value = %s';
	$attachment_id = @$wpdb->get_results( $wpdb->prepare( $query, $filepath ) )[0]->post_id;

	return $attachment_id;
}

$old_html = '<a href="http://btsalp.com/wp-content/uploads/2018/05/52-End-of-year-celebrations.pdf">#52 May 2018 End of Year Celebrations</a>
<a href="http://btsalp.com/wp-content/uploads/2018/04/51-Weber-State-region-7-different-projects.pdf">#51 April 2018 Weber State University Region - 7 Projects</a>
<a href="http://btsalp.com/wp-content/uploads/2018/03/50-Totem-Poles-and-Mandals-Paige-Hernandez-101-Dalmatians.pdf">#50 March 2018 Totem Poles &amp; Mandalas, Paige Henandez and 101 Dalmatians</a>
<a href="http://btsalp.com/wp-content/uploads/2018/02/49-Day-on-the-Hill-Hamilton-and-Olympic-sculpture.pdf">#49 February 2018 Day on the Hill 2018, Hamilton the Musical Play &amp; Olympic Sculptures</a>
<a href="http://btsalp.com/wp-content/uploads/2018/01/48-Legislative-Luncheon-2018.pdf">#48 January 2018 BTSALP Legislative Luncheon 2018</a>
<a href="http://btsalp.com/wp-content/uploads/2017/12/47-Fused-Glass-Utah-State.pdf">#47 December 2017 Cache County Glass Project</a>
<a href="http://btsalp.com/wp-content/uploads/2017/12/46-Collaboration.pdf">#46 November 2017 Solar System Collaboration &amp; Art Works for Kids Award</a>
<a href="http://btsalp.com/wp-content/uploads/2017/10/45-Bruce-Hucko.pdf">#45 October 2017 Spotlight on Bruce Hucko &amp; Plein Air Art in Arches National Park</a>
<a href="http://btsalp.com/wp-content/uploads/2017/09/44-Back-to-School-with-BTSALP.pdf">#44 September 2017 Back to School with BTSALP</a>
<a href="http://btsalp.com/wp-content/uploads/2017/05/43-Celebrating-Music-Dance-and-Visual-Art.pdf">#43 May 2017 Celebrating Music, Dance &amp; Visual Art</a>
<a href="http://btsalp.com/wp-content/uploads/2017/04/42-Drama-Jungle-Book-Kids-Suessical-Jr.-Opera-By-Children1.pdf">#42 April 2017 Drama - Jungle Book Kids, Seussical Jr and Opera By Children</a>
<a href="http://btsalp.com/wp-content/uploads/2017/03/41-Spotlight-Kate-Watson-Color-Harmonies-Color-Theory-germs-mural-Science.pdf">#41 March 2017 Spotlight on Kate Watson, Color Harmonies, Color Theory, Germs, Mural and Science</a>
<a href="http://btsalp.com/wp-content/uploads/2017/02/40-Legislative-activity-2017.pdf">#40 Febuary 2017  Arts Day on the Hill &amp; The Legislative Luncheon</a>
<a href="http://btsalp.com/wp-content/uploads/2017/01/39-STEAM-Wind-Turbines-and-Bouncy-Balls.pdf">#39 January 2017  STEAM, Wind Turbines &amp; Bouncy Balls</a>
<a href="http://btsalp.com/wp-content/uploads/2016/12/38-Cassie-Walker-Lone-Peak.pdf">#38 December 8, 2016 Cassie Walker, Visual Art, Origami Snowflakes, Lone Peak Elementary</a>
<a href="http://btsalp.com/wp-content/uploads/2016/11/37-Jim-Solomon-Dance-and-Drama.pdf">#37 November 15, 2016 Jim Solomon, Dance &amp; Drama</a>
<a href="http://btsalp.com/wp-content/uploads/2016/10/36-Spotlight-on-Westminster-region.pdf">#36 October 13, 2016 Spotlight Westmister RegionRebecca Penerosa, Mariah King, Melissa Gonzalez, Madison Lindgren, Kathleen Briley, <i>Many Luscious Lollipops,</i> Henri Rosseau</a>
<a href="http://btsalp.com/wp-content/uploads/2016/08/35-Back-to-School-2016.pdf">#35 September 8, 2016  Back to School</a>
<a href="http://btsalp.com/wp-content/uploads/2016/05/34-Celebrate-the-Arts.pdf">#34 May 26, 2016 Celebrate the Arts</a>
<a href="http://btsalp.com/wp-content/uploads/2016/05/33-Eric-Carle-and-Art-Night.pdf">#33 May 11, 2016  Eric Carle &amp; Art Night</a>
<a href="http://btsalp.com/wp-content/uploads/2016/04/32-ArtsLINK-2016.pdf">#32 April 28, 2016  ArtsLINK 2016, Penny Caywood, Rebecca Penerosa, Paul Heath &amp; Jana Shumway</a>
<a href="http://btsalp.com/wp-content/uploads/2016/04/31-BTS-newsletter-Bruce-Hucko.pdf">#31 April 14, 2016  Drama &amp; Bruce Hucko</a>
<a href="http://btsalp.com/wp-content/uploads/2016/03/30-Draper-Elementary-Integration.pdf">#30 March 24, 2016  Draper Elementary &amp; Integration</a>
<a href="http://btsalp.com/wp-content/uploads/2016/03/29-BTS-newsletter-Drama-Best-and-Dr-Suess.pdf">#29 March 10, 2016  Marmalade Murals, Drama &amp; Dr Seuss</a>
<a href="http://btsalp.com/wp-content/uploads/2016/02/28-BTS-newsletter-Day-on-the-Hill.pdf">#28 February 18, 2016  Day on the Hill</a>
<a href="http://btsalp.com/wp-content/uploads/2016/02/27-Legislative-luncheon-crayons.pdf">#27 February 5, 2016  Legislative Luncheon, The Day the Crayons Quit &amp; Leap Year Lesson Plan</a>
<a href="http://btsalp.com/wp-content/uploads/2016/01/26-BTS-newsletter-cartooning-and-stop-motion.pdf">#26 January 28, 2016  Cartooning &amp; Stop Motion</a>
<a href="http://btsalp.com/wp-content/uploads/2016/01/25-BTS-newsletter-Theatre-and-dance.pdf">#25 January 14, 2016  Wasatch Elementary, Drama, Savion Glover &amp; Oakwood Art Night</a>
<a href="http://btsalp.com/wp-content/uploads/2015/12/24-Drama-Weber.pdf">#24 December 10, 2015  Drama, Shadow Show, Yellow Ball Project &amp; Mother Goose Summer Camp</a>
<a href="http://btsalp.com/wp-content/uploads/2015/11/23-newsletter-Paper-Circuits-SUU.pdf">#23 November 19, 2015  Paper Circuits &amp; SUU</a>
<a href="http://btsalp.com/wp-content/uploads/2015/11/22-BTS-newsletter-Westminster-South-Summit.pdf">#22 November 12, 2015  Dia de los Muertos &amp; Kathleen Briley</a>
<a href="http://btsalp.com/wp-content/uploads/2015/10/21-BTS-newsletter-Touch-Tour.pdf">#21 October 15, 2015  Touch Tour</a>
<a href="http://btsalp.com/wp-content/uploads/2015/09/20-BTS-newsletter-Oct-1st.pdf">#20 October 1, 2015  Spotlight on Miriam Bowen, Jana Shumway &amp; Lesson Plan</a>
<a href="http://btsalp.com/wp-content/uploads/2015/09/19-BTS-newsletter.pdf">#19 September 17, 2015  Spotlight on Rosie Mitchell</a>
<a href="http://btsalp.com/wp-content/uploads/2015/09/18-Back-to-School.pdf">#18 Back to School Edition &amp; Paint Party at Draper Elementary</a>
<a href="http://btsalp.com/wp-content/uploads/2015/08/BTSALP-Newletter-17-Summer-Edition.pdf">#17 Summer 2015 Edition</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-16-May.pdf">#16 May 28, 2015  Architecture Part 2, Donna Pence &amp; Lesson Plan</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-15-May.pdf">#15 May 14, 2015  Architecture Part 1, Donna Pence, Box City &amp; Lesson Plan</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-14-April.pdf">#14 April 29, 2015  Informances, Kite Festival &amp; Arts Nights</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-13-April.pdf">#13 April 9, 2015  ArtsLINK, Rosie Mitchell, Jana Shumway, Jennifer Purdy, Penny Caywood &amp; Geography</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-12-March.pdf">#12 March 26, 2015  Legislative Luncheon and Ryan Hourigan</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-11-March.pdf">#11 March 12, 2015  Rebecca Penerosa, Maori, New Zealand, Music &amp; Lesson Plan</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-10-February.pdf">#10 February 26, 2015  Sandy Brunvand, Reuse and Recycle Altered Vintage Papers Professional Development &amp; Lesson Plan</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-9-January.pdf">#9 January 22, 2015  Pixalated 8-Bit Art, Color Mixing &amp; Math and Art Integration</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-8-January.pdf">#8 January 8, 2015  Oakwood Elementary Art Night - Starry, Starry Night</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-7-December.pdf">#7 December 4, 2014  Veteran\'s Day at Wasatch Elementary</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-6-November.pdf">#6 November 20, 2014  Laurie Shea, Native American Culture &amp; Fall Still Life</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-5-November.pdf">#5 November 6, 2014  Bookmaking &amp; Tape Transfer</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-4-October.pdf">#4 October 16, 2014  Joanne Meitler, Peek Inside the Music Studio, Side-By-Side &amp; Funeral March of a Marionette Lesson Plan</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-3-October.pdf">#3 October 2, 2014  Paul Heath, Large Murals, Fish Patterns &amp; Coral</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-2.pdf">#2 September 18, 2014  Making Waves at Arts Fusion</a>
<a href="http://btsalp.com/wp-content/uploads/2015/06/BTSALP-Newletter-1-September.pdf">#1 September 4, 2014  Kelby McIntyre, Theater &amp; Cyclops Lesson Plan</a>';

// load wp
include '../../../../../wp-load.php';

// get all the anchor tags
$dom = new DOMDocument();
$dom->loadHTML($old_html);
$links = $dom->getElementsByTagName('a');

// init newsletters array
$newsletters = array();
// loop through anchor tags
foreach( $links as $link ){

	// get the inner text
	$inner = $link->nodeValue;

	// look for the date
	preg_match_all('/\#\d*\s(.*\d\d\d\d)\s/m', $inner, $matches, PREG_SET_ORDER, 0);

	$date = '';

	if( !empty($matches[0][1]) ){
		$date = date('Ymd', strtotime($matches[0][1]));
	}

	// look for the title
	preg_match_all('/\#\d*\s.*\d\d\d\d\s(.*)/m', $inner, $matches, PREG_SET_ORDER, 0);

	$title = '';

	if( !empty($matches[0][1]) ){
		$title = $matches[0][1];
	}

	$temp = array(
		'attachment_id' => get_attachment_id_by_filepath( $link->getAttribute('href') ),
		'issue' => str_replace('#', '', explode(' ', $link->nodeValue)[0]),
		'date' => $date,
		'title' => $title,
	);

	$newsletters[] = $temp;
}

error_log(var_export($newsletters, true));

if( !empty($newsletters) ){
	foreach( $newsletters as $newsletter ){
		update_field('newsletter_date', $newsletter['date'], $newsletter['attachment_id']);
		update_field('newsletter_title', $newsletter['title'], $newsletter['attachment_id']);
		update_field('newsletter_issue', $newsletter['issue'], $newsletter['attachment_id']);
		wp_set_object_terms( $newsletter['attachment_id'], array('newsletter'), 'category' );
	}
}






































?>