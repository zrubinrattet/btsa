<?php
$id = MiscUtil::get_field('oembed_grid_id', $post_id, $loader_vars);
$title = MiscUtil::get_field('oembed_grid_title', $post_id, $loader_vars);
$items = MiscUtil::get_field('oembed_grid_items', $post_id, $loader_vars);
if( !empty($items) ):
?>
<section class="oembedgrid"<?php MiscUtil::maybe_echo_id($id); ?>>
	<div class="oembedgrid-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h2 class="oembedgrid-wrapper-title"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php foreach( $items as $item ): ?>
			<div class="oembedgrid-wrapper-item">
				<div class="oembedgrid-wrapper-item-oembed">
					<?php echo $item['oembed_grid_item_oembed']; ?>
				</div>
				<div class="oembedgrid-wrapper-item-text">
					<h3 class="oembedgrid-wrapper-item-text-title"><?php echo $item['oembed_grid_item_title'] ?></h3>
					<div class="oembedgrid-wrapper-item-text-desc"><?php echo $item['oembed_grid_item_desc'] ?></div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>
<?php endif; ?>