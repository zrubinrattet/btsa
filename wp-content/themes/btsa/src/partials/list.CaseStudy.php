<?php
$id = MiscUtil::get_field('casestudy_list_id', $post_id, $loader_vars);

$casestudies = get_posts(array(
	'posts_per_page' => -1,
	'post_type' => 'casestudy',
	'post_status' => array('publish'),
));

if( !empty( $casestudies ) ):
?>
<section class="casestudylist"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<h2 class="casestudylist-title">Case Studies</h2>
	<div class="casestudylist-wrapper section-wrapper">
		<?php foreach( $casestudies as $casestudy ): ?>
			<div class="casestudylist-wrapper-casestudy">
				<div class="casestudylist-wrapper-casestudy-stats"<?php MiscUtil::maybe_render_inline_style(array('background-color' => MiscUtil::get_color(rand(3, 7)))); ?>>
					<?php
					echo preg_replace_callback('/(\d.%\sto\s\d.%)|(\d.%)/m', function($matches){
						foreach( $matches as $index => $match ){
							return $matches[$index] = '<span class="huge">' . $match . '</span>';
						}
					}, get_field('casestudy_stats', $casestudy->ID));
					?>
				</div>
				<div class="casestudylist-wrapper-casestudy-main">
					<div class="casestudylist-wrapper-casestudy-main-copy"><?php the_field('casestudy_copy', $casestudy->ID); ?></div>
					<ul class="casestudylist-wrapper-casestudy-main-attrs">
						<li class="casestudylist-wrapper-casestudy-main-attrs-attr"><?php the_field('casestudy_school', $casestudy->ID); ?></li>
						<li class="casestudylist-wrapper-casestudy-main-attrs-attr"><?php the_field('casestudy_district', $casestudy->ID); ?></li>
						<?php foreach( get_field('casestudy_authors', $casestudy->ID) as $author ): ?>
							<li class="casestudylist-wrapper-casestudy-main-attrs-attr"><?php echo $author['casestudy_author']; ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>
<?php endif; ?>