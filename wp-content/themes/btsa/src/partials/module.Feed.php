<?php
$id = MiscUtil::get_field('feed_module_id', $post_id, $loader_vars);
$title = MiscUtil::get_field('feed_module_title', $post_id, $loader_vars);
$max_posts = MiscUtil::get_field('feed_module_max_posts', $post_id, $loader_vars);
$post_terms = MiscUtil::get_field('feed_module_post_terms', $post_id, $loader_vars);
$post_type = MiscUtil::get_field('feed_module_post_type', $post_id, $loader_vars);
?>
<section class="feedmodule"<?php MiscUtil::maybe_echo_id($id); ?>>
	<div class="feedmodule-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h2 class="feedmodule-wrapper-title"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php

			$posts = get_posts(array(
				'posts_per_page' => !empty($max_posts) ? $max_posts : 5,
				'post_type' => !empty($post_type) ? $post_type : 'post',
				'post_status' => 'publish',
				'tax_query' => MiscUtil::build_tax_query_from_terms($post_terms),
			));

			if( !empty($posts) ): ?>
				<div class="feedmodule-wrapper-posts">
					<?php
					foreach( $posts as $post ):
						echo PostHandler::get_feed_post($post);
					endforeach; ?>
				</div>
		<?php
			endif;

			if( $cta = MiscUtil::get_field('feed_module_cta', $post_id, $loader_vars) ): ?>
				<a href="<?php echo $cta['url']; ?>" class="feedmodule-wrapper-cta<?php echo $cta['isarchivefeed'] ? ' archivefeed' : ''; ?>"><?php echo $cta['title']; ?></a>
		<?php endif; ?>
	</div>
</section>