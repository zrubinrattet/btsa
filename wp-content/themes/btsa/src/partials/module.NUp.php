<?php
$id = MiscUtil::get_field('nup_module_id', $post_id, $loader_vars);
$title = MiscUtil::get_field('nup_module_title', $post_id, $loader_vars);
$items = MiscUtil::get_field('nup_module_items', $post_id, $loader_vars);
$full_width = MiscUtil::get_field('nup_module_full_width', $post_id, $loader_vars);
$max_column_count = MiscUtil::get_field('nup_module_max_column_count', $post_id, $loader_vars);
$hover_mode = MiscUtil::get_field('nup_module_hover_mode', $post_id, $loader_vars);
$cta = MiscUtil::get_field('nup_module_cta', $post_id, $loader_vars);
$hash = MiscUtil::get_rand_hash();
?>
<section class="nup <?php echo $hash ?>"<?php MiscUtil::maybe_echo_id($id); ?>>
	<div class="nup-wrapper section-wrapper"<?php MiscUtil::maybe_render_inline_style(array(
			'max-width' => $full_width ? '100%' : '',
			'padding-left' => $full_width ? '0px' : '',
			'padding-right' => $full_width ? '0px' : '',
		)); ?>>
		<?php if( !empty($title) ): ?>
			<h2 class="nup-wrapper-title"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php if( !empty($items) ):
			if( !empty($max_column_count) ): ?>
				<style type="text/css">
					<?php if( $hover_mode ): ?>
						@media only screen {
							<?php foreach( $items as $index => $item ): ?>
								.<?php echo $hash ?> .nup-wrapper-items-item:nth-child(<?php echo $index + 1 ?>){
									position: relative;
								}
								.<?php echo $hash ?> .nup-wrapper-items-item:nth-child(<?php echo $index + 1 ?>):hover{
									background-color: <?php echo $item['nup_module_item_bg_color']; ?>;
								}
								.<?php echo $hash ?> .nup-wrapper-items-item:nth-child(<?php echo $index + 1 ?>):after{
									content: '';
									background-image: url('<?php echo $item['nup_module_item_bg_image']['url']; ?>');
									position: absolute;
									top: 0px;
									left: 0px;
									bottom: 0px;
									right: 0px;
									background-size: <?php echo $item['nup_module_item_bg_image_size']; ?>;
									background-repeat: no-repeat;
									background-position: center center;
									z-index: 5;
									transition: opacity 0.25s ease;
									opacity: 1;
								}
							<?php endforeach; ?>
						}
					<?php endif; ?>
					@media only screen and (min-width: 768px){
						.<?php echo $hash ?> .nup-wrapper-items{
							display: grid;
							grid-template-columns: repeat(<?php echo $max_column_count ?>, 1fr);
						}
					}
				</style>
			<?php endif; ?>
			<div class="nup-wrapper-items">
				<?php
					foreach( $items as $index => $item ):
						$link = $item['nup_module_item_link'];
						$style_arr = array();
						if( $hover_mode ){
							// $style_arr['background-image'] = 'url( ' . $item['nup_module_item_bg_image']['url'] . ' )';
							// $style_arr['background-color'] = $item['nup_module_item_bg_color'];
						}
						else{
							if( $item['nup_module_item_bg_type'] === 'image' ){
								$style_arr['background-image'] = 'url( ' . $item['nup_module_item_bg_image']['url'] . ' )';
							}
							elseif( $item['nup_module_item_bg_type'] === 'color' ){
								$style_arr['background-color'] = $item['nup_module_item_bg_color'];
							}
							$style_arr['background-size'] = $item['nup_module_item_bg_image_size'];
						}
						$style = MiscUtil::maybe_render_inline_style($style_arr, true);
						// el start
						if( !empty($link) ):
							?>
							<a target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>" class="nup-wrapper-items-item<?php echo $hover_mode ? ' hovermode' : ''; ?>"<?php echo $style; ?>>
							<?php
						else:
							?>
							<div class="nup-wrapper-items-item<?php echo $hover_mode ? ' hovermode' : ''; ?>"<?php echo $style; ?>>
							<?php
						endif;
						?>
							<div class="nup-wrapper-items-item-content">
								<?php
									if( $item['nup_module_item_no_filter'] ){
										remove_filter('acf_the_content', 'wpautop');
										
										echo get_field( 'module_builder_' . $loader_vars['fc_index'] . '_nup_module_items_' . $index . '_nup_module_item_text', $post_id, false );
										
										add_filter('acf_the_content', 'wpautop');
									}
									else{
										echo $item['nup_module_item_text'];
									}
								?>
							</div>
						<?php
						// el end
						if( !empty($link) ):
							?>
							</a>
							<?php
						else:
							?>
							</div>
							<?php
						endif;
					endforeach; ?>
			</div>
		<?php endif; ?>
		<?php if( !empty($cta) ): ?>
			<a target="<?php echo $cta['target'] ?>" href="<?php echo $cta['url'] ?>" class="nup-wrapper-cta"><?php echo $cta['title']; ?></a>
		<?php endif; ?>
	</div>
</section>