<?php
$id = MiscUtil::get_field('wysiwyg_module_id', $post_id, $loader_vars);
$content = MiscUtil::get_field('wysiwyg_module_content', $post_id, $loader_vars, false);
$disable_filter = MiscUtil::get_field('wysiwyg_module_disable_filter', $post_id, $loader_vars, false);
?>
<section class="wysiwyg"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="wysiwyg-wrapper section-wrapper">
		<?php if( !empty($content) ): ?>
			<div class="wysiwyg-wrapper-content">
				<?php
					if( $disable_filter ){
						echo do_shortcode($content);
					}
					else{
						echo apply_filters( 'the_content', do_shortcode($content) );
					}
				?>
			</div>
		<?php endif; ?>
	</div>
</section>