<?php

$tile_images = get_field('tile_images', 'option');

$random_index = rand(0, count($tile_images) - 1);

?>
<section class="desktoptiles">
	<div class="desktoptiles-tiles">
		<div class="desktoptiles-tiles-tile <?php echo array('pink', 'green', 'aqua', 'blue', 'orange')[rand(0, 4)]; ?>"></div>
		<div class="desktoptiles-tiles-tile"></div>
		<div class="desktoptiles-tiles-tile"></div>
		<div class="desktoptiles-tiles-tile"></div>
		<div class="desktoptiles-tiles-tile"<?php
		if( !empty($tile_images[$random_index]) ):
			MiscUtil::maybe_render_inline_style(array(
				'background-image' => "url('" . $tile_images[$random_index]['url'] . "')"
			));
		endif;
		?>></div>
		<div class="desktoptiles-tiles-tile"></div>
		<div class="desktoptiles-tiles-tile"></div>
		<div class="desktoptiles-tiles-tile"></div>
		<div class="desktoptiles-tiles-tile <?php echo array('pink', 'green', 'aqua', 'blue', 'orange')[rand(0, 4)]; ?>"></div>
	</div>
</section>