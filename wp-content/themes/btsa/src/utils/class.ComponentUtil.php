<?php 
	
	class ComponentUtil extends LoaderUtil{
		/**
		 * [$files the components to register]
		 * @var array
		 */
		public static $files = array(
			'nav.Main' => '/src/components/nav.Main.php',
			'nav.Footer' => '/src/components/nav.Footer.php',
			'module.Calendar' => '/src/components/module.Calendar.php',
			'module.CTA' => '/src/components/module.CTA.php',
			'module.Gallery' => '/src/components/module.Gallery.php',
		);
	}
	
?>