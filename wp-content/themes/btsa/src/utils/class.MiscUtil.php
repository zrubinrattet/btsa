<?php

	class MiscUtil{

		/**
		 * [comma_separate_with_ampersand takes an array of strings & comma separates except for last separation which uses an ampersand like: str1, str2, & str3]
		 * @param  array $items array of strings
		 * @return string        comma & ampersand separated string
		 */
		public static function comma_separate_with_ampersand($items = array()){
			if( count($items) > 1 ) {
			    $last = array_pop($items);
			    $result = implode(", ", $items) . " &amp; {$last}";
			}
			else    {
			    $result = $items[0];
			}
			return $result;
		}
		/**
		 * [build_tax_query_from_terms creates the tax query array from a single-D array of term ids]
		 * @param  array $post_terms array of term_ids
		 * @return array             the tax query array
		 */
		public static function build_tax_query_from_terms($post_terms){
			// bail if post_terms are empty
			if( empty($post_terms) ) return array();
			
			$terms_by_taxonomy = array();

			foreach( $post_terms as $term_id ){
				if( empty($terms_by_taxonomy[get_term_field( 'taxonomy', $term_id )]) ) {
					$terms_by_taxonomy[get_term_field( 'taxonomy', $term_id )] = array();
				}

				$terms_by_taxonomy[get_term_field( 'taxonomy', $term_id )] = array_merge($terms_by_taxonomy[get_term_field( 'taxonomy', $term_id )], array($term_id));
			}

			$return = array();

			foreach( $terms_by_taxonomy as $taxonomy => $terms ){
				$return[] = array(
					'taxonomy' => $taxonomy,
					'field' => 'term_id',
					'operator' => 'IN',
					'terms' => $terms
				);
			}
			
			return $return;
		}
		/**
		 * [get_rand_hash makes & returns low-collision small character hash]
		 * @return string the hash
		 */
		public static function get_rand_hash(){
			return 'h' . hash('crc32', random_bytes(10));
		}
		/**
		 * [get_color php's store of the theme colors]
		 * @param  mixed $color_name color by name or int by index
		 * @param  bool   $hues_only  doesn't return black gray or white
		 * @return mixed             hex of the color name if a name is provided otherwise an array of all the colors is returned
		 */
		public static function get_color($color_name = '', $hues_only = false){
			$colors = array(
				'black' => '#000',
				'gray' => '#D1D2D3',
				'white' => '#fff',
				'pink' => '#CC3C92',
				'green' => '#A2CB39',
				'orange' => '#E96548',
				'aqua' => '#51C0B4',
				'blue' => '#2CABE1',
			);

			if( $color_name === '' ){
				if( $hues_only ){
					return array_slice($colors, 3);
				}
				return $colors;
			}
			else{
				if( gettype($color_name) === 'string' ){
					return $colors[$color_name];
				}
				else{
					return array_values($colors)[$color_name];
				}
			}
		}
		/**
		 * [get_field used to get a field in a partial or component]
		 * @param  string      $field_name the field name
		 * @param  string|int  $id         the post's id
		 * @param  array       $lv         the loader_vars array
		 * @param  boolean     $formatting whether or not to 
		 * @return mixed                   the content from the field
		 */
		public static function get_field($field_name, $id, $lv, $formatting = true){
			// use the value in the loader vars if it exists
			if( isset($lv[$field_name]) && !empty($lv[$field_name]) ) return $lv[$field_name];
			// return nothing if it's not provided in loader vars and from_fc is set & is false
			if( isset($lv['from_fc']) && !$lv['from_fc'] && !isset($lv[$field_name]) ) return '';
			// otherwise get the value from the db
			return get_field($lv['mb_basename'] . '_' . $lv['fc_index'] . '_' . $field_name, $id, $formatting, $formatting);
		}
		/**
		 * [get_intelligent_id used mostly for internal purposes. this function will return an ID that's best passed in as a second param to get_field. if it's a post id it'll be either a string or an int of that post id. if it's a term it'll be term_ followed by the term_id like 'term_234']
		 * @return str|int the intelligent ID to be passed into get_field
		 */
		public static function get_intelligent_id(){
			// get the queried object
			$queried_object = get_queried_object();
			// setup wp_the_query
			global $wp_the_query;
			// sometimes when pages get redirected get_queried_object returns null
			if( is_null($queried_object) ){
				// use the post object in wp_the_query in this case
				$queried_object = $wp_the_query->post; 
			}
			// assign ID based on the queryed object class name
			switch (get_class($queried_object)) {
				case 'WP_Term':
					return 'term_' . strval($queried_object->term_id);
					break;
				
				case 'WP_Post':
					return $queried_object->ID;
					break;
			}
		}
		/**
		 * [maybe_render_inline_style renders inline style tag if associative pairs are avail]
		 * @param  array $arr associative array of inline-style keys and their values
		 * @return null      echo's inline style tag
		 */
		public static function maybe_render_inline_style($arr, $return = false){
			$output = ' style="';
			foreach( $arr as $key => $value ){
				if( !empty($value) ){
					$output .= $key . ': ' . $value . ';';
				}
			}
			$output .= '"';
			if( $output != ' style=""' ){
				if( $return ){
					return $output;
				}
				else{
					echo $output;
				}
			}
			else{
				if( $return ){
					return '';
				}
				else{
					echo '';
				}
			}
		}
		/**
		 * [maybe_echo_id maybe adds id attr to section]
		 * @param  string $id the id to set
		 */
		public static function maybe_echo_id($id = ''){
			if( !empty($id) ){
				echo ' id="' . $id . '" ';
			}
			else{
				echo '';
			}
		}
	}
?>