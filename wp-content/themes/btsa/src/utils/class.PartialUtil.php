<?php

	class PartialUtil extends LoaderUtil{
		/**
		 * [$files the partials to register]
		 * @var array
		 */
		public static $files = array(
			'module.WYSIWYG' => '/src/partials/module.WYSIWYG.php',
			'module.Feed' => '/src/partials/module.Feed.php',
			'module.NUp' => '/src/partials/module.NUp.php',
			'logo.Main' => '/src/partials/logo.Main.php',
			'logo.GetInvolved' => '/src/partials/logo.GetInvolved.php',
			'grid.Oembed' => '/src/partials/grid.Oembed.php',
			'tiles.Main' => '/src/partials/tiles.Main.php',
			'list.CaseStudy' => '/src/partials/list.CaseStudy.php',
		);
	}

?>